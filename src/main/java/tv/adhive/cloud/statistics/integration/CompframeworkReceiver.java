package tv.adhive.cloud.statistics.integration;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import tv.adhive.cloud.rest.Request;
import tv.adhive.cloud.rest.channel.AdhChannelTagsAI;
import tv.adhive.cloud.statistics.client.AccountClient;
import tv.adhive.cloud.statistics.service.ElasticService;
import tv.adhive.cloud.statistics.service.MediaService;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.media.response.AIStatistics;
import tv.adhive.model.media.response.AiStatus;
import tv.adhive.model.media.response.Source;
import tv.adhive.model.media.response.Tag;
import tv.adhive.model.media.statistics.MediaStatistics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@MessageEndpoint
public class CompframeworkReceiver {

    private static final Logger log = LoggerFactory.getLogger(CompframeworkReceiver.class);

    private static final String FROM_COMPFRAMEWORK_RESPONSE_QUEUE_CHANNEL = "fromCompframeworkResponseQueueChannel";

    private final ElasticService elasticService;

    private final AccountClient accountClient;

    private final MediaService mediaService;

    private static final String SPACE = " ";

    @Autowired
    public CompframeworkReceiver(ElasticService elasticService, AccountClient accountClient, MediaService mediaService) {
        this.elasticService = elasticService;
        this.accountClient = accountClient;
        this.mediaService = mediaService;
    }

    /**
     * {@inheritDoc}
     */
    @ServiceActivator(inputChannel = FROM_COMPFRAMEWORK_RESPONSE_QUEUE_CHANNEL)
    public void registerAIStatistics(AIStatistics statistics) {
        log.debug("registerAIStatistics() - start: statistics = {}", statistics);
        elasticService.save(statistics, statistics.getTypeSource());
        Channel channel = elasticService.findChannelByMediaId(statistics.getTypeSource(), statistics.getId());
        if (channel != null) {
            statistics.setChannelId(channel.getId());
            elasticService.save(statistics, statistics.getTypeSource());
            log.debug("registerAIStatistics() - send tags to an account");
            List<String> tags = new ArrayList<>();
            StringBuilder textSound = new StringBuilder();
            for (Source source : statistics.getSources()) {
                if (source.getTags() != null) {
                    for (Tag tag : source.getTags()) {
                        if (!tags.contains(tag.getTag())) tags.add(tag.getTag());
                    }
                } else if (StringUtils.isNotBlank(source.getText())) textSound.append(source.getText());
            }

            if (channel != null && channel.getTags() != null) {
                channel.getTags().addAll(tags);
                elasticService.save(channel, channel.getType().name());
            }

            if (tags.size() > 0 || textSound.length() > 0) {
                log.debug("found tags: idChannel={}; idMedia={}; type={}; tags={}", channel.getId(), statistics.getId(), statistics.getTypeSource(), StringUtils.join(tags, ","));
                Request<AdhChannelTagsAI> request = new Request();
                AdhChannelTagsAI adhChannelTagsAI = new AdhChannelTagsAI();
                adhChannelTagsAI.setExternalId(channel.getId());
                adhChannelTagsAI.setType(statistics.getTypeSource().toLowerCase());
                adhChannelTagsAI.setTagsAI(tags);
                adhChannelTagsAI.setTextSoundAI(textSound.toString());
                request.setParams(adhChannelTagsAI);
                accountClient.updateTagsAI(request);
            } else {
                log.debug("tags not found: idChannel={}; idMedia={}; type={}", channel.getId(), statistics.getId(), statistics.getTypeSource());
            }
        }

        setMediaStatisticAsProcessed(statistics);

        log.debug("registerAIStatistics() - end");
    }

    private void setMediaStatisticAsProcessed(AIStatistics statistics) {
        log.debug("setMediaStatisticsAsProcessed() - start");
        final MediaStatistics mediaStatistics = mediaService.findById(statistics.getTypeSource(), statistics.getId());
        if (mediaStatistics != null) {
            mediaStatistics.setAiStatus(AiStatus.PROCESSED);
            mediaService.save(statistics.getTypeSource(), mediaStatistics);

            statistics.setChannelId(mediaStatistics.getChannelId());
            elasticService.save(statistics, statistics.getTypeSource());
        }
        log.debug("setMediaStatisticsAsProcessed() - end");
    }

    private List<String> getOfftenIdenticalTags(String str) {
        String[] allTags = str.split(SPACE);
        return Arrays.stream(allTags).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream().filter(map -> map.getValue() > 1).limit(10).map(Map.Entry::getKey).collect(Collectors.toList());

    }
}
