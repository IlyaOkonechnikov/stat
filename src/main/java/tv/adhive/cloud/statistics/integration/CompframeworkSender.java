package tv.adhive.cloud.statistics.integration;


import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import tv.adhive.model.media.request.MediaRequest;
import tv.adhive.cloud.statistics.config.IntegrationConfig;

@MessagingGateway
public interface CompframeworkSender {
    @Gateway(requestChannel = IntegrationConfig.TO_COMPFRAMEWORK_REQUEST_QUEUE_CHANNEL)
    void addComputationMedia(MediaRequest mediaRequest);
}
