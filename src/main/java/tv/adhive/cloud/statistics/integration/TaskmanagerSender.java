package tv.adhive.cloud.statistics.integration;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import tv.adhive.cloud.statistics.config.IntegrationConfig;
import tv.adhive.model.channel.Channel;

@MessagingGateway
public interface TaskmanagerSender {
    @Gateway(requestChannel = IntegrationConfig.TO_ASSIGN_TASKS_QUEUE_CHANNEL)
    void assignTasks(Channel channel);
}
