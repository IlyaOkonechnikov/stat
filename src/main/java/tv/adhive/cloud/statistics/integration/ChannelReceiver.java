package tv.adhive.cloud.statistics.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.transaction.annotation.Transactional;
import tv.adhive.cloud.channel.api.channel.ChannelNotFoundException;
import tv.adhive.cloud.channel.api.utils.JsonUtils;
import tv.adhive.cloud.mq.MessageQueue;
import tv.adhive.cloud.statistics.Constants;
import tv.adhive.cloud.statistics.config.IntegrationConfig;
import tv.adhive.cloud.statistics.service.ChannelService;
import tv.adhive.model.channel.Channel;

@MessageEndpoint
public class ChannelReceiver {

    private static final Logger log = LoggerFactory.getLogger(ChannelReceiver.class);

    private static final int DELAY = 3000;

    @Autowired
    private ChannelService channelService;
    @Autowired
    private TaskmanagerSender taskmanagerSender;

    @Transactional
    @ServiceActivator(inputChannel = IntegrationConfig.FROM_CHANNEL_QUEUE_CHANNEL)
    @Retryable(maxAttempts = 2, backoff = @Backoff(delay = DELAY))
    public void registerMediaStatistics(MessageQueue<Object> messageQueue) {

        try {
            //TODO: придумать нормальное решения для маппинга json из объекта с дженериками
            Channel channel = JsonUtils.convertJsonToObject(JsonUtils.convertObjectToJson(messageQueue.getData()), Channel.class);
            switch (messageQueue.getOperation()) {
                case SAVE:
                    log.debug("registerCommonStatistics() - start: statistics = {}", channel);
                    channelService.registerChannel(channel, Constants.MEDIA_AMOUNT);
                    Channel channelForAssignTasks = new Channel();
                    channelForAssignTasks.setId(channel.getId());
                    channelForAssignTasks.setType(channel.getType());
                    taskmanagerSender.assignTasks(channelForAssignTasks);
                    log.debug("registerCommonStatistics() - end");
                    break;
                case UPDATE:
                    log.debug("updateCommonStatistics() - start: statistics = {}", channel);
                    channelService.updateChannel(channel, Constants.MEDIA_AMOUNT);
                    log.debug("updateCommonStatistics() - end");
                    break;
                case DELETE:
                    log.debug("unregisterCommonStatistics() - start: type = {}; id = {}", channel.getType(), channel.getId());
                    channelService.unregisterChannel(channel.getType().name().toLowerCase(), channel.getId());
                    log.debug("unregisterCommonStatistics() - end: type = {}; id = {}", channel.getType(), channel.getId());
                    break;
            }
        } catch (Exception e) {
            log.error("*** registerMediaStatistics Error", e);
        }


    }
}
