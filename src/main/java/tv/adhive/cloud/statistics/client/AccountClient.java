package tv.adhive.cloud.statistics.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import tv.adhive.cloud.rest.Request;
import tv.adhive.cloud.rest.Response;
import tv.adhive.cloud.rest.channel.AdhChannel;
import tv.adhive.cloud.rest.channel.AdhChannelTagsAI;

/**
 * Account client
 */
@FeignClient(value = "account", path = "/fwk")
public interface AccountClient {

    @PostMapping(value = "/methodPost/updateTagsAI", consumes = "application/json")
    Response updateTagsAI(@RequestBody Request<AdhChannelTagsAI> request);

    @PostMapping(value = "/methodPost/updateIsStatChannel", consumes = "application/json")
    Response updateIsStatChannel(@RequestBody Request<AdhChannel> request);
}
