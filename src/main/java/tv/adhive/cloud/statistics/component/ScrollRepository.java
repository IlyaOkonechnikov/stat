package tv.adhive.cloud.statistics.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.elasticsearch.action.ActionRequestValidationException;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.*;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.channel.statistics.ChannelStatisticsItem;
import tv.adhive.model.media.response.AIStatistics;
import tv.adhive.model.media.statistics.MediaStatistics;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

@Slf4j
public class ScrollRepository {

    private final RestHighLevelClient client;

    private ObjectMapper mapper = new ObjectMapper();

    public ScrollRepository(RestHighLevelClient client) {
        this.client = client;
    }

    public void scroll(String index, int stepSize) throws IOException {
        AtomicInteger i = new AtomicInteger(0);

        ClearScrollRequest clearRequest = new ClearScrollRequest();

        // Step #1

        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchAllQuery());
        searchSourceBuilder.size(stepSize);
        searchRequest.source(searchSourceBuilder);
        searchRequest.scroll(TimeValue.timeValueMinutes(1L));
        SearchResponse searchResponse = client.search(searchRequest);
        String scrollId = searchResponse.getScrollId();
        SearchHits hits = searchResponse.getHits();

        clearRequest.addScrollId(scrollId);

        // Step #2

        while (hits.getHits().length >= stepSize) {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(TimeValue.timeValueSeconds(30));
            SearchResponse searchScrollResponse = client.searchScroll(scrollRequest);
            scrollId = searchScrollResponse.getScrollId();
            hits = searchScrollResponse.getHits();

            clearRequest.addScrollId(scrollId);
        }

        ClearScrollResponse response = client.clearScroll(clearRequest);
        boolean success = response.isSucceeded();
        int released = response.getNumFreed();
    }

    public void scrollAndUpdate(String parentIndex, String childIndex, String childType, int stepSize) throws IOException {

        // Step #1

        SearchRequest searchRequest = new SearchRequest(parentIndex);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchAllQuery());
        searchSourceBuilder.size(stepSize);
        searchRequest.source(searchSourceBuilder);
        searchRequest.scroll(TimeValue.timeValueMinutes(1L));
        SearchResponse searchResponse = client.search(searchRequest);
        String scrollId = searchResponse.getScrollId();
        SearchHits hits = searchResponse.getHits();

        hits.forEach(hit ->
                updateLastMedia(childIndex, childType, hit)
        );

        ClearScrollRequest clearRequest = new ClearScrollRequest();
        clearRequest.addScrollId(scrollId);

        // Step #2

        while (hits.getHits().length >= stepSize) {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(TimeValue.timeValueSeconds(30));
            SearchResponse searchScrollResponse = client.searchScroll(scrollRequest);
            scrollId = searchScrollResponse.getScrollId();
            hits = searchScrollResponse.getHits();

            hits.forEach(hit ->
                    updateLastMedia(childIndex, childType, hit)
            );
            clearRequest.addScrollId(scrollId);
        }

        // Step #3

        ClearScrollResponse response = client.clearScroll(clearRequest);
        boolean success = response.isSucceeded();
        int released = response.getNumFreed();
        log.info("Success {} scrolled through {} released {}", success, hits.totalHits, released);
    }

    private void updateLastMedia(String childIndex, String childType, SearchHit hit) {
        try {
            ArrayList<String> lastIdMedia = (ArrayList<String>) hit.getSourceAsMap().get("lastIdMedia");
            BulkRequest bulkRequest = new BulkRequest();
            lastIdMedia.forEach(mediaId -> {
                UpdateRequest request = new UpdateRequest(childIndex, childType, mediaId);

                final Map<String, Object> updateMap = new HashMap<>();
                updateMap.put("channelId", hit.getSourceAsMap().get("id"));
                updateMap.put("lastModifiedDate", new Date().getTime());
                request.doc(updateMap);

                bulkRequest.add(request);
            });

            final ActionRequestValidationException validate = bulkRequest.validate();
            if (validate == null) client.bulk(bulkRequest);
        } catch (IOException e) {
            log.error("Fail while migrating lastIdMedia ", e);
        }
    }


    public void updateTagsInChannel(String parentIndex, String childIndex, String childType, int stepSize) throws IOException {
        // Step #1

        SearchRequest searchRequest = new SearchRequest(parentIndex);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchAllQuery());
        searchSourceBuilder.size(stepSize);
        searchRequest.source(searchSourceBuilder);
        searchRequest.scroll(TimeValue.timeValueMinutes(1L));
        SearchResponse searchResponse = client.search(searchRequest);
        String scrollId = searchResponse.getScrollId();
        SearchHits hits = searchResponse.getHits();

        hits.forEach(hit ->
                updateTags(childIndex, childType, hit)
        );

        ClearScrollRequest clearRequest = new ClearScrollRequest();
        clearRequest.addScrollId(scrollId);

        // Step #2

        while (hits.getHits().length >= stepSize) {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(TimeValue.timeValueSeconds(30));
            SearchResponse searchScrollResponse = client.searchScroll(scrollRequest);
            scrollId = searchScrollResponse.getScrollId();
            hits = searchScrollResponse.getHits();

            hits.forEach(hit ->
                    updateTags(childIndex, childType, hit)
            );
            clearRequest.addScrollId(scrollId);
        }

        // Step #3

        ClearScrollResponse response = client.clearScroll(clearRequest);
        boolean success = response.isSucceeded();
        int released = response.getNumFreed();
        log.info("Success {} scrolled through {} released {}", success, hits.totalHits, released);
    }

    private void updateTags(String childIndex, String childType, SearchHit hit) {
        try {
            ArrayList<String> lastIdMedia = (ArrayList<String>) hit.getSourceAsMap().get("lastIdMedia");
            Set<String> tags = new LinkedHashSet<>();

            for (String mediaId : lastIdMedia) {
                GetRequest getRequest = new GetRequest(childIndex, childType, mediaId);
                final GetResponse ai = client.get(getRequest);
                log.warn(ai.getSourceAsString());
                if (ai.isExists()) {
                    final AIStatistics aiStatistics = mapper.readValue(ai.getSourceAsString(), AIStatistics.class);
                    aiStatistics.getSources().forEach(s -> {
                        if (s != null && s.getTags() != null) {
                            s.getTags().forEach(t -> {
                                tags.add(t.getTag());
                            });
                        }
                    });
                }
            }

            UpdateRequest updateRequest = new UpdateRequest(hit.getIndex(), hit.getType(), hit.getId());
            final Map<String, Object> updateMap = new HashMap<>();
            updateMap.put("tags", tags.toArray());
            updateRequest.doc(updateMap);

            ActionRequestValidationException validate = updateRequest.validate();
            if (validate == null) client.update(updateRequest);
        } catch (IOException e) {
            log.error("Fail while migrating tags ", e);
        }
    }


    public void updateLikesInChannel(String parentIndex, String childIndex, String childType, int stepSize) throws IOException {
        // Step #1

        SearchRequest searchRequest = new SearchRequest(parentIndex);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchAllQuery());
        searchSourceBuilder.size(stepSize);
        searchRequest.source(searchSourceBuilder);
        searchRequest.scroll(TimeValue.timeValueMinutes(1L));
        SearchResponse searchResponse = client.search(searchRequest);
        String scrollId = searchResponse.getScrollId();
        SearchHits hits = searchResponse.getHits();

        hits.forEach(hit ->
                updateLikesTags(childIndex, childType, hit)
        );

        ClearScrollRequest clearRequest = new ClearScrollRequest();
        clearRequest.addScrollId(scrollId);

        // Step #2

        while (hits.getHits().length >= stepSize) {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(TimeValue.timeValueSeconds(30));
            SearchResponse searchScrollResponse = client.searchScroll(scrollRequest);
            scrollId = searchScrollResponse.getScrollId();
            hits = searchScrollResponse.getHits();

            hits.forEach(hit ->
                    updateLikesTags(childIndex, childType, hit)
            );
            clearRequest.addScrollId(scrollId);
        }

        // Step #3

        ClearScrollResponse response = client.clearScroll(clearRequest);
        boolean success = response.isSucceeded();
        int released = response.getNumFreed();
        log.info("Success {} scrolled through {} released {}", success, hits.totalHits, released);
    }

    private void updateLikesTags(String childIndex, String childType, SearchHit hit) {
        try {
            ArrayList<String> lastIdMedia = (ArrayList<String>) hit.getSourceAsMap().get("lastIdMedia");
            AtomicLong likeCount = new AtomicLong();

            for (String mediaId : lastIdMedia) {
                GetRequest getRequest = new GetRequest(childIndex, childType, mediaId);
                final GetResponse ms = client.get(getRequest);
                log.warn(ms.getSourceAsString());
                if (ms.isExists()) {
                    final MediaStatistics mediaStatistics = mapper.readValue(ms.getSourceAsString(), MediaStatistics.class);
                    likeCount.getAndAdd(mediaStatistics.getStatistics().getLikeCount());
                }
            }

            final Channel channel = mapper.readValue(hit.getSourceAsString(), Channel.class);
            channel.getStatistics().setLikeCount(likeCount.get());
            final String source = mapper.writeValueAsString(channel);

            IndexRequest indexRequest = new IndexRequest(hit.getIndex(), hit.getType(), hit.getId());
            indexRequest.source(source, XContentType.JSON);

            UpdateRequest updateRequest = new UpdateRequest(hit.getIndex(), hit.getType(), hit.getId());
            updateRequest.doc(indexRequest);

            ActionRequestValidationException validate = updateRequest.validate();
            if (validate == null) client.update(updateRequest);
        } catch (IOException e) {
            log.error("Fail while migrating tags ", e);
        }
    }

}
