package tv.adhive.cloud.statistics.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class DataMigration {

    private final ScrollRepository scrollRepository;

    @Autowired
    public DataMigration(ScrollRepository scrollRepository) {
        this.scrollRepository = scrollRepository;
    }

    @Async
    public void addTags() throws IOException {
        scrollRepository.updateTagsInChannel("instagram-channel-index",
                "instagram-aistatistics-index",
                "instagram-aistatistics", 100);

        scrollRepository.updateTagsInChannel("youtube-channel-index",
                "youtube-aistatistics-index",
                "youtube-aistatistics", 100);
    }
}
