package tv.adhive.cloud.statistics.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import tv.adhive.cloud.statistics.filter.media.Media;
import tv.adhive.cloud.statistics.util.ItemConverter;
import tv.adhive.cloud.statistics.util.JsonMapper;
import tv.adhive.model.media.request.TypeMediaSource;
import tv.adhive.model.media.response.AiStatus;
import tv.adhive.model.media.statistics.MediaStatistics;

import java.io.IOException;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

@Repository
public class MediaRepository {

    static final Logger log = LoggerFactory.getLogger(MediaRepository.class);

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final RestHighLevelClient client;

    @Autowired
    public MediaRepository(RestHighLevelClient client) {
        this.client = client;
    }

    public MediaStatistics findById(String index, String type, String id) throws IOException {
        GetRequest getRequest = new GetRequest(index, type, id);
        GetResponse getResponse = client.get(getRequest);

        if (getResponse.isExists()) {
            final MediaStatistics mediaStatistics = objectMapper.readValue(getResponse.getSourceAsString(), MediaStatistics.class);
            return mediaStatistics;
        } else {
            return null;
        }
    }

    @Async
    public MediaStatistics save(String index, String type, MediaStatistics mediaStatistics) throws IOException {
        log.debug("save - start: Media = {} ", mediaStatistics);
        IndexRequest request = new IndexRequest(index, type, mediaStatistics.getId());
        final String toJson = JsonMapper.toJson(mediaStatistics);

        request.source(toJson, XContentType.JSON);
        client.index(request);
        log.debug("save  - end: Media = {} ", mediaStatistics);
        return mediaStatistics;
    }

    public List<MediaStatistics> findAllByChannelId(String index, String type, String channelId) throws IOException {
        log.debug("findAllByChannelId - start: channelId = {} ", channelId);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(termQuery("channelId", channelId));
        sourceBuilder.size(100);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(index);
        searchRequest.types(type);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);
        final SearchHit[] hits = search.getHits().getHits();
        List<MediaStatistics> result = new ArrayList<>(hits.length);
        for (SearchHit hit : hits) {
            final MediaStatistics fromJson = JsonMapper.fromJson(hit.getSourceAsString(), MediaStatistics.class);
            result.add(fromJson);
        }

        log.debug("findAllByChannelId  - end: channelId = {} ", channelId);
        return result;
    }

    public List<MediaStatistics> findAllByAiStatus(String index, String type, AiStatus aiStatus) throws IOException {
        log.debug("findAllByAiStatus - start: aiStatus = {} ", aiStatus);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(
                boolQuery()
                        .should(termQuery("aiStatus", aiStatus))
                        .should(boolQuery().mustNot(existsQuery("aiStatus")))
        );
        sourceBuilder.size(10000);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(index);
        searchRequest.types(type);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);
        final SearchHit[] hits = search.getHits().getHits();
        List<MediaStatistics> result = new ArrayList<>(hits.length);
        for (SearchHit hit : hits) {
            final MediaStatistics fromJson = JsonMapper.fromJson(hit.getSourceAsString(), MediaStatistics.class);
            result.add(fromJson);
        }

        log.debug("findAllByAiStatus  - end: aiStatus = {} hits {}", aiStatus, result.size());
        return result;
    }


    public int saveAll(String index, String type, List<MediaStatistics> all) throws IOException {
        BulkRequest request = new BulkRequest();
        int successes = 0;
        for (MediaStatistics media : all) {
            final String toJson = JsonMapper.toJson(media);
            request.add(
                    new IndexRequest(index, type, media.getId())
                            .source(toJson, XContentType.JSON));
        }

        BulkResponse bulkResponse = client.bulk(request);

        for (BulkItemResponse bulkItemResponse : bulkResponse) {
            DocWriteResponse itemResponse = bulkItemResponse.getResponse();

            if (bulkItemResponse.getOpType() == DocWriteRequest.OpType.INDEX
                    || bulkItemResponse.getOpType() == DocWriteRequest.OpType.CREATE) {
                IndexResponse indexResponse = (IndexResponse) itemResponse;
                if (RestStatus.CREATED == indexResponse.status()) {
                    successes++;
                }
            }
        }

        return successes;
    }

    public List<Media> getMediaWithWordsInCaption(String channelId, List<String> words, String channelType, Date publishedAt) throws IOException {
        BoolQueryBuilder boolQueryBuilder = boolQuery();
        boolQueryBuilder
                .must(termQuery("channelId", channelId))
                .must(rangeQuery("publishedAt").gte(publishedAt.getTime()));

        words.forEach(word -> boolQueryBuilder.must(matchQuery("caption", word)));

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.size(10000);
        Map<String, String> documentMeta = ItemConverter.getDocumentMeta(
                channelType + "-" + "mediastatistics"
        );
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(documentMeta.get("index"));
        searchRequest.types(documentMeta.get("type"));
        searchRequest.source(searchSourceBuilder);

        SearchResponse response = client.search(searchRequest);
        List<Media> medias = new ArrayList<>();
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            MediaStatistics mediaStatistics = objectMapper.readValue(hit.getSourceAsString(), MediaStatistics.class);
            Media media = new Media();
            media.setId(mediaStatistics.getId());
            media.setType(TypeMediaSource.valueOf(channelType.toUpperCase()));
            medias.add(media);
        }

        return medias;
    }
}
