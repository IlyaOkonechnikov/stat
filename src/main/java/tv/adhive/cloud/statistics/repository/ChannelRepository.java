package tv.adhive.cloud.statistics.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.metrics.valuecount.ValueCount;
import org.elasticsearch.search.aggregations.metrics.valuecount.ValueCountAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.NestedSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.media.request.TypeMediaSource;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static tv.adhive.cloud.statistics.Constants.*;

@Repository
public class ChannelRepository {

    static final Logger log = LoggerFactory.getLogger(ChannelRepository.class);

    private final RestHighLevelClient client;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public ChannelRepository(RestHighLevelClient client) {
        this.client = client;
    }

    /**
     * Returns the number of entities available.
     *
     * @return the number of entities
     */
    public long count(String index, String type) throws IOException {
        final ValueCountAggregationBuilder count = AggregationBuilders
                .count("agg_count").field("id");

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.aggregation(count);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(index);
        searchRequest.types(type);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);
        Aggregations aggregations = search.getAggregations();

        ValueCount agg_count = aggregations.get("agg_count");

        return agg_count.getValue();
    }

    /**
     * Returns entity by id.
     *
     * @return entity
     */
    public Channel findById(TypeMediaSource socialMedia, String id) throws IOException {
        String index = YOUTUBE_CHANNEL_INDEX;
        String type = YOUTUBE_CHANNEL_TYPE;

        if (TypeMediaSource.YOUTUBE == socialMedia) {
            index = YOUTUBE_CHANNEL_INDEX;
            type = YOUTUBE_CHANNEL_TYPE;
        } else if (TypeMediaSource.INSTAGRAM == socialMedia) {
            index = INSTAGRAM_CHANNEL_INDEX;
            type = INSTAGRAM_CHANNEL_TYPE;
        }

        return findById(index, type, id);
    }

    /**
     * Returns entity by id.
     *
     * @return entity
     */
    public Channel findById(String index, String type, String id) throws IOException {
        GetRequest getRequest = new GetRequest(index, type, id);

        GetResponse getResponse = client.get(getRequest);

        if (!getResponse.isSourceEmpty()) {
            final Channel channel = objectMapper.readValue(getResponse.getSourceAsString(), Channel.class);
            return channel;
        } else {
            return null;
        }
    }

    public List<Channel> searchAndFilterByNotToday(String index, String type, long size) throws IOException {
        log.debug("searchAndFilterByNotToday() size {} - start", size);

        List<Channel> channels = new ArrayList<>();

        final FieldSortBuilder sort = new FieldSortBuilder("statistics.subscriberCount")
                .setNestedSort(new NestedSortBuilder("statistics"))
                .order(SortOrder.DESC);

        final Today today = new Today();
        log.debug("searchAndFilterByNotToday from {} to {}", today.from, today.to);

        final BoolQueryBuilder rootBool = boolQuery();
        final BoolQueryBuilder rangeBool = boolQuery();
        final BoolQueryBuilder existsBool = boolQuery();

        rangeBool.mustNot(rangeQuery("lastModifiedDate").from(today.from).to(today.to));
        existsBool.mustNot(existsQuery("lastModifiedDate"));

        rootBool.should(rangeBool);
        rootBool.should(existsBool);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.size(Math.toIntExact(size));
        sourceBuilder.query(rootBool);
        sourceBuilder.sort(sort);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(index);
        searchRequest.types(type);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);
        final SearchHit[] hits = search.getHits().getHits();

        for (SearchHit hit : hits) {
            final Channel channel = objectMapper.readValue(hit.getSourceAsString(), Channel.class);
            channels.add(channel);
        }

        log.debug("searchAndFilterByNotToday() hits {} - end", hits.length);
        return channels;
    }

    class Today {
        final long from;

        final long to;

        Today() {
            LocalDate localDate = LocalDate.now();
            LocalTime localTimeStart = LocalTime.of(0, 0, 0);
            LocalTime localTimeFinish = LocalTime.of(23, 59, 59);

            LocalDateTime localDateTimeStart = LocalDateTime.of(localDate, localTimeStart);
            LocalDateTime localDateTimeFinish = LocalDateTime.of(localDate, localTimeFinish);

            this.from = localDateTimeStart.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
            this.to = localDateTimeFinish.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        }
    }

    public List<Channel> findAllByStatisticsSubscriberCount(String index, String type, long countFrom, long countTo) throws IOException {
        log.debug("findAllBySubscribersCount() size {} - start");

        List<Channel> channels = new ArrayList<>();

        final FieldSortBuilder sort = new FieldSortBuilder("statistics.subscriberCount")
                .setNestedSort(new NestedSortBuilder("statistics"))
                .order(SortOrder.DESC);

        log.debug("findAllBySubscribersCount from {} to {}", countFrom, countTo);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.size(15);
        sourceBuilder.query(nestedQuery("statistics", rangeQuery("statistics.subscriberCount").from(countFrom).to(countTo), ScoreMode.Avg));
        sourceBuilder.sort(sort);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(index);
        searchRequest.types(type);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);
        final SearchHit[] hits = search.getHits().getHits();

        for (SearchHit hit : hits) {
            final Channel channel = objectMapper.readValue(hit.getSourceAsString(), Channel.class);
            channels.add(channel);
        }

        log.debug("findAllBySubscribersCount() hits {} - end", hits.length);
        return channels;
    }

    public List<Channel> findAll(String index, String type) throws IOException {
        log.debug("findAll() size {} - start");

        List<Channel> channels = new ArrayList<>();

        final FieldSortBuilder sort = new FieldSortBuilder("statistics.subscriberCount")
                .setNestedSort(new NestedSortBuilder("statistics"))
                .order(SortOrder.DESC);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.size(10000);
        sourceBuilder.query(matchAllQuery());
        sourceBuilder.sort(sort);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(index);
        searchRequest.types(type);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);
        final SearchHit[] hits = search.getHits().getHits();

        for (SearchHit hit : hits) {
            final Channel channel = objectMapper.readValue(hit.getSourceAsString(), Channel.class);
            channels.add(channel);
        }

        log.debug("findAll() hits {} - end", hits.length);
        return channels;
    }
}
