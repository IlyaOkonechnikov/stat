package tv.adhive.cloud.statistics.scoring;

import feign.Feign;
import feign.Logger;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tv.adhive.cloud.statistics.minter.client.MinterIo;
import tv.adhive.cloud.statistics.minter.pojo.DataUnit;
import tv.adhive.cloud.statistics.minter.pojo.city.CitiesStatisticsItem;
import tv.adhive.cloud.statistics.minter.pojo.follower.FollowersValues;
import tv.adhive.cloud.statistics.minter.pojo.post.PostsValues;
import tv.adhive.cloud.statistics.minter.request.ReportRequest;
import tv.adhive.cloud.statistics.minter.response.*;
import tv.adhive.cloud.statistics.minter.service.MinterService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class ScoringService {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MinterService.class);

    static class AccessTokenInterceptor implements RequestInterceptor {
        @Override
        public void apply(RequestTemplate template) {
            template.query("access_token", "2dniHIpdAUhM7wXQHT1cfnM8Uq7Sdjel");
        }
    }

    MinterIo minterIo = Feign.builder()
            .encoder(new JacksonEncoder())
            .decoder(new JacksonDecoder())
            .logger(new Logger.JavaLogger().appendToFile("logs/http.log"))
            .logLevel(Logger.Level.FULL)
            .requestInterceptor(new AccessTokenInterceptor())
            .target(MinterIo.class, "https://api.minter.io/v1.0/");

    /**
     * FREQ = Количество публикаций за последние 10 дней.
     * Integer [0; +inf)
     * <p>
     * LIKE_COEFF = Средняя сумма лайков / Кол-во подписчиков.
     * Сумму лайков считаем за посление 10 постов и делим на среднее кол-во подписчиков за последние 10 постов (на момент их публикации).
     * Double [0.0; +inf)
     * <p>
     * COMMENT_COEFF = Средняя сумма комментариев / Кол-во подписчиков.
     * Среднюю сумму комментариев считаем за последний месяц и делим на среднее кол-во подписчиков за последние 10 постов (на момент их публикации).
     * Double [0.0; +inf)
     * <p>
     * MOSCOW_PART = (кол-во подписчиков из Москвы / общее кол-во подписчиков)
     * Double [0.0; 1.0]
     * <p>
     * SPB_PART = (кол-во подписчиков из Санкт-Петербурга / общее кол-во подписчиков)
     * Double [0.0; 1.0]
     * <p>
     * REGION_PART = (кол-во подписчиков из прочих регионов / общее кол-во подписчиков)
     * Double [0.0; 1.0]
     * <p>
     * SUB = кол-во подписчиков (среднее значение по дням за последний месяц) данного лидера мнений
     * Integer [0; +inf)
     */
    public List<ScoringVector> getFeatureVector(List<String> urls) {
        List<ScoringVector> vectors = new ArrayList<>();

        for (String url : urls) {
            final String[] split = url.split("/");
            final String alias = split[split.length - 1];
            final UserDataResponse userData = minterIo.getUserData(alias);

            final ReportRequest reportRequest = new ReportRequest();
            reportRequest.setUserId(userData.getUser().getId());
            final ReportResponse report = minterIo.createReport(reportRequest);

            final LocalDate to = LocalDate.now();
            LocalDate from = to.minusDays(10);

            final String toDate = to.format(DateTimeFormatter.ISO_LOCAL_DATE);
            String dateFrom = from.format(DateTimeFormatter.ISO_LOCAL_DATE);
            final DataResponse numberOfPosts = minterIo.getNumberOfPosts(report.getReportId(), dateFrom, toDate, DataUnit.day);

            from = to.minusDays(30);
            dateFrom = from.format(DateTimeFormatter.ISO_LOCAL_DATE);
            final FollowersResponse totalFollowers = minterIo.getTotalFollowers(report.getReportId(), dateFrom, toDate, DataUnit.day);

            from = to.minusDays(356);
            dateFrom = from.format(DateTimeFormatter.ISO_LOCAL_DATE);
            final CitiesResponse citiesOfFollowers = minterIo.getCitiesOfFollowers(report.getReportId(), dateFrom, toDate, DataUnit.day);

            from = to.minusDays(10);
            dateFrom = from.format(DateTimeFormatter.ISO_LOCAL_DATE);
            final InteractionsResponse interactions = minterIo.getInteractions(report.getReportId(), dateFrom, toDate, DataUnit.day);

            ScoringVector vector = new ScoringVector();

            AtomicInteger pub = getPublications(numberOfPosts);
            vector.setFreq(pub.get());

            int sub = getAvgSubscribers(totalFollowers);
            vector.setSub(sub);

            vector.setMoscowPart(getMoscow(citiesOfFollowers) / 100);

            vector.setSpbPart(getSaintPetersburg(citiesOfFollowers) / 100);

            vector.setRegionPart(1D - vector.getMoscowPart() - vector.getSpbPart());

            vector.setCommentCoeff((double) interactions.getComments() / (double) sub);

            vector.setLikeCoeff((double) interactions.getLikes() / (double) sub);

            vectors.add(vector);
        }

        return vectors;
    }

    public Double getMoscow(CitiesResponse citiesOfFollowers) {
        final List<CitiesStatisticsItem> series = citiesOfFollowers.getData().getSeries();
        if (series != null) {
            for (CitiesStatisticsItem item : series) {
                if ("Moscow".equalsIgnoreCase(item.getName())) {
                    return item.getY();
                }
            }
        }
        return 0D;
    }

    public Double getSaintPetersburg(CitiesResponse citiesOfFollowers) {
        final List<CitiesStatisticsItem> series = citiesOfFollowers.getData().getSeries();
        if (series != null) {
            for (CitiesStatisticsItem item : series) {
                if ("Saint Petersburg".equalsIgnoreCase(item.getName())) {
                    return item.getY();
                }
            }
        }
        return 0D;
    }

    public AtomicInteger getPublications(DataResponse numberOfPosts) {
        final PostsValues postsValues = numberOfPosts.getPostsData().getPostsValues();
        final Map<String, Integer> carousel = postsValues.getCarousel();
        final Map<String, Integer> photo = postsValues.getPhoto();
        final Map<String, Integer> video = postsValues.getVideo();

        AtomicInteger pub = new AtomicInteger(0);

        carousel.forEach((k, v) -> {
            if (v != null) pub.getAndAdd(v);
        });

        photo.forEach((k, v) -> {
            if (v != null) pub.getAndAdd(v);
        });

        video.forEach((k, v) -> {
            if (v != null) pub.getAndAdd(v);
        });

        return pub;
    }

    public int getAvgSubscribers(FollowersResponse totalFollowers) {
        final FollowersValues values = totalFollowers.getFollowersData().getFollowersValues();
        final Map<String, Integer> followers = values.getFollowers();

        AtomicInteger sub = new AtomicInteger(0);
        AtomicInteger days = new AtomicInteger(0);
        followers.forEach((k, v) -> {
            if (v != null) {
                sub.getAndAdd(v);
                days.incrementAndGet();
            }
        });
        log.info(followers.toString());
        return sub.intValue() / days.intValue();
    }
}
