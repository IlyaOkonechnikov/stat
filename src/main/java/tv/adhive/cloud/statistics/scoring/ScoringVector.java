package tv.adhive.cloud.statistics.scoring;

import lombok.Data;

@Data
public class ScoringVector {

    // = Количество публикаций за последние 10 дней.
    Integer freq;

    // = Средняя сумма лайков / Кол-во подписчиков.
    Double likeCoeff;

    // = Средняя сумма комментариев / Кол-во подписчиков.
    Double commentCoeff;

    // = (кол-во подписчиков из Москвы / общее кол-во подписчиков)
    Double moscowPart;

    // = (кол-во подписчиков из Санкт-Петербурга / общее кол-во подписчиков)
    Double spbPart;

    //  = (кол-во подписчиков из прочих регионов / общее кол-во подписчиков)
    Double regionPart;

    // = кол-во подписчиков (среднее значение по дням за последний месяц) данного лидера мнений
    Integer sub;

    public Integer getFreq() {
        return freq;
    }

    public void setFreq(Integer freq) {
        this.freq = freq;
    }

    public Double getLikeCoeff() {
        return likeCoeff;
    }

    public void setLikeCoeff(Double likeCoeff) {
        this.likeCoeff = likeCoeff;
    }

    public Double getCommentCoeff() {
        return commentCoeff;
    }

    public void setCommentCoeff(Double commentCoeff) {
        this.commentCoeff = commentCoeff;
    }

    public Double getMoscowPart() {
        return moscowPart;
    }

    public void setMoscowPart(Double moscowPart) {
        this.moscowPart = moscowPart;
    }

    public Double getSpbPart() {
        return spbPart;
    }

    public void setSpbPart(Double spbPart) {
        this.spbPart = spbPart;
    }

    public Double getRegionPart() {
        return regionPart;
    }

    public void setRegionPart(Double regionPart) {
        this.regionPart = regionPart;
    }

    public Integer getSub() {
        return sub;
    }

    public void setSub(Integer sub) {
        this.sub = sub;
    }
}
