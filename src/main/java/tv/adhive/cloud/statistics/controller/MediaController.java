package tv.adhive.cloud.statistics.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import tv.adhive.cloud.statistics.filter.media.CheckWordsCriteria;
import tv.adhive.cloud.statistics.filter.media.Media;
import tv.adhive.cloud.statistics.filter.media.MediaResponse;
import tv.adhive.cloud.statistics.model.LongTailMediaStatistics;
import tv.adhive.cloud.statistics.service.ChannelService;
import tv.adhive.cloud.statistics.service.MediaService;
import tv.adhive.model.media.request.TypeMediaSource;
import tv.adhive.model.media.statistics.MediaStatistics;
import tv.adhive.model.media.statistics.MediaStatisticsItem;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping(value = "/media")
public class MediaController {

    static final Logger log = LoggerFactory.getLogger(MediaController.class);

    static final Map<String, Long> map = new ConcurrentHashMap<>();

    @Autowired
    private MediaService mediaService;

    @Autowired
    private ChannelService channelService;

    @Value("${statistics.media.statistics.fake:false}")
    private boolean fakeStatistics;

    @GetMapping("/findById/{type}/{id}")
    public MediaStatistics findById(@PathVariable("type") TypeMediaSource type, @PathVariable("id") String mediaId) throws IOException {
        log.debug("findById() - start, id {}", mediaId);
        MediaStatistics mediaStatistics = mediaService.findById(type, mediaId);
        log.debug("findById() - end, stat {}", mediaStatistics);
        return mediaStatistics;
    }

    @GetMapping("/statistics/findById/{type}/{id}")
    public MediaStatisticsItem findStatisticsByMediaId(@PathVariable("type") TypeMediaSource type, @PathVariable("id") String mediaId) throws IOException {
        log.debug("findStatisticsByMediaId() - start, id {}", mediaId);

        MediaStatisticsItem mediaStatisticsItem = null;
        if (fakeStatistics) {
            Long step = map.get(mediaId);
            if (step == null) {
                step = 1L;
                map.put(mediaId, step);
            } else {
                ++step;
                map.put(mediaId, step);
            }
            LongTailMediaStatistics longTailMediaStatistics = new LongTailMediaStatistics(step, 1500L, TypeMediaSource.YOUTUBE == type);
            mediaStatisticsItem = longTailMediaStatistics.getMediaStatisticsItem();
        } else {
            MediaStatistics mediaStatistics = mediaService.findById(type, mediaId);
            if (mediaStatistics != null) {
                mediaStatisticsItem = mediaStatistics.getStatistics();
            }
        }

        log.debug("findStatisticsByMediaId() - end, stat {}", mediaStatisticsItem);
        return mediaStatisticsItem;
    }

    @PostMapping("/checkWords")
    public MediaResponse checkIfChannelContainsWordsInCaption(@RequestBody CheckWordsCriteria criteria) throws IOException {
        log.debug("checkWords with {} - start", criteria);
        List<Media> media = mediaService.checkWordsInCaption(criteria);
        log.debug("checkWords result {} - end", media);
        MediaResponse mediaResponse = new MediaResponse();
        mediaResponse.setMedia(media);
        return mediaResponse;
    }
}
