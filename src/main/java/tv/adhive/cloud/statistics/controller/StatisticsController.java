package tv.adhive.cloud.statistics.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tv.adhive.cloud.statistics.model.Metrics;
import tv.adhive.cloud.statistics.service.AggregationService;
import tv.adhive.cloud.statistics.service.SearchService;
import tv.adhive.cloud.statistics.service.StatisticsService;
import tv.adhive.model.channel.statistics.StatisticsResponse;
import tv.adhive.model.media.request.TypeMediaSource;

import java.io.IOException;

/**
 * Controller for getting {@link StatisticsResponse} and {@link Metrics}.
 */
@RestController
@RequestMapping(value = "/statistics")
public class StatisticsController {

    static final Logger log = LoggerFactory.getLogger(StatisticsController.class);

    private final AggregationService aggregationService;
    private final StatisticsService statisticsService;

    @Autowired
    public StatisticsController(AggregationService aggregationService,
                                StatisticsService statisticsService) {
        this.aggregationService = aggregationService;
        this.statisticsService = statisticsService;
    }

    @PostMapping("/youtube")
    public StatisticsResponse getYoutubeAvgViews(@RequestBody tv.adhive.cloud.statistics.search.Criteria criteria) throws IOException {
        log.debug("getYoutubeAvgViews() - start: criteria = {}", criteria);
        return statisticsService.findAverageViewsAndCpm(criteria);
    }

    @PostMapping("/instagram")
    public StatisticsResponse getInstagramAvgLikes(@RequestBody tv.adhive.cloud.statistics.search.Criteria criteria) throws IOException {
        log.debug("getInstagramAvgLikes() - start: criteria = {}", criteria);
        return statisticsService.findAverageLikesAndCpm(criteria);
    }

    @GetMapping("/metrics/youtube")
    public Metrics getMetricsYoutube() throws IOException {
        return aggregationService.getYoutubeChannelMetrics();
    }

    @GetMapping("/metrics/instagram")
    public Metrics getMetricsInstagram() throws IOException {
        return aggregationService.getInstagramChannelMetrics();
    }

    //TODO: add documentation to atlassian if it fits
    @GetMapping("/channel/er/{type}/{id}")
    public Float findEngagementRate(@PathVariable("type") TypeMediaSource type, @PathVariable("id") String id) throws IOException {
        log.debug("findEngagementRate for {} {} - start", type, id);
        return statisticsService.findEngagementRateByChannel(type, id);
    }
}
