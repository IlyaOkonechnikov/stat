package tv.adhive.cloud.statistics.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tv.adhive.cloud.statistics.repository.MediaRepository;
import tv.adhive.model.media.request.TypeMediaSource;
import tv.adhive.model.media.statistics.MediaStatistics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static tv.adhive.cloud.statistics.Constants.*;

@RestController
public class BotController {

    @Autowired
    MediaRepository mediaRepository;

    @PostMapping("/bot/media/{type}")
    public void save(@PathVariable("type") TypeMediaSource type, @RequestBody MediaStatistics mediaStatistics) throws IOException {
        String index = YOUTUBE_MEDIASTATISTICS_INDEX;
        String typeE = YOUTUBE_MEDIASTATISTICS_TYPE;

        if (TypeMediaSource.YOUTUBE == type) {
            index = YOUTUBE_MEDIASTATISTICS_INDEX;
            typeE = YOUTUBE_MEDIASTATISTICS_TYPE;
        } else if (TypeMediaSource.INSTAGRAM == type) {
            index = INSTAGRAM_MEDIASTATISTICS_INDEX;
            typeE = INSTAGRAM_MEDIASTATISTICS_TYPE;
        }

        mediaRepository.save(index, typeE, mediaStatistics);
    }

    @GetMapping("/bot/media/{type}/{channelId}")
    public List<MediaStatistics> getMediaIds(@PathVariable("type") TypeMediaSource type, @PathVariable("channelId") String channelId) throws IOException {
        String indexE = YOUTUBE_MEDIASTATISTICS_INDEX;
        String typeE = YOUTUBE_MEDIASTATISTICS_TYPE;

        if (TypeMediaSource.YOUTUBE == type) {
            indexE = YOUTUBE_MEDIASTATISTICS_INDEX;
            typeE = YOUTUBE_MEDIASTATISTICS_TYPE;
        } else if (TypeMediaSource.INSTAGRAM == type) {
            indexE = INSTAGRAM_MEDIASTATISTICS_INDEX;
            typeE = INSTAGRAM_MEDIASTATISTICS_TYPE;
        }
        return mediaRepository.findAllByChannelId(indexE, typeE, channelId);
    }
}
