package tv.adhive.cloud.statistics.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tv.adhive.cloud.statistics.filter.data.ChannelData;
import tv.adhive.cloud.statistics.search.Criteria;
import tv.adhive.cloud.statistics.service.SearchService;
import tv.adhive.model.channel.Channel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static tv.adhive.cloud.statistics.Constants.MEDIA_AMOUNT;

@RestController
@RequestMapping(value = "/influencer")
public class InfluencerController {

    static final Logger log = LoggerFactory.getLogger(InfluencerController.class);

    static final ObjectMapper MAPPER = new ObjectMapper();

    private final SearchService searchService;

    @Autowired
    public InfluencerController(SearchService searchService) {
        this.searchService = searchService;
    }

    @PostMapping("/list")
    public List<ChannelData> getChannelsByCriteria(@RequestBody Criteria criteria) throws IOException {
        log.debug("getChannelsByCriteria() - start: criteria = {}", criteria);
        final List<String> channelsByCriteria = searchService.findChannelsByCriteria(criteria);

        List<ChannelData> channelDataList = new ArrayList<>(channelsByCriteria.size());

        for (String channelAsString : channelsByCriteria) {
            final Channel channel = MAPPER.readValue(channelAsString, Channel.class);

            ChannelData data = new ChannelData();
            data.setId(channel.getId());

            if ("YOUTUBE".equalsIgnoreCase(criteria.getType())) {
                if (channel.getStatistics() != null && channel.getStatistics().getPublicationCount() > 0)
                    data.setCoverage(channel.getStatistics().getViewCount() / channel.getStatistics().getPublicationCount());
            } else if ("INSTAGRAM".equalsIgnoreCase(criteria.getType())) {
                if (channel.getStatistics() != null)
                    data.setCoverage(channel.getStatistics().getLikeCount() / MEDIA_AMOUNT);
            }

            channelDataList.add(data);
        }

        log.debug("getChannelsByCriteria() - end: resultSize = {} criteria = {}", channelDataList.size(), criteria);
        return channelDataList;
    }
}
