package tv.adhive.cloud.statistics.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tv.adhive.cloud.channel.api.channel.ChannelNotFoundException;
import tv.adhive.cloud.statistics.service.ChannelService;
import tv.adhive.model.channel.Channel;

@RestController
public class ChannelController {

    static final Logger log = LoggerFactory.getLogger(ChannelController.class);

    private final ChannelService channelService;

    @Autowired
    public ChannelController(ChannelService channelService) {
        this.channelService = channelService;
    }

    /**
     * Saves common channel statistics.
     *
     * @param mediaAmount amount of last videos
     * @param channel     {@link Channel}
     */
    @PostMapping("/statistics/channel/{mediaAmount}")
    public void register(@RequestBody Channel channel, @PathVariable Integer mediaAmount) throws ChannelNotFoundException {
        log.debug("registerCommonStatistics() - start: statistics = {}", channel);
        channelService.registerChannel(channel, mediaAmount);
        log.debug("registerCommonStatistics() - end");
    }

    /**
     * Updates common channel statistics.
     *
     * @param mediaAmount amount of last videos
     * @param channel     {@link Channel}
     */
    @PutMapping("/statistics/channel/{mediaAmount}")
    public void update(@RequestBody Channel channel, @PathVariable Integer mediaAmount) throws ChannelNotFoundException {
        log.debug("updateCommonStatistics() - start: statistics = {}", channel);
        channelService.updateChannel(channel, mediaAmount);
        log.debug("updateCommonStatistics() - end");
    }

    /**
     * Delete common channel statistics.
     *
     * @param type
     * @param id
     */
    @DeleteMapping("/statistics/channel/{type}/{id}")
    public void unregister(@PathVariable("type") String type, @PathVariable("id") String id) {
        log.debug("unregisterCommonStatistics() - start: type = {}; id = {}", type, id);
        channelService.unregisterChannel(type, id);
        log.debug("unregisterCommonStatistics() - end: type = {}; id = {}", type, id);
    }
}
