package tv.adhive.cloud.statistics.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tv.adhive.cloud.statistics.filter.media.Media;
import tv.adhive.cloud.statistics.filter.media.MediaCriteria;
import tv.adhive.cloud.statistics.filter.media.MediaResponse;
import tv.adhive.cloud.statistics.filter.statistics.StatisticsCriteria;
import tv.adhive.cloud.statistics.filter.statistics.StatisticsResponse;
import tv.adhive.cloud.statistics.service.AggregationService;
import tv.adhive.cloud.statistics.service.MediaService;
import tv.adhive.cloud.statistics.service.SearchService;
import tv.adhive.model.media.request.TypeMediaSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping(value = "/analytics")
public class AnalyticsController {

    static final Logger log = LoggerFactory.getLogger(AnalyticsController.class);

    private final AggregationService aggregationService;

    private final MediaService mediaService;

    private final SearchService searchService;

    @Value("${statistics.media.analytics.fake:false}")
    private boolean fakeAnalytics;

    @Autowired
    public AnalyticsController(AggregationService aggregationService, MediaService mediaService, SearchService searchService) {
        this.aggregationService = aggregationService;
        this.mediaService = mediaService;
        this.searchService = searchService;
    }

    @PostMapping("/media")
    public MediaResponse getChannelMedia(@RequestBody MediaCriteria criteria) throws IOException {
        log.debug("getChannelMedia() - start: criteria = {}", criteria);

        MediaResponse response = new MediaResponse();

        if (fakeAnalytics) {
            final List<Media> mediaList = getFakeMedia(criteria);
            response.setMedia(mediaList);
        } else {
            if ("YOUTUBE".equalsIgnoreCase(criteria.getType())) {
                final List<Media> media = mediaService.getYoutubeAnalyticsMedia(criteria);
                response.setMedia(media);
            } else if ("INSTAGRAM".equalsIgnoreCase(criteria.getType())) {
                final List<Media> media = mediaService.getInstagramAnalyticsMedia(criteria);
                response.setMedia(media);
            }
        }

        log.debug("getChannelMedia() - end: criteria = {}", criteria);
        return response;
    }

    @PostMapping("/statistics")
    public StatisticsResponse getChannelsStatistics(@RequestBody StatisticsCriteria criteria) throws IOException {
        log.debug("getChannelsStatistics() - start: criteria = {}", criteria);
        StatisticsResponse response = new StatisticsResponse();

        if (criteria != null && criteria.getChannels() != null) {
            if ("YOUTUBE".equalsIgnoreCase(criteria.getType())) {
                final long averageViews = aggregationService.getAvgViewsByChannelIds(criteria.getChannels());
                response.setCoverage(averageViews * criteria.getChannels().size());
            } else if ("INSTAGRAM".equalsIgnoreCase(criteria.getType())) {
                final long averageLikes = aggregationService.getAvgLikesByChannelIds(criteria.getChannels());
                response.setCoverage(averageLikes * criteria.getChannels().size());
            }
        }
        log.debug("getChannelsStatistics() - end: response = {}", response);
        return response;
    }

    @Value("${statistics.media.analytics.fake_coeficient:100}")
    private int fakeCoeficient;

    private AtomicInteger fakeCounter = new AtomicInteger(0);

    private Map<String, Media> validFakes = new ConcurrentHashMap<>();

    private Map<String, Media> invalidFakes = new ConcurrentHashMap<>();

    private List<Media> getFakeMedia(@RequestBody MediaCriteria criteria) {
        if (fakeCoeficient == 0) {
            return new ArrayList<>();
        } else if (fakeCoeficient == 50) {
            if (validFakes.containsKey(criteria.getChannelId())) {
                final List<Media> mediaList = new ArrayList<>();
                mediaList.add((validFakes.get(criteria.getChannelId())));
                return mediaList;
            }
            if (invalidFakes.containsKey(criteria.getChannelId())) {
                return new ArrayList<>();
            }

            final int i = fakeCounter.incrementAndGet();
            if (i % 2 != 0) {
                final List<Media> mediaList = new ArrayList<>();
                validFakes.put(criteria.getChannelId(), createFakeMedia(criteria));
                mediaList.add((validFakes.get(criteria.getChannelId())));
                return mediaList;
            } else {
                invalidFakes.put(criteria.getChannelId(), createFakeMedia(criteria));
                return new ArrayList<>();
            }
        } else {
            final List<Media> mediaList = new ArrayList<>();
            mediaList.add(createFakeMedia(criteria));
            return mediaList;
        }
    }

    private Media createFakeMedia(@RequestBody MediaCriteria criteria) {
        final Media media = new Media();
        media.setId("fake_" + criteria.getChannelId());
        if ("YOUTUBE".equalsIgnoreCase(criteria.getType())) {
            media.setType(TypeMediaSource.YOUTUBE);
        } else if ("INSTAGRAM".equalsIgnoreCase(criteria.getType())) {
            media.setType(TypeMediaSource.INSTAGRAM);
        }
        return media;
    }
}
