package tv.adhive.cloud.statistics.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tv.adhive.cloud.statistics.integration.CompframeworkReceiver;
import tv.adhive.cloud.statistics.service.ElasticService;
import tv.adhive.model.media.response.AIStatistics;
import tv.adhive.model.media.response.Source;
import tv.adhive.model.media.response.Tag;
import tv.adhive.model.smart.MediaType;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/aistatistics")
public class AiStatisticsController {

    private final ElasticService elasticService;

    private final CompframeworkReceiver compframeworkReceiver;

    @Autowired
    public AiStatisticsController(ElasticService elasticService, CompframeworkReceiver compframeworkReceiver) {
        this.elasticService = elasticService;
        this.compframeworkReceiver = compframeworkReceiver;
    }

    @PostMapping("/create")
    public void registerAiStatistics(@RequestBody AIStatistics aiStatistics) {
        compframeworkReceiver.registerAIStatistics(aiStatistics);
    }

    @GetMapping("/{type}/{id}")
    public AIStatistics aiStatistics(@PathVariable("type") String type, @PathVariable("id") String id) {
        return elasticService.findById(id, AIStatistics.class, type);
    }

    @GetMapping("/{type}/{id}/sources/tags/")
    public List<Tag> tags(@PathVariable("type") String type, @PathVariable("id") String id) {
        final AIStatistics statistics = elasticService.findById(id, AIStatistics.class, type);

        List<Tag> tags = new ArrayList<>();
        if (statistics != null && statistics.getSources() != null) {
            for (Source source : statistics.getSources()) {
                if (source.getTags() != null) {
                    tags.addAll(source.getTags());
                }
            }
        }
        return tags;
    }

    @GetMapping("/{type}/{id}/sources/{sourceType}/tags/")
    public List<Tag> tags(@PathVariable("type") String type, @PathVariable("sourceType") MediaType sourceType, @PathVariable("id") String id) {
        final AIStatistics statistics = elasticService.findById(id, AIStatistics.class, type);

        List<Tag> tags = new ArrayList<>();
        if (statistics.getSources() != null) {
            for (Source source : statistics.getSources()) {
                if (source.getType() != null
                        && source.getType().equalsIgnoreCase(sourceType.getType())
                        && source.getTags() != null) {
                    tags.addAll(source.getTags());
                }
            }
        }
        return tags;
    }
}
