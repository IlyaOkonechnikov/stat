package tv.adhive.cloud.statistics.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tv.adhive.cloud.statistics.minter.service.ReportsScheduler;
import tv.adhive.cloud.statistics.service.SchedulerService;

import java.io.IOException;

@RestController
@RequestMapping(value = "/scheduler-api")
public class SchedulerController {

    static final Logger log = LoggerFactory.getLogger(SchedulerController.class);

    private final SchedulerService schedulerService;
    private final ReportsScheduler reportsScheduler;

    @Autowired
    public SchedulerController(SchedulerService schedulerService, ReportsScheduler reportsScheduler) {
        this.schedulerService = schedulerService;
        this.reportsScheduler = reportsScheduler;
    }

    @GetMapping("/update-all")
    public String updateAllChannels() throws IOException {
        log.debug("updateAllChannels() - start");
        schedulerService.updateAllChannels();
        log.debug("updateAllChannels() - end");
        return "true";
    }

    @GetMapping("/update-unprocessed-media")
    public String updateUnprocessedMedia() throws IOException {
        log.debug("updateUnprocessedMedia() - start");
        schedulerService.updateUnprocessedMedia();
        log.debug("updateUnprocessedMedia() - end");
        return "true";
    }

    @GetMapping("/create-reports")
    public String createReports() throws IOException {
        log.info("createReports - start");
        reportsScheduler.createReports();
        log.info("createReports - end");
        return "true";
    }

    @GetMapping("/update-reports")
    public String updateReports() throws IOException {
        log.info("updateReports - start");
        reportsScheduler.updateReports();
        log.info("updateReports - end");
        return "true";
    }
}
