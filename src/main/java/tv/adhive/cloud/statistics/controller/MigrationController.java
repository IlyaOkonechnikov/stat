package tv.adhive.cloud.statistics.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import tv.adhive.cloud.statistics.component.DataMigration;

import java.io.IOException;

@Controller
public class MigrationController {

    private final DataMigration dataMigration;

    @Autowired
    public MigrationController(DataMigration dataMigration) {
        this.dataMigration = dataMigration;
    }

    @GetMapping("/migration/addTags")
    public void addTags() throws IOException {
        dataMigration.addTags();
    }
}
