package tv.adhive.cloud.statistics.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.stream.Stream;

@Slf4j
public final class FileUtil {

    private FileUtil() {
    }

    public static String readFileToString(File file) {
        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines(file.toPath(), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e) {
            log.error("error while reading file, e = {}", e);
        }

        return contentBuilder.toString();
    }
}
