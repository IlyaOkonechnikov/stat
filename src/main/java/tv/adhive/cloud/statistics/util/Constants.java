package tv.adhive.cloud.statistics.util;

public final class Constants {

    public static final String CHANNEL_ID = "channelId";
    public static final String CHANNEL_TYPE = "channelType";
    public static final String MEDIA_AMOUNT = "mediaAmount";

    public static final String INDEX_POSTFIX = "-index";
    public static final String INDEX = "index";
    public static final String TYPE = "type";
    public static final String SEPARATOR = "-";

    private Constants() {
    }
}
