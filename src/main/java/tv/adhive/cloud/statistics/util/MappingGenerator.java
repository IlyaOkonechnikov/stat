package tv.adhive.cloud.statistics.util;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.xcontent.NamedXContentRegistry;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 * Class for generating elastic index mapping.
 */
@Slf4j
public class MappingGenerator {

    private MappingGenerator() {
    }

    /**
     * Generates json mapping structure for elastic index
     *
     * @param json json mapping
     * @return {@link XContentBuilder} mapping
     * @throws IOException exception while creating parser
     */
    public static XContentBuilder generate(String json) throws IOException {
        log.debug("generate() - start, json = {}", json);
        XContentParser parser = XContentFactory.xContent(XContentType.JSON)
                .createParser(NamedXContentRegistry.EMPTY, json.getBytes());
        parser.close();
        XContentBuilder builder = jsonBuilder().copyCurrentStructure(parser);
        log.debug("generate() - end, content builder = {}", builder);
        return builder;
    }
}
