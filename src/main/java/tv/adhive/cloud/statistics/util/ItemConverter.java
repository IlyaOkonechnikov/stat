package tv.adhive.cloud.statistics.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import tv.adhive.cloud.statistics.model.ElasticItem;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.media.response.AIStatistics;
import tv.adhive.model.media.statistics.MediaStatistics;

import java.util.HashMap;
import java.util.Map;

import static tv.adhive.cloud.statistics.util.Constants.INDEX;
import static tv.adhive.cloud.statistics.util.Constants.INDEX_POSTFIX;
import static tv.adhive.cloud.statistics.util.Constants.SEPARATOR;
import static tv.adhive.cloud.statistics.util.Constants.TYPE;

@Slf4j
public class ItemConverter {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private ItemConverter() {
    }

    /**
     * Converts incomint data object into {@link ElasticItem}
     *
     * @param analytics data object
     * @return {@link ElasticItem}
     */
    public static ElasticItem convert(Object analytics, String type) {
        String id = StringUtils.EMPTY;

        if (analytics instanceof Channel) {
            id = ((Channel) analytics).getId();
        } else if (analytics instanceof MediaStatistics) {
            id = ((MediaStatistics) analytics).getId();
        } else if (analytics instanceof AIStatistics) {
            id = ((AIStatistics) analytics).getId();
        }

        final Map<String, String> documentMeta =
                getDocumentMeta(type + SEPARATOR + analytics.getClass().getSimpleName());

        String dataAsJson = StringUtils.EMPTY;
        try {
            dataAsJson = MAPPER.writeValueAsString(analytics);
        } catch (JsonProcessingException e) {
            log.error("error = {}", e);
        }

        return new ElasticItem()
                .setIndex(documentMeta.get(INDEX))
                .setType(documentMeta.get(TYPE))
                .setData(dataAsJson)
                .setId(id);
    }

    public static Map<String, String> getDocumentMeta(String type) {
        Map<String, String> result = new HashMap<>();

        String index = type + INDEX_POSTFIX;

        result.put(INDEX, index.toLowerCase());
        result.put(TYPE, type.toLowerCase());
        return result;
    }
}
