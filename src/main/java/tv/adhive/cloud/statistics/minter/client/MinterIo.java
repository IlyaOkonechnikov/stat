package tv.adhive.cloud.statistics.minter.client;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import tv.adhive.cloud.statistics.minter.pojo.Report;
import tv.adhive.cloud.statistics.minter.pojo.country.CountriesResponse;
import tv.adhive.cloud.statistics.minter.pojo.DataUnit;
import tv.adhive.cloud.statistics.minter.request.AccessTokenRequest;
import tv.adhive.cloud.statistics.minter.request.ReportRequest;
import tv.adhive.cloud.statistics.minter.response.*;

public interface MinterIo {

//https://minter.io/oauth/token

    /**
     * Getting the Access Token
     * Once your application has completed the above section and gotten an authorization code, it’ll now need to exchange the authorization code for an access token from Minter.io.
     * <p>
     * To get the access_token, you’ll need to make a POST request to https://minter.io/oauth/token with the following parameters:
     */
    @Headers("Content-Type: application/json")
    @RequestLine("POST /oauth/token")
    AccessTokenResponse getAccessToken(AccessTokenRequest accessToken);

//https://api.minter.io/v1.0/instagram/users/username

    /**
     * Instagram User data
     * <p>
     * Get user data from Instagram
     */
    @Headers("Content-Type: application/json")
    @RequestLine("GET /instagram/users/{user}")
    UserDataResponse getUserData(@Param("user") String user);

//    https://api.minter.io/v1.0/reports/

    /**
     * Reports
     * <p>
     * Get a list of all available reports.
     */
    @Headers({"Content-Type: application/json"})
    @RequestLine("GET /reports/")
    ReportsResponse getAllReports();//    https://api.minter.io/v1.0/reports/

//    https://api.minter.io/v1.0/reports/

    /**
     * Reports
     * <p>
     * Create a report for an Instagram User or Instagram Hashtag.
     */
    @Headers({"Content-Type: application/json"})
    @RequestLine("POST /reports/")
    ReportResponse createReport(ReportRequest reportRequest);

//    https://api.minter.io/v1.0/reports/report_id

    /**
     * Report detailed
     * <p>
     * Get information about a report.
     */
    @Headers({"Content-Type: application/json"})
    @RequestLine("GET /reports/{report_id}")
    Report getReport(@Param("report_id") String reportId);


    //    https://api.minter.io/v1.0/reports/report_id

    /**
     * Report detailed
     * <p>
     * Remove a report.
     */
    @Headers({"Content-Type: application/json"})
    @RequestLine("DELETE /reports/{report_id}")
    SuccessResponse deleteReport(@Param("report_id") String reportId);

    /**
     * 1.1 FREQ
     * <p>
     * Number of Posts
     * <p>
     * The total number of Posts during a selected time range.
     * <p>
     * https://api.minter.io/v1.0/reports/report_id/number_posts
     */
    @Headers({"Content-Type: application/json"})
    @RequestLine("GET /reports/{report_id}/number_posts?date_from={date_from}&to_date={to_date}&unit={unit}")
    DataResponse getNumberOfPosts(@Param("report_id") String reportId, @Param("date_from") String dateFrom, @Param("to_date") String toDate, @Param("unit") DataUnit unit);

    /**
     * 1.3 REG
     * <p>
     * Cities of Followers
     * <p>
     * The distribution of cities of Followers during a selected time range.
     * <p>
     * https://api.minter.io/v1.0/reports/report_id/cities_followers
     */
    @Headers({"Content-Type: application/json"})
    @RequestLine("GET /reports/{report_id}/cities_followers?date_from={date_from}&to_date={to_date}&unit={unit}")
    CitiesResponse getCitiesOfFollowers(@Param("report_id") String reportId, @Param("date_from") String dateFrom, @Param("to_date") String toDate, @Param("unit") DataUnit unit);

    /**
     * 1.4. SUB
     * <p>
     * Total Followers
     * <p>
     * The number of profile's Followers during a selected time range.
     * <p>
     * https://api.minter.io/v1.0/reports/report_id/total_followers
     */
    @Headers({"Content-Type: application/json"})
    @RequestLine("GET /reports/{report_id}/total_followers?date_from={date_from}&to_date={to_date}&unit={unit}")
    FollowersResponse getTotalFollowers(@Param("report_id") String reportId, @Param("date_from") String dateFrom, @Param("to_date") String toDate, @Param("unit") DataUnit unit);

    /**
     * Countries of Followers
     * <p>
     * The distribution of countries of Followers during a selected time range.
     * <p>
     * https://api.minter.io/v1.0/reports/report_id/countries_followers
     */
    @Headers({"Content-Type: application/json"})
    @RequestLine("GET /reports/{report_id}/countries_followers?date_from={date_from}&to_date={to_date}&unit={unit}")
    CountriesResponse getCountriesOfFollowers(@Param("report_id") String reportId, @Param("date_from") String dateFrom, @Param("to_date") String toDate, @Param("unit") DataUnit unit);

    /**
     * Gender of Followers
     * <p>
     * The distribution of gender of Followers during a selected time range.
     * <p>
     * https://api.minter.io/v1.0/reports/report_id/gender_followers
     */
    @Headers({"Content-Type: application/json"})
    @RequestLine("GET /reports/{report_id}/gender_followers?date_from={date_from}&to_date={to_date}&unit={unit}")
    GendersResponse getGenderOfFollowers(@Param("report_id") String reportId, @Param("date_from") String dateFrom, @Param("to_date") String toDate, @Param("unit") DataUnit unit);

    /**
     * Language of Followers
     * <p>
     * The distribution of languages of Followers during a selected time range.
     * <p>
     * https://api.minter.io/v1.0/reports/report_id/lang_followers
     */
    @Headers({"Content-Type: application/json"})
    @RequestLine("GET /reports/{report_id}/lang_followers?date_from={date_from}&to_date={to_date}&unit={unit}")
    LanguagesResponse getLanguageOfFollowers(@Param("report_id") String reportId, @Param("date_from") String dateFrom, @Param("to_date") String toDate, @Param("unit") DataUnit unit);

    /**
     * Interactions
     * <p>
     * The total number of Likes and Comments by the post type.
     * <p>
     * https://api.minter.io/v1.0/reports/report_id/interactions
     */
    @Headers({"Content-Type: application/json"})
    @RequestLine("GET /reports/{report_id}/interactions?date_from={date_from}&to_date={to_date}&unit={unit}")
    InteractionsResponse getInteractions(@Param("report_id") String reportId, @Param("date_from") String dateFrom, @Param("to_date") String toDate, @Param("unit") DataUnit unit);
}
