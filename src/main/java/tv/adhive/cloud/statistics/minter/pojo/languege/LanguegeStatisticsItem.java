package tv.adhive.cloud.statistics.minter.pojo.languege;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class LanguegeStatisticsItem {
    @JsonProperty("data")
    private List<Double> data;

    @JsonProperty("name")
    private String name;
}