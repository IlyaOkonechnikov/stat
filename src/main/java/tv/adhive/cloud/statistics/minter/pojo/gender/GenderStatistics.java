package tv.adhive.cloud.statistics.minter.pojo.gender;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class GenderStatistics {

    @JsonProperty("series")
    private List<GenderStatisticsItem> series;
}