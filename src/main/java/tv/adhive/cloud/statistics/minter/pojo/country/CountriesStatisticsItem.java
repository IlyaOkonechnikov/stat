package tv.adhive.cloud.statistics.minter.pojo.country;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CountriesStatisticsItem {

    @JsonProperty("name")
    private String name;

    @JsonProperty("y")
    private Double y;
}