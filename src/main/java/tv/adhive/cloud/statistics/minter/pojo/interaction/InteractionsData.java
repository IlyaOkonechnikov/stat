package tv.adhive.cloud.statistics.minter.pojo.interaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class InteractionsData {

    @JsonProperty("series")
    private List<String> series;

    @JsonProperty("values")
    private InteractionsValues interactionsValues;
}