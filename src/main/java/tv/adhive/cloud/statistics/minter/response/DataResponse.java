package tv.adhive.cloud.statistics.minter.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import tv.adhive.cloud.statistics.minter.pojo.post.PostsData;

@lombok.Data
public class DataResponse {

    @JsonProperty("y_label")
    private String yLabel;

    @JsonProperty("data")
    private PostsData postsData;
}