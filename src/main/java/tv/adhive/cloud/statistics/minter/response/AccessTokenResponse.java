package tv.adhive.cloud.statistics.minter.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AccessTokenResponse {

    /**
     * <ACCESS_TOKEN>
     */
    @JsonProperty("access_token")
    private String accessToken;

    /**
     * token_type
     */
    @JsonProperty("token_type")
    private String tokenType = "Bearer";

    /**
     * <REFRESH_TOKEN>
     */
    @JsonProperty("refresh_token")
    private String refreshToken;

    /**
     * scope
     */
    @JsonProperty("scope")
    private String scope = "basic";
}
