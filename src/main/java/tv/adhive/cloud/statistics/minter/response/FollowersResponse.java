package tv.adhive.cloud.statistics.minter.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import tv.adhive.cloud.statistics.minter.pojo.follower.FollowersData;

@lombok.Data
public class FollowersResponse {

    @JsonProperty("y_label")
    private String yLabel;

    @JsonProperty("data")
    private FollowersData followersData;
}
