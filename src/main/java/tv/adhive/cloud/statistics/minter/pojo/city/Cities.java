package tv.adhive.cloud.statistics.minter.pojo.city;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Cities {

    @JsonProperty("y_label")
    private String yLabel;

    @JsonProperty("data")
    private CitiesData data;
}