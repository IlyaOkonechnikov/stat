package tv.adhive.cloud.statistics.minter.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AccessTokenRequest {

    public static final String AUTHORIZATION_CODE = "authorization_code";

    /**
     * required
     * <p>
     * Must be authorization_code
     */
    @JsonProperty("grant_type")
    private String grantType = AUTHORIZATION_CODE;


    /**
     * required
     * <p>
     * The authorization code you retrieved previously
     */
    @JsonProperty("code")
    private String code;


    /**
     * required
     * <p>
     * client_id gotten from Minter.io in Initial Setup
     */
    @JsonProperty("client_id")
    private String clientId;


    /**
     * required
     * <p>
     * client_secret gotten from Minter.io in Initial Setup
     */
    @JsonProperty("client_secret")
    private String clientSecret;


    /**
     * required
     * <p>
     * redirect_uri gotten from Minter.io in Initial Setup
     */
    @JsonProperty("redirect_uri")
    private String redirectUri;
}
