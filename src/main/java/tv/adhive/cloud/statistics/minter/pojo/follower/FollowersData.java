package tv.adhive.cloud.statistics.minter.pojo.follower;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@lombok.Data
public class FollowersData {

    @JsonProperty("series")
    private List<String> series;

    @JsonProperty("values")
    private FollowersValues followersValues;
}