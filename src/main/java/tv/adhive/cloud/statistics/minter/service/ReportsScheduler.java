package tv.adhive.cloud.statistics.minter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author drius on 27.03.2018.
 */
@Component
public class ReportsScheduler {

    private MinterService minterService;

    @Autowired
    public void minterService(MinterService minterService) {
        this.minterService = minterService;
    }

    @Scheduled(fixedDelay = 24 * 60 * 60 * 1000)
    public void createReports() throws IOException {
        minterService.createReports();
    }

    @Scheduled(fixedDelay = 2 * 60 * 60 * 1000)
    public void updateReports() throws IOException {
        minterService.updateMetrics();
    }
}
