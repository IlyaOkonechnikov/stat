package tv.adhive.cloud.statistics.minter.pojo.country;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class CountriesStatistics {

    @JsonProperty("detailed")
    private List<Map<String, Double>> detailed;

    @JsonProperty("series")
    private List<CountriesStatisticsItem> series;
}