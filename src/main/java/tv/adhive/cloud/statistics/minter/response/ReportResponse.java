package tv.adhive.cloud.statistics.minter.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ReportResponse {

    @JsonProperty("report_id")
    private String reportId;
}
