package tv.adhive.cloud.statistics.minter.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tv.adhive.cloud.statistics.minter.pojo.gender.GenderStatistics;

@Data
public class GendersResponse {

    @JsonProperty("y_label")
    private String yLabel;

    @JsonProperty("data")
    private GenderStatistics data;
}
