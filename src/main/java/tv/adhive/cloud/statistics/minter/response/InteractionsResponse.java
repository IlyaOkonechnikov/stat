package tv.adhive.cloud.statistics.minter.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tv.adhive.cloud.statistics.minter.pojo.interaction.InteractionsData;

@Data
public class InteractionsResponse {

    @JsonProperty("comments")
    private Integer comments;

    @JsonProperty("posts")
    private Integer posts;

    @JsonProperty("likes")
    private Integer likes;

    @JsonProperty("y_label")
    private String yLabel;

    @JsonProperty("data")
    private InteractionsData interactionsData;
}
