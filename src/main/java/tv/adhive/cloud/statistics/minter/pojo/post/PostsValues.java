package tv.adhive.cloud.statistics.minter.pojo.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class PostsValues {

    @JsonProperty("Carousel")
    private Map<String, Integer> carousel;

    @JsonProperty("Video")
    private Map<String, Integer> video;

    @JsonProperty("Photo")
    private Map<String, Integer> photo;
}