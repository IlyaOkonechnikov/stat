package tv.adhive.cloud.statistics.minter.pojo.country;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Countries {

    @JsonProperty("y_label")
    private String yLabel;

    @JsonProperty("data")
    private CountriesData data;
}