package tv.adhive.cloud.statistics.minter.pojo.follower;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class FollowersValues {

    @JsonProperty("Followers")
    private Map<String, Integer> followers;
}