package tv.adhive.cloud.statistics.minter.service.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class LocalDateTuple {

        private LocalDate to;
        private LocalDate from;

        public LocalDateTuple(int minusDays) {
            to = LocalDate.now();
            from = to.minus(minusDays, ChronoUnit.DAYS);
        }

        public LocalDate getFrom() {
            return from;
        }

        public LocalDate getTo() {
            return to;
        }

        public String getStringTo() {
            return to.format(DateTimeFormatter.ISO_DATE);
        }

        public String getStringFrom() {
            return from.format(DateTimeFormatter.ISO_DATE);
        }
    }