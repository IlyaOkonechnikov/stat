package tv.adhive.cloud.statistics.minter.pojo.interaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class InteractionsValues {

    @JsonProperty("Carousel Interactions")
    private Map<String, Integer> carouselInteractions;

    @JsonProperty("Photo Interactions")
    private Map<String, Integer> photoInteractions;

    @JsonProperty("Video Interactions")
    private Map<String, Integer> videoInteractions;
}