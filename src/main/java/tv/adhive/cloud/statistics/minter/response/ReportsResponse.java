package tv.adhive.cloud.statistics.minter.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tv.adhive.cloud.statistics.minter.pojo.Report;

import java.util.List;

@Data
public class ReportsResponse {

    @JsonProperty("reports")
    private List<Report> reports;
}