package tv.adhive.cloud.statistics.minter.pojo.languege;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class LanguegeStatistics {

    @JsonProperty("series")
    private List<LanguegeStatisticsItem> series;

    @JsonProperty("categories")
    private List<String> categories;
}