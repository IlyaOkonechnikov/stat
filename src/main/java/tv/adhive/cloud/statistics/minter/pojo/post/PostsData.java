package tv.adhive.cloud.statistics.minter.pojo.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PostsData {

    @JsonProperty("series")
    private List<String> series;

    @JsonProperty("values")
    private PostsValues postsValues;
}