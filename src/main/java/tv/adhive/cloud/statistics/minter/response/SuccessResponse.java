package tv.adhive.cloud.statistics.minter.response;

import lombok.Data;

@Data
public class SuccessResponse {

    private Boolean success;
}
