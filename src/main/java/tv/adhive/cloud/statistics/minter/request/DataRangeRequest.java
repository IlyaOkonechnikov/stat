package tv.adhive.cloud.statistics.minter.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tv.adhive.cloud.statistics.minter.pojo.DataUnit;

@Data
public class DataRangeRequest {

    @JsonProperty("date_from")
    private String dateFrom;

    @JsonProperty("to_date")
    private String toDate;

    @JsonProperty("unit")
    private DataUnit unit = DataUnit.day;
}
