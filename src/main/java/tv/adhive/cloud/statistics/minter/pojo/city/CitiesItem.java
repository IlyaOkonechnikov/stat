package tv.adhive.cloud.statistics.minter.pojo.city;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CitiesItem {

    @JsonProperty("data")
    private List<Double> data;

    @JsonProperty("name")
    private String name;
}