package tv.adhive.cloud.statistics.minter.pojo.country;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CountriesResponse {

    @JsonProperty("bar")
    private Countries countries;

    @JsonProperty("data")
    private CountriesStatistics countriesStatistics;

    @JsonProperty("y_label")
    private String yLabel;
}