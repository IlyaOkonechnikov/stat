package tv.adhive.cloud.statistics.minter.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataPoints {

    @JsonProperty("total")
    private Integer total;

    @JsonProperty("change")
    private Integer change;

    @JsonProperty("prev")
    private String prev;
}