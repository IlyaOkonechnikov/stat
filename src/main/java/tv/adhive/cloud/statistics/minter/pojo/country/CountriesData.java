package tv.adhive.cloud.statistics.minter.pojo.country;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tv.adhive.cloud.statistics.minter.pojo.city.CitiesItem;

import java.util.List;

@Data
public class CountriesData {

    @JsonProperty("series")
    private List<CitiesItem> series;

    @JsonProperty("categories")
    private List<String> categories;
}