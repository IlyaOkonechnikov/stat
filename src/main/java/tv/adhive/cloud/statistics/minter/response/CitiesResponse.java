package tv.adhive.cloud.statistics.minter.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tv.adhive.cloud.statistics.minter.pojo.city.Cities;
import tv.adhive.cloud.statistics.minter.pojo.city.CitiesStatistics;

@Data
public class CitiesResponse {

    @JsonProperty("bar")
    private Cities cities;

    @JsonProperty("data")
    private CitiesStatistics data;
}