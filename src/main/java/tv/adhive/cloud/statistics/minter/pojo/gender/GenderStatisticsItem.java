package tv.adhive.cloud.statistics.minter.pojo.gender;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class GenderStatisticsItem {

    @JsonProperty("name")
    private String name;

    @JsonProperty("y")
    private Float y;
}