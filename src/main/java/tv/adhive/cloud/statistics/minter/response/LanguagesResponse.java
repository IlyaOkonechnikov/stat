package tv.adhive.cloud.statistics.minter.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tv.adhive.cloud.statistics.minter.pojo.languege.LanguegeStatistics;

@Data
public class LanguagesResponse {

    @JsonProperty("y_label")
    private String yLabel;

    @JsonProperty("data")
    private LanguegeStatistics data;
}
