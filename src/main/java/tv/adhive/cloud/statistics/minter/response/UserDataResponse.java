package tv.adhive.cloud.statistics.minter.response;

import lombok.Data;
import tv.adhive.cloud.statistics.minter.pojo.User;

@Data
public class UserDataResponse {

    private boolean success;

    private User user;
}
