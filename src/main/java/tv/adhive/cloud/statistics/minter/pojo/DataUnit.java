package tv.adhive.cloud.statistics.minter.pojo;

public enum DataUnit {
    day,
    week,
    month
}
