package tv.adhive.cloud.statistics.minter.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ReportRequest {

    /**
     * Instagram User ID
     */
    @JsonProperty("user_id")
    private String userId;

    /**
     * Instagram Hashtag Name
     */
    private String hashtag;
}
