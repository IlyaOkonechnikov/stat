package tv.adhive.cloud.statistics.minter.pojo.city;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CitiesData {

    @JsonProperty("series")
    private List<CitiesItem> series;

    @JsonProperty("categories")
    private List<String> categories;
}