package tv.adhive.cloud.statistics.minter.response;

import lombok.Data;

@Data
public class ErrorResponse {

    private String error;
}
