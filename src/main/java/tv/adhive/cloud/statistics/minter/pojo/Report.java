package tv.adhive.cloud.statistics.minter.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class Report {

    @JsonProperty("is_analytics")
    private Boolean isAnalytics;

    @JsonProperty("data_points")
    private DataPoints dataPoints;

    @JsonProperty("is_competitor")
    private Boolean isCompetitor;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("is_business_account")
    private Boolean isBusinessAccount;

    @JsonProperty("has_valid_fb_token")
    private Boolean hasValidFbToken;

    @JsonProperty("profile_picture")
    private String profilePicture;

    @JsonProperty("report_type")
    private String reportType;

    @JsonProperty("full_name")
    private String fullName;

    @JsonProperty("is_ready")
    private Boolean isReady;

    @JsonProperty("report_id")
    private String reportId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("is_disconnected")
    private Boolean isDisconnected;

    @JsonProperty("is_managed")
    private Boolean isManaged;

    @JsonProperty("is_own")
    private Boolean isOwn;

    @JsonProperty("last_updated")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm z")
    private Date lastUpdated;
}