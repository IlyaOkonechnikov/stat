package tv.adhive.cloud.statistics.minter.service;

import com.ibm.icu.util.ULocale;
import feign.*;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tv.adhive.cloud.statistics.minter.client.MinterIo;
import tv.adhive.cloud.statistics.minter.pojo.DataUnit;
import tv.adhive.cloud.statistics.minter.pojo.Report;
import tv.adhive.cloud.statistics.minter.pojo.country.CountriesResponse;
import tv.adhive.cloud.statistics.minter.request.ReportRequest;
import tv.adhive.cloud.statistics.minter.response.*;
import tv.adhive.cloud.statistics.minter.service.util.LocalDateTuple;
import tv.adhive.cloud.statistics.repository.ChannelRepository;
import tv.adhive.cloud.statistics.service.ElasticService;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.channel.analytics.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static tv.adhive.cloud.statistics.Constants.INSTAGRAM_CHANNEL_INDEX;
import static tv.adhive.cloud.statistics.Constants.INSTAGRAM_CHANNEL_TYPE;

@Service
public class MinterService {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MinterService.class);

    private final ChannelRepository channelRepository;

    private final ElasticService elasticService;

    @Value("${minter.accessToken}")
    private String accessToken;

    @Value("${minter.enabled:false}")
    private Boolean isMinterEnabled = false;

    public class AccessTokenInterceptor implements RequestInterceptor {

        @Override
        public void apply(RequestTemplate template) {
            template.query("access_token", accessToken);
        }
    }

    MinterIo minterIo = Feign.builder()
            .encoder(new JacksonEncoder())
            .decoder(new JacksonDecoder())
            .logger(new Logger.JavaLogger().appendToFile("logs/http.log"))
            .logLevel(Logger.Level.FULL)
            .requestInterceptor(new AccessTokenInterceptor())
            .target(MinterIo.class, "https://api.minter.io/v1.0/");

    @Autowired
    public MinterService(ChannelRepository channelRepository, ElasticService elasticService) {
        this.channelRepository = channelRepository;
        this.elasticService = elasticService;
    }

    public void createReport(Channel channel) {
        if (isMinterEnabled) {
            createReportFromChannel(channel);
        }
    }

    public void createReports() throws IOException {
        if (isMinterEnabled) {
            final List<Channel> channels = channelRepository.findAllByStatisticsSubscriberCount(INSTAGRAM_CHANNEL_INDEX, INSTAGRAM_CHANNEL_TYPE, 500, 750);

            for (Channel channel : channels) {
                log.info("createReports {}", channel.getUrl());

                createReportFromChannel(channel);
            }
        }
    }

    private void createReportFromChannel(Channel channel) {
        final UserDataResponse userData = minterIo.getUserData(channel.getId());
        log.trace("createReport {}", userData);

        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setUserId(userData.getUser().getId());

        try {
            final ReportResponse report = minterIo.createReport(reportRequest);
            updateReportInfo(channel, report);
            log.debug("createReport {}", report);
        } catch (FeignException feignException) {
            log.error("Minter.io Error", feignException);
        }
        log.trace("createReport {}", userData);
    }

    void updateReportInfo(Channel channel, ReportResponse response) {
        if (isMinterEnabled) {
            log.info("update channel with initial report info {} {}", channel, response);
            channel.setReportState(new ReportState()
                    .setReportId(response.getReportId())
                    .setIsReportReady(false)
            );
            elasticService.update(channel, channel.getType().name());
        }
    }

    public void updateMetrics() throws IOException {
        if (isMinterEnabled) {
            List<Channel> channels = channelRepository.findAllByStatisticsSubscriberCount(INSTAGRAM_CHANNEL_INDEX, INSTAGRAM_CHANNEL_TYPE, 500, 750);
            channels.forEach(channel -> {
                if (channel.getReportState() != null && !channel.getReportState().getIsReportReady()) {
                    Report reportById = findReportById(channel.getReportState().getReportId());
                    if (reportById.getIsReady()) {
                        log.info("update metrics for {} - start", channel);
                        List<ChannelAnalyticsItem> countries = getCountriesOfFollowers(channel.getReportState().getReportId());
                        List<ChannelAnalyticsItem> languages = getLanguagesOfFollowers(channel.getReportState().getReportId());
                        List<ChannelAnalyticsItem> genders = getGendersOfFollowers(channel.getReportState().getReportId());
                        List<ChannelAnalyticsItem> cities = getCitiesOfFollowers(channel.getReportState().getReportId());

                        ReportState reportState = new ReportState()
                                .setIsReportReady(true)
                                .setReportId(reportById.getReportId())
                                .setLastModifiedDate(reportById.getLastUpdated());

                        channel.setCountries(countries)
                                .setLanguages(languages)
                                .setGender(genders)
                                .setCities(cities)
                                .setReportState(reportState);

                        elasticService.update(channel, channel.getType().name());
                        log.info("update metrics for {} - end", channel);
                    } else {
                        log.info("Report for {} is not ready yet", channel);
                    }
                }
            });
        }
    }

    public List<Report> findAllReports() throws IOException {
        log.trace(">> findAllReports()");

        List<Report> reports = new ArrayList<>();

        for (Report report : minterIo.getAllReports().getReports()) {
            if (report.getIsReady()) {
                reports.add(report);
                log.info("-- findAllReports > report: {}", report);
            }
        }
        log.trace("<< findAllReports()");
        return reports;
    }

    public Report findReportById(String reportId) {
        log.trace(">> findReportById() : reportId = {}", reportId);
        final Report report = minterIo.getReport(reportId);
        log.trace("<< findReportById() : report = {}", report);
        return report;
    }

    public List<ChannelAnalyticsItem> getCountriesOfFollowers(String reportId) {
        log.trace(">> getCountriesOfFollowers() : reportId = {}", reportId);

        LocalDateTuple localDateTuple = new LocalDateTuple(356);

        final CountriesResponse report = minterIo.getCountriesOfFollowers(
                reportId,
                localDateTuple.getStringFrom(),
                localDateTuple.getStringTo(),
                DataUnit.day);

        if (report != null) {
            List<ChannelAnalyticsItem> result = new ArrayList<>();
            report.getCountriesStatistics().getDetailed()
                    .forEach(
                            detailed -> detailed.forEach(
                                    (c, p) -> result.add(new CountryItem()
                                            .setCountry(c)
                                            .setViewsPercentage(p))
                            ));
            return result;
        }
        log.trace("<< getCountriesOfFollowers() report is empty");
        return Collections.emptyList();
    }

    public List<ChannelAnalyticsItem> getGendersOfFollowers(String reportId) {
        log.trace(">> getGendersOfFollowers : reportId = {}", reportId);

        LocalDateTuple localDateTuple = new LocalDateTuple(356);

        GendersResponse report = minterIo.getGenderOfFollowers(
                reportId,
                localDateTuple.getStringFrom(),
                localDateTuple.getStringTo(),
                DataUnit.day);

        if (report != null) {
            List<ChannelAnalyticsItem> result = new ArrayList<>();
            report.getData().getSeries()
                    .forEach(x -> result.add(new GenderItem()
                            .setGender(x.getName().toUpperCase())
                            .setViewerPercentage(x.getY())
                    ));
            return result;
        }
        log.trace("<< getGendersOfFollowers() report is empty");
        return Collections.emptyList();
    }

    public List<ChannelAnalyticsItem> getLanguagesOfFollowers(String reportId) {
        log.trace(">> getLanguagesOfFollowers : reportId = {}", reportId);

        LocalDateTuple localDateTuple = new LocalDateTuple(356);

        LanguagesResponse report = minterIo.getLanguageOfFollowers(
                reportId,
                localDateTuple.getStringFrom(),
                localDateTuple.getStringTo(),
                DataUnit.day);

        if (report != null) {
            List<ChannelAnalyticsItem> result = new ArrayList<>();
            List<String> categories = report.getData().getCategories();
            List<Double> data = report.getData().getSeries().get(0).getData();

            for (int i = 0; i < categories.size(); i++) {
                result.add(new LanguageItem()
                        .setLanguage(toISO_639_1(categories.get(i)))
                        .setViewerPercentage(data.get(i))
                );
            }
            return result;
        }
        log.trace("<< getLanguagesOfFollowers() report is empty");
        return Collections.emptyList();
    }

    public List<ChannelAnalyticsItem> getCitiesOfFollowers(String reportId) {
        log.trace(">> getCitiesOfFollowers() : reportId = {}", reportId);

        LocalDateTuple localDateTuple = new LocalDateTuple(356);

        CitiesResponse report = minterIo.getCitiesOfFollowers(
                reportId,
                localDateTuple.getStringFrom(),
                localDateTuple.getStringTo(),
                DataUnit.day);

        if (report != null) {
            List<ChannelAnalyticsItem> result = new ArrayList<>();
            report.getData().getSeries()
                    .forEach(x -> result.add(new CityItem()
                            .setCity(x.getName().toUpperCase())
                            .setViewersPercentage(x.getY())
                    ));
            return result;
        }
        log.trace("<< getCitiesOfFollowers() report is empty");
        return Collections.emptyList();
    }

    public String toISO_639_1(String name) {
        for (ULocale locale : ULocale.getAvailableLocales()) {
            System.out.println(locale.getDisplayLanguage());
            System.out.println(locale.getISO3Language());

            if (name.contains(locale.getDisplayLanguage())) {
                return locale.getLanguage();
            }
        }
        return name;
    }
}