package tv.adhive.cloud.statistics.minter.pojo.city;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CitiesStatisticsItem {

    @JsonProperty("name")
    private String name;

    @JsonProperty("y")
    private Double y;
}