package tv.adhive.cloud.statistics;

public interface Constants {

    String YOUTUBE_CHANNEL_INDEX = "youtube-channel-index";

    String YOUTUBE_CHANNEL_TYPE = "youtube-channel";

    String INSTAGRAM_CHANNEL_INDEX = "instagram-channel-index";

    String INSTAGRAM_CHANNEL_TYPE = "instagram-channel";

    String YOUTUBE_MEDIASTATISTICS_INDEX = "youtube-mediastatistics-index";

    String YOUTUBE_MEDIASTATISTICS_TYPE = "youtube-mediastatistics";

    String INSTAGRAM_MEDIASTATISTICS_INDEX = "instagram-mediastatistics-index";

    String INSTAGRAM_MEDIASTATISTICS_TYPE = "instagram-mediastatistics";

    int MEDIA_AMOUNT = 5;
}
