package tv.adhive.cloud.statistics.service;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.avg.AvgAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.max.Max;
import org.elasticsearch.search.aggregations.metrics.max.MaxAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.valuecount.ValueCount;
import org.elasticsearch.search.aggregations.metrics.valuecount.ValueCountAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tv.adhive.cloud.statistics.model.Metrics;

import java.io.IOException;
import java.util.List;

@Service
public class AggregationService {

    static final Logger log = LoggerFactory.getLogger(AggregationService.class);

    public static final String METRICS_AGGREGATION = "metrics";

    public static final String MAX_SUBSCRIBERS_AGGREGATION = "max_subscribers";

    public static final String MAX_VIEWS_AGGREGATION = "max_views";

    public static final String MAX_LIKES_AGGREGATION = "max_likes";

    private final RestHighLevelClient client;

    @Autowired
    public AggregationService(RestHighLevelClient client) {
        this.client = client;
    }

    Metrics getChannelMetrics(String index, String type) throws IOException {

        final NestedAggregationBuilder metrics = AggregationBuilders.nested(METRICS_AGGREGATION, "statistics");

        final MaxAggregationBuilder maxSubscriber = AggregationBuilders
                .max(MAX_SUBSCRIBERS_AGGREGATION)
                .field("statistics.subscriberCount");

        final MaxAggregationBuilder maxLikes = AggregationBuilders
                .max(MAX_LIKES_AGGREGATION)
                .field("statistics.likeCount");

        final MaxAggregationBuilder maxViews = AggregationBuilders
                .max(MAX_VIEWS_AGGREGATION)
                .field("statistics.viewCount");

        metrics.subAggregation(maxLikes);
        metrics.subAggregation(maxSubscriber);
        metrics.subAggregation(maxViews);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.aggregation(metrics);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(index);
        searchRequest.types(type);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);

        Aggregations aggregations = search.getAggregations();
        Nested metricsBucket = aggregations.get(METRICS_AGGREGATION);
        Max subscriberCount = metricsBucket.getAggregations().get(MAX_SUBSCRIBERS_AGGREGATION);
        Max viewCount = metricsBucket.getAggregations().get(MAX_VIEWS_AGGREGATION);
        Max likeCount = metricsBucket.getAggregations().get(MAX_LIKES_AGGREGATION);

        final Metrics metricsResponse = new Metrics(getLongOrZero(viewCount), getLongOrZero(subscriberCount), getLongOrZero(likeCount));

        log.debug("getChannelMetrics {} ", metricsResponse);

        return metricsResponse;
    }

    public Metrics getInstagramChannelMetrics() throws IOException {
        return getChannelMetrics("instagram-channel-index", "instagram-channel");
    }

    public Metrics getYoutubeChannelMetrics() throws IOException {
        return getChannelMetrics("youtube-channel-index", "youtube-channel");
    }

    public long getAvgLikesByChannelIds(List<String> channelIds) throws IOException {
        String index = "instagram-mediastatistics-index";
        String type = "instagram-mediastatistics";
        String field = "statistics.likeCount";
        return getAvgByChannelIds(index, type, field, channelIds);
    }

    public long getAvgViewsByChannelIds(List<String> channelIds) throws IOException {
        String index = "youtube-mediastatistics-index";
        String type = "youtube-mediastatistics";
        String field = "statistics.viewCount";
        return getAvgByChannelIds(index, type, field, channelIds);
    }

    public long getAvgByChannelIds(String index, String type, String field, List<String> channelIds) throws IOException {
        final ValueCountAggregationBuilder count = AggregationBuilders
                .count("agg_count").field("id");

        final NestedAggregationBuilder nested = AggregationBuilders
                .nested("agg_nested", "statistics");
        final AvgAggregationBuilder avg = AggregationBuilders
                .avg("agg_avg")
                .field(field);
        nested.subAggregation(avg);

        final TermsQueryBuilder terms = QueryBuilders.termsQuery("channelId", channelIds);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.aggregation(count);
        sourceBuilder.aggregation(nested);
        sourceBuilder.query(terms);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index);

        searchRequest.types(type);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);
        Aggregations aggregations = search.getAggregations();

        ValueCount agg_count = aggregations.get("agg_count");

        Nested nestedBucket = aggregations.get("agg_nested");
        Avg agg_avg = nestedBucket.getAggregations().get("agg_avg");

        return (long) agg_avg.getValue();
    }

    public long getLongOrZero(Max max) {
        if (max == null) return 0L;
        if (max.getValue() < 0) return 0L;
        return (long) max.getValue();
    }
}
