package tv.adhive.cloud.statistics.service;

import com.google.common.base.Preconditions;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tv.adhive.cloud.channel.api.channel.ChannelNotFoundException;
import tv.adhive.cloud.channel.api.media.ExternalMediaService;
import tv.adhive.cloud.rest.Request;
import tv.adhive.cloud.rest.channel.AdhChannel;
import tv.adhive.cloud.statistics.client.AccountClient;
import tv.adhive.cloud.statistics.integration.CompframeworkSender;
import tv.adhive.cloud.statistics.minter.service.MinterService;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.media.request.MediaRequest;
import tv.adhive.model.media.request.TypeMediaSource;
import tv.adhive.model.media.response.AIStatistics;
import tv.adhive.model.media.response.AiStatus;
import tv.adhive.model.media.statistics.MediaStatistics;
import tv.adhive.model.media.statistics.MediaStatisticsItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChannelService {

    static final Logger log = LoggerFactory.getLogger(ChannelService.class);

    private final ElasticService elasticService;
    private final ExternalMediaService youtubeMediaService;
    private final ExternalMediaService instagramMediaService;
    private final CompframeworkSender client;
    private final AccountClient accountClient;
    private MinterService minterService;

    @Autowired
    public ChannelService(ElasticService elasticService,
                          ExternalMediaService youtubeMediaService,
                          ExternalMediaService instagramMediaService,
                          CompframeworkSender compframeworkSender,
                          AccountClient accountClient,
                          MinterService minterService) {
        this.elasticService = elasticService;
        this.youtubeMediaService = youtubeMediaService;
        this.instagramMediaService = instagramMediaService;
        this.client = compframeworkSender;
        this.accountClient = accountClient;
        this.minterService = minterService;
    }

    /**
     * Saves channel common statistics and statistics for channel medias.
     * During saving sends request to Comutation Framework for eash media.
     *
     * @param channel     channel statistics
     * @param mediaAmount media amount
     * @return channel is new
     */
    public boolean registerChannel(Channel channel, Integer mediaAmount) {
        try {
            log.debug("registerChannel() - start, channel  = {}, mediaAmount = {}", channel, mediaAmount);
            Preconditions.checkNotNull(channel, "Channel is null");
            Preconditions.checkNotNull(channel.getId(), "Channel id is null");

            boolean isNewChannel = false;
            Channel savedChannel = null;

            try {
                savedChannel = elasticService.findById(channel.getId(), Channel.class, channel.getType().name());
            } catch (RuntimeException e) {
                log.error("error = {}", e);
            }

            if (savedChannel == null) {
                saveChannel(channel, mediaAmount);
                isNewChannel = true;
            } else {
                updateChannel(channel, mediaAmount);
            }
            updateIsStat(channel.getId(), channel.getType());
            log.debug("registerChannel() - end, isNew", isNewChannel);
            return isNewChannel;
        } catch (Exception e) {
            log.error("registerChannel() - Error", e);
            return false;
        }
    }

    private void updateIsStat(String externalId, TypeMediaSource typeMediaSource) {
        Request<AdhChannel> request = new Request<>();
        AdhChannel adhChannel = new AdhChannel();
        adhChannel.setIsStat(1);
        adhChannel.setExternalId(externalId);
        adhChannel.setType(typeMediaSource.name().toLowerCase());
        request.setParams(adhChannel);
        accountClient.updateIsStatChannel(request);
    }

    /**
     * Saves new channel
     *
     * @param channel     {@link Channel}
     * @param mediaAmount media amount
     */
    public void saveChannel(Channel channel, Integer mediaAmount) {
        try {
            log.debug("saveChannel() - start, channel = {}, mediaAmount = {}", channel, mediaAmount);
            Preconditions.checkNotNull(channel, "Channel is null");
            Preconditions.checkNotNull(channel.getId(), "Channel id is null");

            List<MediaStatistics> medias = getLastMedia(channel.getType().name(), channel.getId(), mediaAmount);

            final List<String> mediaIds = new ArrayList<>();
            medias.forEach(media -> {
                mediaIds.add(media.getId());
                MediaRequest mediaRequest = new MediaRequest();
                mediaRequest.setId(media.getId());
                mediaRequest.setType(channel.getType());
                client.addComputationMedia(mediaRequest);
                media.setAiStatus(AiStatus.IN_PROCESSING);
                log.debug("saveChannel() - add computation media media = {}", media.getId());
            });
            channel.setLastIdMedia(mediaIds);
            setLikesAndEr(channel, medias);
            elasticService.save(channel, channel.getType().name());
            elasticService.bulkSave(medias.stream()
                    .map(media -> ((Object) media))
                    .collect(Collectors.toList()), channel.getType().name());

            minterService.createReport(channel);
            log.debug("saveChannel() - end");
        } catch (Exception e) {
            log.error("saveChannel() - Error", e);
        }
    }

    /**
     * Updates channel statistics
     *
     * @param channel     channel common statistics
     * @param mediaAmount media amount
     */
    public void updateChannel(Channel channel, Integer mediaAmount) {
        try {
            log.debug("updateChannel() - start, channel  = {}, mediaAmount = {}", channel, mediaAmount);
            Preconditions.checkNotNull(channel, "Channel is null");

            // channel with previous state
            Channel savedChannel = elasticService.findById(channel.getId(), Channel.class, channel.getType().name());
            savedChannel.setLastModifiedDate(new Date());
            copyChannel(savedChannel, channel);

            if (mediaAmount == 0) {
                elasticService.update(savedChannel, channel.getType().name());
            } else {
                List<MediaStatistics> medias = getLastMedia(channel.getType().name(), channel.getId(), mediaAmount);

                // medias for saving to elastic
                List<Object> toSave = new ArrayList<>();
                List<String> toSaveIds = new ArrayList<>();

                // medias needed updating in elastic
                List<Object> toUpdate = new ArrayList<>();
                List<String> toUpdateIds = new ArrayList<>();

                medias.forEach(media -> {
                    if (!savedChannel.getLastIdMedia().contains(media.getId())) {
                        MediaRequest mediaRequest = new MediaRequest();
                        mediaRequest.setId(media.getId());
                        mediaRequest.setType(channel.getType());
                        client.addComputationMedia(mediaRequest);
                        media.setAiStatus(AiStatus.IN_PROCESSING);
                        toSave.add(media);
                        toSaveIds.add(media.getId());
                    } else {
                        final MediaStatistics mediaStatistics = elasticService.findById(media.getId(), MediaStatistics.class, channel.getType().name());
                        if (mediaStatistics != null) {
                            media.setAiStatus(mediaStatistics.getAiStatus());
                        }
                        toUpdate.add(media);
                        toUpdateIds.add(media.getId());
                    }
                });

                setLikesAndEr(savedChannel, medias);
                elasticService.bulkSave(toSave, channel.getType().name());
                elasticService.bulkUpdate(toUpdate, channel.getType().name());

                // update lastMediaIds in channel and its statistics
                savedChannel.getLastIdMedia().clear();
                savedChannel.getLastIdMedia().addAll(toSaveIds);
                savedChannel.getLastIdMedia().addAll(toUpdateIds);
                elasticService.update(savedChannel, channel.getType().name());
            }

            log.debug("updateChannel() - end");
        } catch (Exception e) {
            log.error("updateChannel() - Error", e);
        }
    }

    /**
     * Delete channel common statistics and statistics for channel medias.
     * During saving sends request to Comutation Framework for eash media.
     *
     * @param type
     * @param id
     */
    public void unregisterChannel(String type, String id) {
        try {
            log.info("unregisterChannel() - start, type  = {}, id = {}", type, id);
            Preconditions.checkNotNull(type, "type channel is null");
            Preconditions.checkNotNull(id, "id channel is null");
            Channel savedChannel = elasticService.findById(id, Channel.class, type.toUpperCase());
            Preconditions.checkNotNull(savedChannel, "Channel is null");

            elasticService.bulkDeleteById(savedChannel.getLastIdMedia(), MediaStatistics.class, type.toUpperCase());
            elasticService.bulkDeleteById(savedChannel.getLastIdMedia(), AIStatistics.class, type.toUpperCase());

            elasticService.deleteById(savedChannel.getId(), Channel.class, savedChannel.getType().name());
            log.info("unregisterChannel() - end, id = {}, type = {}", id, type);
        } catch (Exception e) {
            log.error("unregisterChannel error", e);
        }
    }

    private void copyChannel(Channel oldChannel, Channel newChannel) {
        if (StringUtils.isNotEmpty(newChannel.getName())) {
            oldChannel.setName(newChannel.getName());
        }
        if (StringUtils.isNotEmpty(newChannel.getDescription())) {
            oldChannel.setDescription(newChannel.getDescription());
        }
        if (newChannel.getType() != null) {
            oldChannel.setType(newChannel.getType());
        }
        if (StringUtils.isNotEmpty(newChannel.getPictureUrl())) {
            oldChannel.setPictureUrl(newChannel.getPictureUrl());
        }
        if (StringUtils.isNotEmpty(newChannel.getUrl())) {
            oldChannel.setUrl(newChannel.getUrl());
        }
        if (newChannel.getStatistics() != null) {
            oldChannel.setStatistics(newChannel.getStatistics());
        }
        if (CollectionUtils.isNotEmpty(newChannel.getAge())) {
            oldChannel.setAge(newChannel.getAge());
        }
        if (CollectionUtils.isNotEmpty(newChannel.getGender())) {
            oldChannel.setGender(newChannel.getGender());
        }
        if (CollectionUtils.isNotEmpty(newChannel.getCountries())) {
            oldChannel.setCountries(newChannel.getCountries());
        }
        if (newChannel.getDefaultLanguage() != null) {
            oldChannel.setDefaultLanguage(newChannel.getDefaultLanguage());
        }
    }

    private List<MediaStatistics> getLastMedia(String type, String idChannel, Integer mediaAmount) {
        try {
            if (TypeMediaSource.YOUTUBE == TypeMediaSource.valueOf(type)) {
                return youtubeMediaService.getLastMedia(idChannel, mediaAmount);
            } else {
                return instagramMediaService.getLastMedia(idChannel, mediaAmount);
            }
        } catch (ChannelNotFoundException e) {
            log.error("getLastMedia(), error = {}", e);
        }

        return Collections.EMPTY_LIST;
    }

    private void setLikesAndEr(Channel channel, List<MediaStatistics> medias) {
        float likes = 0;
        float dislikes = 0;
        float comments = 0;
        float averageLikes = 0;
        float engagementRate = 0;


        for (MediaStatistics mediaStatistics : medias) {
            MediaStatisticsItem statisticsItem = mediaStatistics.getStatistics();
            likes += statisticsItem.getLikeCount();
            dislikes += statisticsItem.getDislikeCount();
            comments += statisticsItem.getCommentCount();
        }

        if (!medias.isEmpty()) {
            averageLikes = likes / medias.size();
            float averageDislikes = dislikes / medias.size();
            float averageComments = comments / medias.size();
            if (channel.getStatistics().getSubscriberCount() != 0) {
                engagementRate = (averageLikes + averageDislikes + averageComments) / channel.getStatistics().getSubscriberCount();
            }
        }

        channel.getStatistics().setAverageLikes(averageLikes);
        channel.getStatistics().setEngagementRate(engagementRate);
    }
}
