package tv.adhive.cloud.statistics.service;

import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.*;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.avg.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.NestedSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tv.adhive.cloud.statistics.search.*;
import tv.adhive.cloud.statistics.util.ItemConverter;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.media.request.TypeMediaSource;
import tv.adhive.model.media.statistics.MediaStatistics;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static tv.adhive.cloud.statistics.util.Constants.INDEX;
import static tv.adhive.cloud.statistics.util.Constants.SEPARATOR;

@Slf4j
@Service
public class SearchService {

    public static final float THRESHOLD = 3.6F;

    public static final int STEP_SIZE = 1000;

    private final Client client;

    private final RestHighLevelClient restClient;

    @Autowired
    public SearchService(Client client, RestHighLevelClient restClient) {
        this.client = client;
        this.restClient = restClient;
    }

    /**
     * Returns channels according to search criteria.
     *
     * @param criteria {@link Criteria} search criteria.
     * @return views amount.
     */
    public List<String> findChannelsByCriteria(Criteria criteria) throws IOException {
        log.debug("findChannelsByCriteria() - start, criteria = {}", criteria);
        Preconditions.checkNotNull(criteria, "Criteria is null");

        final BoolQueryBuilder rootQuery = boolQuery();

        if (StringUtils.isNotEmpty(criteria.getType())) {
            rootQuery.must(matchQuery("type", criteria.getType()));
        }

        // DESCRIPTION CRITERIA
        if (criteria.getDescription() != null && !criteria.getDescription().isEmpty()) {
            BoolQueryBuilder descClause = boolQuery();
            criteria.getDescription()
                    .forEach(word -> descClause.must(regexpQuery("description", ".*" + word + ".*")));
            rootQuery.must(descClause);
        }

        setLikes(rootQuery, criteria.getLikes());

        setViews(rootQuery, criteria.getViews());

        setSubscribers(rootQuery, criteria.getSubscribers());

        if (criteria.getAges() != null) {
            long from = criteria.getAges().getFrom();
            long to = criteria.getAges().getTo();

            if (from >= 13 || to <= 65) {
                AgeGroup.ageGroupsByRange(criteria.getAges())
                        .forEach(x -> addAge(rootQuery, x));
            }
        }


        if (criteria.getGenders() != null) {
            setGenders(rootQuery, criteria.getGenders());
        }

        if (criteria.getCountries() != null && !criteria.getCountries().isEmpty()) {
            criteria.getCountries().forEach(country -> {
                        final MatchQueryBuilder matchQuery = matchQuery("countries.country", country.toUpperCase());
                        rootQuery.must(nestedQuery("countries", matchQuery, ScoreMode.Avg));
                    }
            );
        }

        if (criteria.getCities() != null) {
            criteria.getCities().forEach(city -> {
                MatchQueryBuilder matchQuery = matchQuery("cities.city", city.toUpperCase());
                rootQuery.must(nestedQuery("cities", matchQuery, ScoreMode.Avg));
            });
        }

        if (criteria.getLanguages() != null) {
            criteria.getLanguages().forEach(language -> {
                MatchQueryBuilder matchQuery = matchQuery("languages.language", language.toUpperCase());
                rootQuery.must(nestedQuery("languages", matchQuery, ScoreMode.Avg));
            });
        }

        if (criteria.getRegions() != null && !criteria.getRegions().isEmpty()) {
            BoolQueryBuilder regionsBool = boolQuery();

            criteria.getRegions().forEach(region ->
                    regionsBool.must(matchQuery("regions.region", region))
            );

            rootQuery.must(nestedQuery("regions", regionsBool, ScoreMode.Avg));
        }

        if (criteria.getTags() != null && !criteria.getTags().isEmpty()) {
            if (criteria.getTags().size() == 1) {
                rootQuery.must(matchQuery("tags", criteria.getTags().get(0)));
            } else {
                BoolQueryBuilder subTagQuery = boolQuery();
                rootQuery.must(subTagQuery);
                criteria.getTags().forEach(tag ->
                        subTagQuery.should(matchQuery("tags", tag))
                );
            }
        }

        if (criteria.getTopics() != null && !criteria.getTopics().isEmpty()) {
            if (criteria.getTopics().size() == 1) {
                rootQuery.must(matchQuery("topics", criteria.getTopics().get(0)));
            } else {
                BoolQueryBuilder subTopicsQuery = boolQuery();
                rootQuery.must(subTopicsQuery);
                criteria.getTopics().forEach(topic ->
                        subTopicsQuery.should(matchQuery("topics", topic))
                );
            }
        }

        if (criteria.getPublishedAt() != null
                && criteria.getPublishedAt().getSign() != null
                && criteria.getPublishedAt().getSign() != Sign.SKIP
                && criteria.getPublishedAt().getValue() != null) {
            if (criteria.getPublishedAt().getSign() == Sign.GT)
                rootQuery.must(rangeQuery("publishedAt").gte(criteria.getPublishedAt().getValue().getTime()));
            if (criteria.getPublishedAt().getSign() == Sign.LT)
                rootQuery.must(rangeQuery("publishedAt").lte(criteria.getPublishedAt().getValue().getTime()));
        }

        if (criteria.getBloggerAge() != null && criteria.getBloggerAge() != Age.ALL) {
            if (Age.PLUS_18 == criteria.getBloggerAge()) {
                LocalDate localDate = LocalDate.now();
                Timestamp eighteenYearsAgo = Timestamp.valueOf(localDate.minusYears(18).atStartOfDay());
                rootQuery.must(rangeQuery("bloggerBirthday").lte(eighteenYearsAgo.getTime()));
            }
            if (Age.PLUS_21 == criteria.getBloggerAge()) {
                LocalDate localDate = LocalDate.now();
                Timestamp twentyOneYearsAgo = Timestamp.valueOf(localDate.minusYears(21).atStartOfDay());
                rootQuery.must(rangeQuery("bloggerBirthday").lte(twentyOneYearsAgo.getTime()));
            }
        }

        if (criteria.getBloggerLanguage() != null) {
            rootQuery.must(matchQuery("defaultLanguage", criteria.getBloggerLanguage().toLowerCase()));
        }

        if (criteria.getCountry() != null) {
            rootQuery.must(matchQuery("country", criteria.getCountry().toUpperCase()));
        }

        if (criteria.getLanguage() != null) {
            final String lang = criteria.getLanguage().toLowerCase();
            rootQuery.must(matchQuery("defaultLanguage", lang));
//            if ("RU".equals(lang) || "ID".equals(lang)) {
//                rootQuery.must(matchQuery("country", lang));
//            } else {
//                rootQuery.mustNot(matchQuery("country", "RU"));
//                rootQuery.mustNot(matchQuery("country", "ID"));
//            }
//            rootQuery.must(matchQuery("country", lang));
        }

        Map<String, String> documentMeta = ItemConverter.getDocumentMeta(
                criteria.getType().toLowerCase() + SEPARATOR + Channel.class.getSimpleName().toLowerCase()
        );

        final FieldSortBuilder sort = new FieldSortBuilder("statistics.subscriberCount")
                .setNestedSort(new NestedSortBuilder("statistics"))
                .order(SortOrder.DESC);

        SearchRequest searchRequest = new SearchRequest(documentMeta.get(INDEX));
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(rootQuery);
        searchSourceBuilder.sort(sort);
        searchSourceBuilder.size(STEP_SIZE);
        searchRequest.source(searchSourceBuilder);

        List<String> channels = scroll(searchRequest);

        log.debug("findChannelsByCriteria() - end, channels = {}", channels);
        return channels;
    }

    /**
     * Returns average number of given views.
     *
     * @param videoIds list of video ids
     * @return average views number
     */
    public Float findAverageViews(List<String> videoIds) {
        log.debug("findAverageViews() - start, ids = {}", videoIds);
        Preconditions.checkNotNull(videoIds, "Video ids is null");

        if (videoIds.isEmpty()) {
            log.debug("findAverageViews() - end");
            return 0f;
        }

        return calculateAverage(videoIds, TypeMediaSource.YOUTUBE.name().toLowerCase(), "viewCount");
    }

    /**
     * Returns average number of CPM by IDs
     *
     * @param channelIds
     * @param typeMediaSource
     * @return
     */
    public Float findAverageCpm(List<String> channelIds, TypeMediaSource typeMediaSource) {
        log.debug("findAverageCpm() - start, ids = {}", channelIds);
        Preconditions.checkNotNull(channelIds, "Channel ids is null");

        if (channelIds.isEmpty()) {
            log.debug("findAverageCpm() - end");
            return 0f;
        }

        return calculateAverageCpm(channelIds, typeMediaSource.name());
    }

    /**
     * Returns average number of posts
     *
     * @param postIds instagram post ids
     * @return average value
     */
    public Float findAverageLikes(List<String> postIds) {
        log.debug("findAverageLikes() - start, ids = {}", postIds);
        Preconditions.checkNotNull(postIds, "{Post ids is null");

        if (postIds.isEmpty()) {
            log.debug("findAverageLikes() - end");
            return 0f;
        }

        return calculateAverage(postIds, TypeMediaSource.INSTAGRAM.name().toLowerCase(), "likeCount");
    }

    private void setLikes(BoolQueryBuilder clause, LongRange likes) {
        if (likes != null) {
            BoolQueryBuilder likesClause = boolQuery();
            likesClause.must(
                    rangeQuery("statistics.likeCount")
                            .from(likes.getFrom(), true)
                            .to(likes.getTo(), true)
            );

            clause.must(nestedQuery("statistics", likesClause, ScoreMode.Avg));
        }
    }

    private void setViews(BoolQueryBuilder clause, LongRange views) {
        if (views != null) {
            BoolQueryBuilder viewsClause = boolQuery();
            viewsClause.must(
                    rangeQuery("statistics.viewCount")
                            .from(views.getFrom(), true)
                            .to(views.getTo(), true)
            );

            clause.must(nestedQuery("statistics", viewsClause, ScoreMode.Avg));
        }
    }

    private void setSubscribers(BoolQueryBuilder clause, LongRange subscribers) {
        if (subscribers != null) {
            BoolQueryBuilder subscribersClause = boolQuery();
            subscribersClause.must(
                    rangeQuery("statistics.subscriberCount")
                            .from(subscribers.getFrom(), true)
                            .to(subscribers.getTo(), true)
            );

            clause.must(nestedQuery("statistics", subscribersClause, ScoreMode.Avg));
        }
    }

    private Float calculateAverageCpm(List<String> ids, String type) {
        AvgAggregationBuilder aggregation = AggregationBuilders
                .avg("aggs")
                .field("cpm");

        Map<String, String> documentMeta = ItemConverter.getDocumentMeta(type + SEPARATOR + "channel");

        SearchResponse searchResponse = client.prepareSearch(documentMeta.get(INDEX))
                .addAggregation(aggregation)
                .setQuery(QueryBuilders.termsQuery("id", ids))
                .setSize(5000)
                .execute()
                .actionGet();

        log.trace("findAverageViews() - aggregations = {}", searchResponse.getAggregations());
        Avg aggs = searchResponse.getAggregations().get("aggs");
        Float result = (float) aggs.getValue();
        log.debug("findAverageViews() - end, avg views = {}", result);
        return result;
    }

    private Float calculateAverage(List<String> ids, String type, String field) {
        NestedAggregationBuilder nestedAgg = AggregationBuilders.nested("aggs", "statistics");
        nestedAgg.subAggregation(AggregationBuilders.avg("aggs").field("statistics." + field));

        Map<String, String> documentMeta = ItemConverter.getDocumentMeta(
                type + SEPARATOR + MediaStatistics.class.getSimpleName().toLowerCase());

        SearchResponse searchResponse = client.prepareSearch(documentMeta.get(INDEX))
                .addAggregation(nestedAgg)
                .setQuery(QueryBuilders.termsQuery("id", ids))
                .setSize(5000)
                .execute().actionGet();

        log.trace("findAverageViews() - aggregations = {}", searchResponse.getAggregations().asMap().keySet());

        Nested nested = searchResponse.getAggregations().get("aggs");
        log.trace("nested = {}", nested);
        log.trace("sub aggs - {}", nested.getAggregations().getAsMap().keySet());
        Avg average = nested.getAggregations().get("aggs");

        Float result = (float) average.getValue();
        log.debug("findAverageViews() - end, avg views = {}", result);
        return result;
    }

    private void addAge(BoolQueryBuilder clause, AgeGroup ageGroup) {
        clause.should(nestedQuery("age",
                boolQuery()
                        .must(matchQuery("age.ageGroup", ageGroup.getName()))
                        .must(rangeQuery("age.viewerPercentage").gte(THRESHOLD)),
                ScoreMode.Avg));
    }

    private void setGenders(BoolQueryBuilder clause, Genders genders) {
        BoolQueryBuilder genderBoolQuery = boolQuery();
        if (genders.getMale() != null) {
            genderBoolQuery
                    .must(matchQuery("gender.gender", "MALE"))
                    .must(rangeQuery("gender.viewerPercentage")
                            .from(genders.getMale() * 0.964F)
                            .to(genders.getMale() * 1.036F));
        }
        if (genders.getFemale() != null) {
            genderBoolQuery
                    .must(matchQuery("gender.gender", "FEMALE"))
                    .must(rangeQuery("gender.viewerPercentage")
                            .from(genders.getFemale() * 0.9F)
                            .to(genders.getFemale() * 1.1F));
        }
        clause.must(nestedQuery("gender", boolQuery(), ScoreMode.Avg));
    }

    public List<String> scroll(SearchRequest searchRequest) throws IOException {
        AtomicInteger i = new AtomicInteger(0);

        ClearScrollRequest clearRequest = new ClearScrollRequest();

        // Step #1

        searchRequest.scroll(TimeValue.timeValueSeconds(60L));
        SearchResponse searchResponse = restClient.search(searchRequest);
        String scrollId = searchResponse.getScrollId();
        SearchHits hits = searchResponse.getHits();

        List<String> results = Arrays.stream(searchResponse.getHits().getHits())
                .map(SearchHit::getSourceAsString)
                .collect(Collectors.toList());

        clearRequest.addScrollId(scrollId);

        // Step #2

        while (hits.getHits().length >= STEP_SIZE) {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(TimeValue.timeValueSeconds(30L));
            SearchResponse searchScrollResponse = restClient.searchScroll(scrollRequest);
            scrollId = searchScrollResponse.getScrollId();
            hits = searchScrollResponse.getHits();

            results.addAll(Arrays.stream(searchResponse.getHits().getHits())
                    .map(SearchHit::getSourceAsString)
                    .collect(Collectors.toList()));

            clearRequest.addScrollId(scrollId);
        }

        ClearScrollResponse response = restClient.clearScroll(clearRequest);
        return results;
    }
}
