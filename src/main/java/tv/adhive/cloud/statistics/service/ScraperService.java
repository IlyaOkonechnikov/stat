package tv.adhive.cloud.statistics.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tv.adhive.cloud.channel.api.channel.ChannelNotFoundException;
import tv.adhive.cloud.channel.api.media.ExternalMediaService;
import tv.adhive.model.media.request.TypeMediaSource;
import tv.adhive.model.media.statistics.MediaStatistics;

import java.util.Collections;
import java.util.List;

@Service
public class ScraperService {

    static final Logger log = LoggerFactory.getLogger(ScraperService.class);

    private final ExternalMediaService youtubeMediaService;

    private final ExternalMediaService instagramMediaService;

    @Autowired
    public ScraperService(ExternalMediaService youtubeMediaService, ExternalMediaService instagramMediaService) {
        this.youtubeMediaService = youtubeMediaService;
        this.instagramMediaService = instagramMediaService;
    }

    public List<MediaStatistics> getLastMedia(String type, String idChannel, Integer mediaAmount) {
        try {
            if (TypeMediaSource.YOUTUBE == TypeMediaSource.valueOf(type)) {
                return youtubeMediaService.getLastMedia(idChannel, mediaAmount);
            } else {
                return instagramMediaService.getLastMedia(idChannel, mediaAmount);
            }
        } catch (ChannelNotFoundException e) {
            log.error("getLastMedia(), error = {}", e);
        }

        return Collections.EMPTY_LIST;
    }
}
