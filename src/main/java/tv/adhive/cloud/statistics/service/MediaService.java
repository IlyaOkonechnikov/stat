package tv.adhive.cloud.statistics.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tv.adhive.cloud.statistics.Constants;
import tv.adhive.cloud.statistics.filter.media.CheckWordsCriteria;
import tv.adhive.cloud.statistics.filter.media.Media;
import tv.adhive.cloud.statistics.filter.media.MediaCriteria;
import tv.adhive.cloud.statistics.filter.media.MediaTag;
import tv.adhive.cloud.statistics.repository.MediaRepository;
import tv.adhive.model.media.request.TypeMediaSource;
import tv.adhive.model.media.response.AIStatistics;
import tv.adhive.model.media.response.Source;
import tv.adhive.model.media.response.Tag;
import tv.adhive.model.media.statistics.MediaStatistics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

@Service
public class MediaService {

    static final Logger log = LoggerFactory.getLogger(MediaService.class);

    private final RestHighLevelClient client;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final MediaRepository mediaRepository;

    @Autowired
    public MediaService(RestHighLevelClient client, MediaRepository mediaRepository) {
        this.client = client;
        this.mediaRepository = mediaRepository;
    }

    public List<String> getYouTubeMedia(MediaCriteria criteria) throws IOException {
        String index = "youtube-mediastatistics-index";
        String type = "youtube-mediastatistics";
        return getChannelMedia(index, type, criteria);
    }

    public List<String> getInstagramMedia(MediaCriteria criteria) throws IOException {
        String index = "instagram-mediastatistics-index";
        String type = "instagram-mediastatistics";
        return getChannelMedia(index, type, criteria);
    }

    public List<String> getChannelMedia(String index, String type, MediaCriteria criteria) throws IOException {
        final Date publishedAt = criteria.getPublishedAt();
        final String channelId = criteria.getChannelId();

        final BoolQueryBuilder boolQueryBuilder = boolQuery();
        boolQueryBuilder.must(rangeQuery("publishedAt").gte(publishedAt.getTime()));
        boolQueryBuilder.must(termQuery("channelId", channelId));

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.size(10000);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index);

        searchRequest.types(type);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);
        final SearchHit[] hits = search.getHits().getHits();
        List<String> result = new ArrayList<>(hits.length);
        for (SearchHit hit : hits) {
            result.add(hit.getId());
        }
        return result;
    }

    public List<String> getYouTubeAi(List<String> ids, List<MediaTag> tags) throws IOException {
        String index = "youtube-aistatistics-index";
        String type = "youtube-aistatistics";
        return getChannelAi(index, type, ids, tags);
    }

    public List<String> getInstagramAi(List<String> ids, List<MediaTag> tags) throws IOException {
        String index = "instagram-aistatistics-index";
        String type = "instagram-aistatistics";
        return getChannelAi(index, type, ids, tags);
    }

    public List<String> getChannelAi(String index, String type, List<String> ids, List<MediaTag> tags) throws IOException {

        final BoolQueryBuilder boolQueryBuilder = boolQuery();
        boolQueryBuilder.must(termsQuery("id", ids));

        tags.forEach(tag -> {
            boolQueryBuilder.must(nestedQuery("sources", boolQuery()
                            .must(termQuery("sources.type", tag.getMediaType().getType()))
                    , ScoreMode.Avg));
            boolQueryBuilder.must(nestedQuery("sources.tags", boolQuery()
                            .must(matchQuery("sources.tags.tag", tag.getTag()))
                    , ScoreMode.Avg));
        });

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index);

        searchRequest.types(type);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);
        final SearchHit[] hits = search.getHits().getHits();
        List<String> result = new ArrayList<>(hits.length);

        for (SearchHit hit : hits) {
            final AIStatistics aiStatistics = objectMapper.readValue(hit.getSourceAsString(), AIStatistics.class);

            boolean isAiStatisticsValid = false;

            for (MediaTag tag : tags) {
                int count = 0;

                for (Source source : aiStatistics.getSources()) {
                    if (source.getTags() != null) {
                        for (Tag t : source.getTags()) {
                            if (source.getType().equals(tag.getMediaType().getType()) && t.getTag().equals(tag.getTag())) {
                                count++;
                            }
                        }
                    }
                }

                /**
                 * Теги с quantity == 0 считаются стоп-словами
                 * в случае если стоп-слово найдено данный медиа файл исключается.
                 */
                if (tag.getQuantity() == 0 && count > 0) {
                    isAiStatisticsValid = false;
                }

                /**
                 * Теги с quantity > 0 работают как обычные теги
                 */
                if (tag.getQuantity() > 0 && count >= tag.getQuantity()) {
                    isAiStatisticsValid = true;
                }
            }

            if (isAiStatisticsValid) {
                result.add(hit.getId());
            }
        }
        return result;
    }

    public List<Media> getYoutubeAnalyticsMedia(MediaCriteria criteria) throws IOException {
        log.debug("getYoutubeAnalyticsMedia - start: criteria = {}", criteria);
        List<Media> mediaList = new ArrayList<>();
        final List<String> ids = getYouTubeMedia(criteria);

        final List<String> aiIds = getYouTubeAi(ids, criteria.getTags());

        aiIds.forEach(id -> {
            final Media media = new Media();
            media.setType(TypeMediaSource.YOUTUBE);
            media.setId(id);
            mediaList.add(media);
        });

        log.debug("getYoutubeAnalyticsMedia - end: medias = {}", mediaList);
        return mediaList;
    }

    public List<Media> getInstagramAnalyticsMedia(MediaCriteria criteria) throws IOException {
        log.debug("getInstagramAnalyticsMedia - start: criteria = {}", criteria);
        List<Media> mediaList = new ArrayList<>();
        final List<String> ids = getInstagramMedia(criteria);

        final List<String> aiIds = getInstagramAi(ids, criteria.getTags());

        aiIds.forEach(id -> {
            final Media media = new Media();
            media.setType(TypeMediaSource.INSTAGRAM);
            media.setId(id);
            mediaList.add(media);
        });

        log.debug("getInstagramAnalyticsMedia - end: medias = {}", mediaList);
        return mediaList;
    }

    public MediaStatistics findById(TypeMediaSource type, String id) throws IOException {
        return findById(type.toString(), id);
    }

    public MediaStatistics findById(String type, String id) {
        try {
            if ("YOUTUBE".equalsIgnoreCase(type)) {
                return mediaRepository.findById(Constants.YOUTUBE_MEDIASTATISTICS_INDEX, Constants.YOUTUBE_MEDIASTATISTICS_TYPE, id);
            }
            if ("INSTAGRAM".equalsIgnoreCase(type)) {
                return mediaRepository.findById(Constants.INSTAGRAM_MEDIASTATISTICS_INDEX, Constants.INSTAGRAM_MEDIASTATISTICS_TYPE, id);
            }
        } catch (IOException e) {
            log.error("findById error", e);
        }
        return null;
    }

    public MediaStatistics save(String type, MediaStatistics mediaStatistics) {
        try {
            if ("YOUTUBE".equalsIgnoreCase(type)) {
                return mediaRepository.save(Constants.YOUTUBE_MEDIASTATISTICS_INDEX, Constants.YOUTUBE_MEDIASTATISTICS_TYPE, mediaStatistics);
            }
            if ("INSTAGRAM".equalsIgnoreCase(type)) {
                return mediaRepository.save(Constants.INSTAGRAM_MEDIASTATISTICS_INDEX, Constants.INSTAGRAM_MEDIASTATISTICS_TYPE, mediaStatistics);
            }
        } catch (IOException e) {
            log.error("save error", e);
        }
        return null;
    }

    public List<Media> checkWordsInCaption(CheckWordsCriteria criteria) throws IOException {
        return mediaRepository.getMediaWithWordsInCaption(
                criteria.getChannelId(),
                criteria.getWords(),
                criteria.getType(),
                criteria.getPublishedAt());
    }
}
