package tv.adhive.cloud.statistics.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import tv.adhive.cloud.channel.api.channel.ChannelNotFoundException;
import tv.adhive.cloud.statistics.integration.CompframeworkSender;
import tv.adhive.cloud.statistics.repository.ChannelRepository;
import tv.adhive.cloud.statistics.repository.MediaRepository;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.media.request.MediaRequest;
import tv.adhive.model.media.request.TypeMediaSource;
import tv.adhive.model.media.response.AiStatus;
import tv.adhive.model.media.statistics.MediaStatistics;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static tv.adhive.cloud.statistics.Constants.*;

@Service
public class SchedulerService {

    static final Logger log = LoggerFactory.getLogger(SchedulerService.class);

    private final ChannelRepository channelRepository;

    private final ChannelService channelService;

    private final MediaRepository mediaRepository;

    private final CompframeworkSender compframeworkSender;

    @Value("${statistics.scheduler.youtube.update.enabled:true}")
    private boolean youtubeUpdateEnabled;

    @Value("${statistics.scheduler.instagram.update.enabled:true}")
    private boolean instagramUpdateEnabled;

    @Value("${statistics.scheduler.hours:24}")
    private long hours = 24;

    @Autowired
    public SchedulerService(ChannelRepository channelRepository, ChannelService channelService, MediaRepository mediaRepository, CompframeworkSender compframeworkSender) {
        this.channelRepository = channelRepository;
        this.channelService = channelService;
        this.mediaRepository = mediaRepository;
        this.compframeworkSender = compframeworkSender;
    }

    /* В СТАТ
     1. Реагируем на дернутый URL
     2. Сразу возращаем статус шедулеру что Job Started
     3. Смотрим количество объектов в базе ALL_CHANELS
     4. Считаем количество объектов в расчете на час ALL_CHANELS/24
     5. Из базы считваем ALL_CHANELS/24 объектов, фильтруя уже модифицированные в текущие  сутки объекты.
     6. Обновляем каналы из шага 5.
    */

    @Async
    public void updateAllChannels() {
        log.info("updateAllChannels() - start");
        try {
            if (youtubeUpdateEnabled) {
                updateYoutubeChannels();
            }
            if (instagramUpdateEnabled) {
                updateInstagramChannels();
            }
        } catch (Exception e) {
            log.error("updateAllChannels() - error", e);
        }
        log.info("updateAllChannels() - end");
    }

    public void updateYoutubeChannels() throws IOException {
        log.info("updateYoutubeChannels() - start");
        updateChannels(YOUTUBE_CHANNEL_INDEX, YOUTUBE_CHANNEL_TYPE);
        log.info("updateYoutubeChannels() - end");
    }

    public void updateInstagramChannels() throws IOException {
        log.info("updateInstagramChannels() - start");
        updateChannels(INSTAGRAM_CHANNEL_INDEX, INSTAGRAM_CHANNEL_TYPE);
        log.info("updateInstagramChannels() - end");
    }

    void updateChannels(String index, String type) throws IOException {
        log.debug("updateChannels() - start");
        final long count = channelRepository.count(index, type);
        final long hourly = count / hours;

        List<Channel> channels;
        if (hours == 1) {
            channels = channelRepository.findAll(index, type);
        } else {
            channels = channelRepository.searchAndFilterByNotToday(index, type, hourly);
        }

        for (Channel channel : channels) {
            try {
                channelService.updateChannel(channel, MEDIA_AMOUNT);
            } catch (Exception e) {
                log.error("*** updateChannels Error", e);
            }
        }
        log.debug("updateChannels() - end");
    }

    @Async
    public void updateUnprocessedMedia() {
        log.info("updateUnprocessedMedia() - start");
        try {
            updateYoutubeUnprocessedMedia();
            updateInstagramUnprocessedMedia();
        } catch (Exception e) {
            log.error("updateUnprocessedMedia() - error", e);
        }
        log.info("updateUnprocessedMedia() - end");
    }


    void updateYoutubeUnprocessedMedia() throws IOException {
        updateUnprocessedMediaStatistics(YOUTUBE_MEDIASTATISTICS_INDEX, YOUTUBE_MEDIASTATISTICS_TYPE, TypeMediaSource.YOUTUBE);
    }

    void updateInstagramUnprocessedMedia() throws IOException {
        updateUnprocessedMediaStatistics(INSTAGRAM_MEDIASTATISTICS_INDEX, INSTAGRAM_MEDIASTATISTICS_TYPE, TypeMediaSource.INSTAGRAM);
    }

    void updateUnprocessedMediaStatistics(String index, String type, TypeMediaSource mediaType) throws IOException {
        final List<MediaStatistics> allByAiStatus = mediaRepository.findAllByAiStatus(index, type, AiStatus.UNPROCESSED);
        allByAiStatus.addAll(mediaRepository.findAllByAiStatus(index, type, AiStatus.IN_PROCESSING));
        for (MediaStatistics mediaStatistics : allByAiStatus) {
            compframeworkSender.addComputationMedia(new MediaRequest()
                    .setId(mediaStatistics.getId())
                    .setType(mediaType)
            );
            mediaStatistics.setAiStatus(AiStatus.IN_PROCESSING);
            mediaStatistics.setLastModifiedDate(new Date());
        }
        mediaRepository.saveAll(index, type, allByAiStatus);
    }
}
