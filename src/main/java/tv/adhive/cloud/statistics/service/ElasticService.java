package tv.adhive.cloud.statistics.service;

import com.google.common.base.Preconditions;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetRequestBuilder;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import tv.adhive.cloud.statistics.model.ElasticItem;
import tv.adhive.cloud.statistics.util.ItemConverter;
import tv.adhive.cloud.statistics.util.JsonMapper;
import tv.adhive.cloud.statistics.util.MappingGenerator;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.media.request.TypeMediaSource;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static tv.adhive.cloud.statistics.util.Constants.*;

/**
 * Service for work with elastic.
 */
public class ElasticService {

    private static final Logger log = LoggerFactory.getLogger(ElasticService.class);

    private Client client;

    private RestHighLevelClient restClient;

    private Map<String, String> mapping;

    @Autowired
    public ElasticService(Client client, RestHighLevelClient restClient, Map<String, String> mapping) {
        this.client = client;
        this.mapping = mapping;
        this.restClient = restClient;
    }

    /**
     * Saves one item into elastic.
     *
     * @param data {@link ElasticItem}.
     * @return response status value.
     */
    public int save(final Object data, final String type) {
        log.debug("save() start, item = {}", data);
        Preconditions.checkNotNull(data, "Data is null");
        final ElasticItem item = ItemConverter.convert(data, type);
        try {
            if (item == null) {
                return RestStatus.NO_CONTENT.getStatus();
            }

            loadMapping(item);

            log.trace("saving data = {}", item.getData());
            IndexResponse response = client.prepareIndex(item.getIndex(), item.getType(), item.getId())
                    .setSource(item.getData(), XContentType.JSON)
                    .get();
            log.debug("save() end");
            return response.status().getStatus();
        } catch (IOException e) {
            log.error("save exception = {}", e);
            log.debug("save() end");
            return RestStatus.INTERNAL_SERVER_ERROR.getStatus();
        }
    }

    /**
     * Saves list of items into elastic.
     *
     * @param objects list of objects.
     * @return response status value.
     */
    public int bulkSave(final List<Object> objects, final String type) {
        log.debug("bulkSave() start, item = {}", objects);
        Preconditions.checkNotNull(objects, "Objects equal null");

        if (objects.isEmpty()) {
            return DocWriteResponse.Result.NOOP.ordinal();
        }

        List<ElasticItem> items = objects.stream()
                .map(object -> ItemConverter.convert(object, type))
                .collect(Collectors.toList());

        try {
            loadMapping(items.get(0));

            BulkRequestBuilder bulk = client.prepareBulk();
            items.forEach(item -> bulk.add(
                    client.prepareIndex(
                            item.getIndex(),
                            item.getType(),
                            item.getId()
                    ).setSource(item.getData(), XContentType.JSON)));

            BulkResponse bulkResponse = bulk.get();
            if (bulkResponse.hasFailures()) {
                Arrays.stream(bulkResponse.getItems())
                        .forEach(response -> log.error("Index request index = {}, type = {} has failed. {}",
                                response.getIndex(),
                                response.getType(),
                                response.getFailureMessage()));
            }

            log.debug("bulkSave() end, response = {}", bulkResponse);
            return bulkResponse.status().getStatus();
        } catch (IOException e) {
            log.error("bulkSave exception = {}", e);
            log.debug("bulkSave() end");
            return RestStatus.INTERNAL_SERVER_ERROR.getStatus();
        }
    }

    /**
     * Updates an existing document by id.
     *
     * @param data json data
     * @return response status
     */
    public int update(final Object data, final String type) {
        log.debug("update() start, item = {}", data);
        Preconditions.checkNotNull(data, "Data is null");
        Preconditions.checkNotNull(type, "Type is null");
        final ElasticItem item = ItemConverter.convert(data, type);

        UpdateResponse response = client.update(createUpdateRequest(item)).actionGet();
        log.debug("update() - end, result = {}", response);
        return response.status().getStatus();
    }

    /**
     * Updates collection of objects.
     *
     * @param objects list of data
     * @return status code
     */
    public int bulkUpdate(final List<Object> objects, final String type) {
        log.debug("bulkUpdate() start, items = {}", objects);
        Preconditions.checkNotNull(objects, "Objects are null");
        Preconditions.checkNotNull(type, "Type is null");

        if (objects.isEmpty()) {
            return DocWriteResponse.Result.NOOP.ordinal();
        }

        BulkRequestBuilder builder = client.prepareBulk();
        objects.forEach(object -> {
            ElasticItem item = ItemConverter.convert(object, type);
            builder.add(createUpdateRequest(item));
        });

        BulkResponse bulkItemResponses = builder.get();
        log.debug("bulkUpdate() end, status = {}", bulkItemResponses.status().getStatus());
        return bulkItemResponses.status().getStatus();
    }

    private UpdateRequest createUpdateRequest(ElasticItem item) {
        UpdateRequest updateRequest = new UpdateRequest(item.getIndex(), item.getType(), item.getId());
        updateRequest.doc(item.getData(), XContentType.JSON);
        return updateRequest;
    }

    /**
     * Returns object by id.
     *
     * @param id    document id.
     * @param clazz class type
     * @param <T>   generic type
     * @return object
     */
    public <T> T findById(String id, Class<T> clazz, final String type) {
        log.debug("findById() start, id = {}, class = {}", id, clazz);
        Preconditions.checkNotNull(id, "Document id is null");
        Preconditions.checkNotNull(clazz, "Clazz is null");
        Preconditions.checkNotNull(type, "Type is null");

        Map<String, String> documentMeta = ItemConverter.getDocumentMeta(type + SEPARATOR + clazz.getSimpleName());

        GetResponse response = client.prepareGet(documentMeta.get(INDEX), documentMeta.get(TYPE), id).get();
        if (response.isSourceEmpty()) {
            log.debug("findById() end, response is null");
            return null;
        } else {
            T result = JsonMapper.fromJson(response.getSourceAsString(), clazz);
            log.debug("findById() end, response = {}", result);
            return result;
        }
    }

    /**
     * Finds by ids
     *
     * @param ids   list of document ids
     * @param clazz type
     * @param <T>   class param
     * @return list of objects
     */
    public <T> List<T> findByIds(List<String> ids, Class<T> clazz, final String type) {
        log.debug("findByIds() start, ids = {}, class = {}", ids, clazz);

        Preconditions.checkNotNull(ids, "Document id is null");
        Preconditions.checkNotNull(clazz, "Clazz is null");
        Preconditions.checkNotNull(type, "Type is null");

        if (ids.isEmpty()) {
            return Collections.emptyList();
        }

        Map<String, String> documentMeta = ItemConverter.getDocumentMeta(type + SEPARATOR + clazz.getSimpleName());

        MultiGetRequestBuilder builder = client.prepareMultiGet();
        ids.forEach(id -> builder.add(documentMeta.get(INDEX), documentMeta.get(TYPE), id));

        MultiGetResponse multiGetItemResponses = builder.get();

        List<T> result = Arrays.stream(multiGetItemResponses.getResponses())
                .filter(multiGetItemResponse -> !multiGetItemResponse.isFailed())
                .map(multiGetItemResponse ->
                        JsonMapper.fromJson(multiGetItemResponse.getResponse().getSourceAsString(), clazz))
                .collect(Collectors.toList());
        log.debug("findByIds() end, result", result);
        return result;
    }

    /**
     * Deletes document from elastic by id
     *
     * @param id document id
     * @return response status
     */
    public <T> int deleteById(String id, Class<T> clazz, final String type) {
        log.debug("deleteById() - start, id = {}, clazz = {}", id, clazz);
        Preconditions.checkNotNull(id, "Id is null");
        Preconditions.checkNotNull(type, "Type is null");

        final Map<String, String> documentMeta = ItemConverter.getDocumentMeta(type + SEPARATOR + clazz.getSimpleName());

        DeleteResponse response = client.prepareDelete(documentMeta.get(INDEX), documentMeta.get(TYPE), id).get();
        int status = response.status().getStatus();
        log.debug("deleteById() - end, status = {}", status);
        return status;
    }

    /**
     * Deletes serveral documents from elastic by ids
     *
     * @param ids documents' ids
     * @return response status
     */
    public <T> int bulkDeleteById(List<String> ids, Class<T> clazz, final String type) {
        log.debug("bulkDeleteById() - start, ids = {}, clazz = {}", ids, clazz);
        Preconditions.checkNotNull(ids, "Ids are null");
        Preconditions.checkNotNull(clazz, "Clazz is null");
        Preconditions.checkNotNull(type, "Type is null");

        if (ids.isEmpty()) {
            return DocWriteResponse.Result.NOOP.ordinal();
        }

        final Map<String, String> documentMeta = ItemConverter.getDocumentMeta(type + SEPARATOR + clazz.getSimpleName());

        BulkRequestBuilder builder = client.prepareBulk();
        ids.forEach(id -> builder.add(new DeleteRequest(documentMeta.get(INDEX), documentMeta.get(TYPE), id)));

        BulkResponse response = builder.get();
        int status = response.status().getStatus();
        log.debug("bulkDeleteById() - end, status = {}", status);
        return status;
    }

    /**
     * Returns channel by media id.
     *
     * @param channelType channel type.
     * @param mediaId     media id.
     * @return {@link Channel}
     */
    public Channel findChannelByMediaId(final String channelType, final String mediaId) {
        log.debug("findChannelByMediaId() - start, channelType = {}, mediaId = {} ", channelType, mediaId);
        Preconditions.checkNotNull(channelType, "Criteria is null");
        Preconditions.checkNotNull(mediaId, "Criteria is null");

        // validate channel type without wrapping exception
        TypeMediaSource.valueOf(channelType);

        Map<String, String> documentMeta = ItemConverter.getDocumentMeta(channelType + SEPARATOR + "channel");

        SearchResponse searchResponse = client.prepareSearch(documentMeta.get(INDEX))
                .setQuery(QueryBuilders.termsQuery("lastIdMedia", mediaId))
                .setSize(5000)
                .execute()
                .actionGet();

        log.debug("findChannelByMediaId() - end, searchResponse = {}", searchResponse);
        return searchResponse.getHits().getHits().length > 0
                ? JsonMapper.fromJson(searchResponse.getHits().getHits()[0].getSourceAsString(), Channel.class)
                : null;
    }

    private void loadMapping(ElasticItem item) throws IOException {
        final IndicesExistsResponse res = client.admin().indices().prepareExists(item.getIndex()).execute().actionGet();
        log.trace("index exists {}", res.isExists());
        if (!res.isExists()) {
            log.trace("mapping type = {}, metadata = {}", item.getType(), mapping);
            String json = mapping.get(item.getType());
            log.trace("json mapping = {}", json);

            XContentBuilder mappingBuilder = MappingGenerator.generate(json);
            log.trace("mapping result = {}", mappingBuilder.string());

            client.admin().indices()
                    .prepareCreate(item.getIndex())
                    .addMapping(item.getType(), mappingBuilder).execute().actionGet();
        }
    }

}
