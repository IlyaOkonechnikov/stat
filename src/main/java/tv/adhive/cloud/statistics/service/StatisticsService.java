package tv.adhive.cloud.statistics.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tv.adhive.cloud.statistics.repository.ChannelRepository;
import tv.adhive.cloud.statistics.search.Criteria;
import tv.adhive.cloud.statistics.util.JsonMapper;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.channel.statistics.StatisticsResponse;
import tv.adhive.model.media.request.TypeMediaSource;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service for working with statistics.
 */
@Slf4j
@Service
public class StatisticsService {

    private final SearchService searchService;

    private final ChannelRepository channelRepository;

    @Value("${scoring.fixedCPM:false}")
    private Boolean fixedCPM;

    @Value("${scoring.fixedCPMValue:125}")
    private Float fixedCPMValue;

    @Autowired
    public StatisticsService(SearchService searchService, ChannelRepository channelRepository) {
        this.searchService = searchService;
        this.channelRepository = channelRepository;
    }

    /**
     * Returns {@link StatisticsResponse} which holds average views and average CPM values.
     * NOTE: for instagram value always equals 0
     *
     * @param criteria {@link Criteria}
     * @return {@link StatisticsResponse}
     */
    public StatisticsResponse findAverageViewsAndCpm(Criteria criteria) throws IOException {
        final StatisticsResponse response = new StatisticsResponse();
        Map<String, List<String>> channelIdMediasMap = findMediaIdsByCriteria(criteria, response);
        log.trace("channel ids and video ids = {}", channelIdMediasMap);
        setStatisticsResponse(channelIdMediasMap, TypeMediaSource.YOUTUBE, response);
        return response;
    }

    /**
     * Returns {@link StatisticsResponse} which holds average posts and average CPM values.
     *
     * @param criteria search criteria
     * @return {@link StatisticsResponse}
     */
    public StatisticsResponse findAverageLikesAndCpm(Criteria criteria) throws IOException {
        final StatisticsResponse response = new StatisticsResponse();
        Map<String, List<String>> channelIdMediasMap = findMediaIdsByCriteria(criteria, response);
        log.trace("channel ids and post ids = {}", channelIdMediasMap);
        setStatisticsResponse(channelIdMediasMap, TypeMediaSource.INSTAGRAM, response);
        return response;
    }

    private void setStatisticsResponse(Map<String, List<String>> channelIdMediasMap, TypeMediaSource typeMediaSource, StatisticsResponse response) {
        if (!channelIdMediasMap.isEmpty()) {
            List<String> mediaIds = channelIdMediasMap.values().stream()
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
            Float views = null;
            if (typeMediaSource == TypeMediaSource.INSTAGRAM) {
                views = searchService.findAverageLikes(mediaIds);
            } else if (typeMediaSource == TypeMediaSource.YOUTUBE) {
                views = searchService.findAverageViews(mediaIds);
            }

            Float cpm = searchService.findAverageCpm(new ArrayList<>(channelIdMediasMap.keySet()), typeMediaSource);
            if (fixedCPM) {
                cpm = fixedCPMValue;
            } else {
                cpm = searchService.findAverageCpm(new ArrayList<>(channelIdMediasMap.keySet()), typeMediaSource);
            }
            response.setAverage(views);
            response.setAverageCpm(cpm);
        }
    }

    private Map<String, List<String>> findMediaIdsByCriteria(Criteria criteria, StatisticsResponse response) throws IOException {
        List<String> jsonChannel = searchService.findChannelsByCriteria(criteria);

        if (jsonChannel.isEmpty()) {
            return new HashMap<>();
        }

        log.trace("found {} channels", jsonChannel.size());
        response.setChannelsAmount((long) jsonChannel.size());

        return jsonChannel.stream()
                .map(json -> JsonMapper.fromJson(json, Channel.class))
                .collect(Collectors.toMap(Channel::getId, Channel::getLastIdMedia, (a, b) -> a));
    }

    public Float findEngagementRateByChannel(TypeMediaSource type, String id) throws IOException {
        Channel channel = channelRepository.findById(type, id);
        if (channel != null && channel.getStatistics() != null) {
            return channel.getStatistics().getEngagementRate();
        }
        return null;
    }
}