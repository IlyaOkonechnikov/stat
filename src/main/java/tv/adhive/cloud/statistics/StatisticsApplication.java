package tv.adhive.cloud.statistics;

import feign.Logger;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.transaction.RabbitTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tv.adhive.cloud.statistics.component.IndexAndMapping;

@EnableFeignClients({"tv.adhive.cloud.channel.api", "tv.adhive.cloud.statistics"})
@ComponentScan({"tv.adhive.cloud.channel.api", "tv.adhive.cloud.statistics"})
@SpringBootApplication(scanBasePackages = "tv.adhive.cloud.statistics")
@EnableDiscoveryClient
@EnableScheduling
public class StatisticsApplication implements ApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(StatisticsApplication.class, args);
    }

    @Bean
    public RabbitTransactionManager transactionManager(ConnectionFactory connectionFactory) {
        return new RabbitTransactionManager(connectionFactory);
    }

    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Autowired
    IndexAndMapping indexAndMapping;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        indexAndMapping.checkAndUpdate();
    }
}
