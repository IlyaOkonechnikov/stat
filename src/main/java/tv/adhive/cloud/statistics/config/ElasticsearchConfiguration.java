package tv.adhive.cloud.statistics.config;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.webvane.elastic.storage.repository.IndexAndMappingLowLevelRepository;
import tv.adhive.cloud.statistics.component.ScrollRepository;
import tv.adhive.cloud.statistics.service.AggregationService;
import tv.adhive.cloud.statistics.service.ElasticService;
import tv.adhive.cloud.statistics.service.SearchService;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class ElasticsearchConfiguration {

    static final Logger log = LoggerFactory.getLogger(AggregationService.class);

    private static final String FILE_EXTENSION = ".json";

    @Value("${elasticsearch.host}")
    private String esHost;

    @Value("${elasticsearch.port}")
    private int esPort;

    @Value("${elasticsearch.http.port}")
    private int esHttpPort;

    @Value("${elasticsearch.clustername}")
    private String esClusterName;

    @Value("${elasticsearch.mapping.path}")
    private String path;

    @Value("${elasticsearch.mapping.types}")
    private String[] types;

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean
    public Client client() throws UnknownHostException {
        log.info("elastic client configuration started ...");

        Settings esSettings = Settings.builder()
                .put("cluster.name", esClusterName)
                .build();
        log.info("settings = {}", esSettings);

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName(esHost), esPort));
        log.info("client = {}", client);

        log.info("elastic client configuration finished...");
        return client;
    }

    @Bean
    public ElasticService elasticService(Client client, RestHighLevelClient restHighLevelClient) throws IOException {
        log.info("elastic service configuration started ...");
        Map<String, String> map = new HashMap<>();

        Arrays.stream(types).forEach(type -> {
            try {
                log.info("type = {}", type);
                String mapping = IOUtils.toString(resourceLoader.getResource(path + type + FILE_EXTENSION).getInputStream(), "UTF-8");
                log.info("mapping = {}", mapping);
                map.put(type, mapping);
            } catch (IOException e) {
                log.error("exception: {}", e);
            }
        });

        log.info("elastic service configuration finished ...");
        return new ElasticService(client, restHighLevelClient, map);
    }

    @Bean
    public RestClient restClient() {
        RestClient restClient = RestClient.builder(
                new HttpHost(esHost, esHttpPort, "http")).build();
        return restClient;
    }

    @Bean
    public RestHighLevelClient restHighLevelClient() {
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(esHost, esHttpPort, "http")));
        return restHighLevelClient;
    }

    @Bean
    public IndexAndMappingLowLevelRepository indexAndMappingLowLevelRepository(RestClient restClient) {
        IndexAndMappingLowLevelRepository indexAndMappingLowLevelRepository = new IndexAndMappingLowLevelRepository(restClient);
        return indexAndMappingLowLevelRepository;
    }

    @Bean
    public ScrollRepository scrollRepository(RestHighLevelClient restHighLevelClient) {
        ScrollRepository scrollRepository = new ScrollRepository(restHighLevelClient);
        return scrollRepository;
    }

    @Bean
    public SearchService searchService(Client client, RestHighLevelClient restClient) {
        SearchService searchService = new SearchService(client, restClient);
        return searchService;
    }

    @Bean
    public AggregationService aggregationService(RestHighLevelClient restHighLevelClient) {
        AggregationService aggregationService = new AggregationService(restHighLevelClient);
        return aggregationService;
    }
}
