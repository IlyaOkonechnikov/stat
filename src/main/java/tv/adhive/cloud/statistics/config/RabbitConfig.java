package tv.adhive.cloud.statistics.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class RabbitConfig {

    @Value("${spring.rabbitmq.queue.directExchange}")
    private String directExchange;

    @Value("${spring.rabbitmq.queue.request}")
    private String requestQueue;

    @Value("${spring.rabbitmq.queue.response}")
    private String responseQueue;

    @Value("${spring.rabbitmq.queue.channel}")
    private String channelQueue;

    @Value("${spring.rabbitmq.queue.assignTasks}")
    private String assignTasksQueue;

    @Autowired
    private ConnectionFactory connectionFactory;

    @Bean
    public RabbitAdmin rabbitAdmin() {
        log.info("RabbitAdmin, connectionFactory = {}", connectionFactory);
        RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory);
        RabbitTemplate rabbitTemplate = rabbitAdmin.getRabbitTemplate();
        rabbitTemplate.setChannelTransacted(true);
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        return rabbitAdmin;
    }


    @Bean
    public DirectExchange directExchange() {
        log.info("DirectExchange, directExchange = {}", directExchange);
        return (DirectExchange) ExchangeBuilder
                .directExchange(directExchange)
                .durable(true)
                .build();
    }


    @Bean
    public Binding requestCompframeworkBinding() {
        return BindingBuilder
                .bind(requestCompframeworkQueue())
                .to(directExchange()).withQueueName();
    }

    @Bean
    public Queue requestCompframeworkQueue() {
        log.info("Computation framework request queue, requestQueue = {}", requestQueue);
        return QueueBuilder.durable(requestQueue)
                .build();
    }

    @Bean
    public Binding responseCompframeworkBinding() {
        return BindingBuilder
                .bind(responseCompframeworkQueue())
                .to(directExchange()).withQueueName();
    }

    @Bean
    public Queue responseCompframeworkQueue() {
        log.info("Computation framework response queue, responseQueue = {}", responseQueue);
        return QueueBuilder.durable(responseQueue)
                .build();
    }

    @Bean
    public Queue channelQueue() {
        return QueueBuilder.durable(channelQueue)
                .build();
    }

    @Bean
    public Binding channelBinding() {
        return BindingBuilder
                .bind(channelQueue())
                .to(directExchange()).withQueueName();
    }

    @Bean
    public Queue assignTasksQueue() {
        return QueueBuilder.durable(assignTasksQueue)
                .build();
    }

    @Bean
    public Binding assignTasksBinding() {
        return BindingBuilder
                .bind(assignTasksQueue())
                .to(directExchange()).withQueueName();
    }
}
