package tv.adhive.cloud.statistics.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.amqp.Amqp;
import org.springframework.integration.dsl.support.Transformers;
import tv.adhive.cloud.mq.MessageQueue;
import tv.adhive.model.media.response.AIStatistics;

@Slf4j
@Configuration
@EnableIntegration
@IntegrationComponentScan
public class IntegrationConfig {

    public static final String TO_COMPFRAMEWORK_REQUEST_QUEUE_CHANNEL = "toCompframeworkRequestQueueChannel";
    public static final String TO_ASSIGN_TASKS_QUEUE_CHANNEL = "toAssignTasksQueueChannel";
    public static final String FROM_COMPFRAMEWORK_RESPONSE_QUEUE_CHANNEL = "fromCompframeworkResponseQueueChannel";
    public static final String FROM_CHANNEL_QUEUE_CHANNEL = "fromChannelQueueChannel";

    @Value("${spring.rabbitmq.queue.request}")
    private String requestQueue;
    @Value("${spring.rabbitmq.queue.response}")
    private String responseQueue;
    @Value("${spring.rabbitmq.queue.channel}")
    private String channelQueue;
    @Value("${spring.rabbitmq.queue.assignTasks}")
    private String assignTasksQueue;

    @Bean
    public IntegrationFlow compframeworkAmqpOutbound(AmqpTemplate amqpTemplate) {
        log.info("Computation framework: amqp OUT bound {}, requestQueue = {}",
                TO_COMPFRAMEWORK_REQUEST_QUEUE_CHANNEL, requestQueue);
        return IntegrationFlows.from(TO_COMPFRAMEWORK_REQUEST_QUEUE_CHANNEL)
                .transform(Transformers.toJson())
                .handle(Amqp.outboundAdapter(amqpTemplate).routingKey(requestQueue))
                .get();
    }

    @Bean
    public IntegrationFlow compframeworkAmqpInbound(ConnectionFactory connectionFactory) {
        log.info("Computation framework: amqp IN bound {}, responseQueue = {}",
                FROM_COMPFRAMEWORK_RESPONSE_QUEUE_CHANNEL, responseQueue);
        return IntegrationFlows.from(
                Amqp.inboundAdapter(connectionFactory, responseQueue).concurrentConsumers(10).channelTransacted(true))
                .transform(Transformers.fromJson(AIStatistics.class))
                .channel(FROM_COMPFRAMEWORK_RESPONSE_QUEUE_CHANNEL)
                .get();
    }

    @Bean
    public IntegrationFlow channelAmqpInbound(ConnectionFactory connectionFactory) {

        return IntegrationFlows.from(
                Amqp.inboundAdapter(connectionFactory, channelQueue).concurrentConsumers(5).channelTransacted(true))
                .transform(Transformers.fromJson(MessageQueue.class))
                .channel(FROM_CHANNEL_QUEUE_CHANNEL)
                .get();
    }

    @Bean
    public IntegrationFlow assignTasksAmqpOutbound(AmqpTemplate amqpTemplate) {
        return IntegrationFlows.from(TO_ASSIGN_TASKS_QUEUE_CHANNEL)
                .transform(Transformers.toJson())
                .handle(Amqp.outboundAdapter(amqpTemplate).routingKey(assignTasksQueue))
                .get();
    }

}
