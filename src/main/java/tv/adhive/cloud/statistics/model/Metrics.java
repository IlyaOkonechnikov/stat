package tv.adhive.cloud.statistics.model;

import java.util.Objects;

public class Metrics {

    public Metrics() {
    }

    public Metrics(long maxViews, long maxSubscribers, long maxLikes) {
        this.maxViews = maxViews;
        this.maxSubscribers = maxSubscribers;
        this.maxLikes = maxLikes;
    }

    private long maxViews;

    private long maxSubscribers;

    private long maxLikes;

    public long getMaxViews() {
        return maxViews;
    }

    public void setMaxViews(long maxViews) {
        this.maxViews = maxViews;
    }

    public long getMaxSubscribers() {
        return maxSubscribers;
    }

    public void setMaxSubscribers(long maxSubscribers) {
        this.maxSubscribers = maxSubscribers;
    }

    public long getMaxLikes() {
        return maxLikes;
    }

    public void setMaxLikes(long maxLikes) {
        this.maxLikes = maxLikes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Metrics мetrics = (Metrics) o;
        return maxViews == мetrics.maxViews &&
                maxSubscribers == мetrics.maxSubscribers &&
                maxLikes == мetrics.maxLikes;
    }

    @Override
    public int hashCode() {

        return Objects.hash(maxViews, maxSubscribers, maxLikes);
    }

    @Override
    public String toString() {
        return "Metrics{" +
                "maxViews=" + maxViews +
                ", maxSubscribers=" + maxSubscribers +
                ", maxLikes=" + maxLikes +
                '}';
    }
}
