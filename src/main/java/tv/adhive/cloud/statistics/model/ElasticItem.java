package tv.adhive.cloud.statistics.model;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model for saving data into elastic.
 */
@Data
@Accessors(chain = true)
public class ElasticItem {

    /* Entity id. Document id */
    private String id;

    /* Elastic index */
    private String index;

    /* Elastic type */
    private String type;

    /* Elastic data. JSON */
    private String data;
}
