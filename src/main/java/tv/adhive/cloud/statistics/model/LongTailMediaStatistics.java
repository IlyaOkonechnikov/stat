package tv.adhive.cloud.statistics.model;

import tv.adhive.model.media.statistics.MediaStatisticsItem;

public class LongTailMediaStatistics {
    private long step;
    private long coverage;
    private long views;
    private long likes;
    private MediaStatisticsItem mediaStatisticsItem;

    public LongTailMediaStatistics(long step, long coverage, boolean isVideo) {
        this.step = step;
        this.coverage = coverage;

        views = viewsOnStep(step, coverage);
        likes = (long) ((double) views * 0.1D);
        final long comments = (long) ((double) likes * 0.1D);
        final long dislikes = (long) ((double) likes * 0.01D);

        mediaStatisticsItem = new MediaStatisticsItem();
        if (isVideo) {
            mediaStatisticsItem.setViewCount(views);
        }
        mediaStatisticsItem.setLikeCount(likes);
        mediaStatisticsItem.setCommentCount(comments);
        mediaStatisticsItem.setDislikeCount(dislikes);
    }

    public long getViews() {
        return views;
    }

    public long getLikes() {
        return likes;
    }

    public MediaStatisticsItem getMediaStatisticsItem() {
        return mediaStatisticsItem;
    }

    long viewsOnStep(long step, long coverage) {
        long views = 0;
        for (int i = 0; i < step; i++) {
            views = views + (long) ((double) coverage * (1D / (step + 1D)));
        }
        if (coverage % 2 == 0) {
            return (long) (1.2D * views);
        } else {
            return views;
        }
    }
}
