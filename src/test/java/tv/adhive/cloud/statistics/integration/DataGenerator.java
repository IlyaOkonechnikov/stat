package tv.adhive.cloud.statistics.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.*;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

@Slf4j
public class DataGenerator {

    private String[] topics = {"news", "movies", "music",
            "humor", "cookery", "games",
            "technology", "sport", "fashion",
            "lifestyle"};

    private String[] languages = {"ru", "en"};

    private final RestHighLevelClient client;

    private ObjectMapper mapper = new ObjectMapper();

    AtomicInteger RU = new AtomicInteger();

    AtomicInteger US = new AtomicInteger();

    AtomicInteger BOTH = new AtomicInteger();

    public DataGenerator(RestHighLevelClient client) {
        this.client = client;
    }

    public void scroll(String index, int stepSize) throws IOException {
        AtomicInteger i = new AtomicInteger(0);

        ClearScrollRequest clearRequest = new ClearScrollRequest();

        // Step #1

        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchAllQuery());
        searchSourceBuilder.size(stepSize);
        searchRequest.source(searchSourceBuilder);
        searchRequest.scroll(TimeValue.timeValueMinutes(1L));
        SearchResponse searchResponse = client.search(searchRequest);
        String scrollId = searchResponse.getScrollId();
        SearchHits hits = searchResponse.getHits();

        hits.forEach(this::doStaff);

        clearRequest.addScrollId(scrollId);

        // Step #2

        while (hits.getHits().length >= stepSize) {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(TimeValue.timeValueSeconds(30));
            SearchResponse searchScrollResponse = client.searchScroll(scrollRequest);
            scrollId = searchScrollResponse.getScrollId();
            hits = searchScrollResponse.getHits();

            hits.forEach(this::doStaff);

            clearRequest.addScrollId(scrollId);
        }

        ClearScrollResponse response = client.clearScroll(clearRequest);
        boolean success = response.isSucceeded();
        int released = response.getNumFreed();

        log.warn("RU {} US {} BOTH {}", RU, US, BOTH);
    }

    private void doStaff(SearchHit hit) {
        updateChannelInfo(hit);
        updateChannelAges(hit);
        updateChannelCountries(hit);
        updateChannelGenders(hit);
    }


    private void updateChannelAges(SearchHit hit) {
        UpdateRequest updateRequest = new UpdateRequest(hit.getIndex(), hit.getType(), hit.getId());
        try {
            double age13_17 = ThreadLocalRandom.current().nextDouble(0D, 15D);
            double age18_24 = ThreadLocalRandom.current().nextDouble(0D, 15D);
            double age35_44 = ThreadLocalRandom.current().nextDouble(0D, 16.6D);
            double age45_54 = ThreadLocalRandom.current().nextDouble(0D, 16.6D);
            double age55_64 = ThreadLocalRandom.current().nextDouble(0D, 16.6D);
            double age65 = ThreadLocalRandom.current().nextDouble(0D, 16.6D);
            double age25_34 = 100 - age13_17 - age18_24 - age35_44 - age45_54 - age65;

            XContentBuilder builder = XContentFactory.jsonBuilder();
            builder.startObject();
            builder.field("age");
            builder.startArray();

            builder.startObject();
            builder.field("@type", "AgeGroupsItem");
            builder.field("ageGroup", "age13-17");
            builder.field("viewerPercentage", (float) (age13_17));
            builder.endObject();

            builder.startObject();
            builder.field("@type", "AgeGroupsItem");
            builder.field("ageGroup", "age18-24");
            builder.field("viewerPercentage", (float) (age18_24));
            builder.endObject();

            builder.startObject();
            builder.field("@type", "AgeGroupsItem");
            builder.field("ageGroup", "age25-34");
            builder.field("viewerPercentage", (float) (age25_34));
            builder.endObject();

            builder.startObject();
            builder.field("@type", "AgeGroupsItem");
            builder.field("ageGroup", "age35-44");
            builder.field("viewerPercentage", (float) (age35_44));
            builder.endObject();

            builder.startObject();
            builder.field("@type", "AgeGroupsItem");
            builder.field("ageGroup", "age45-54");
            builder.field("viewerPercentage", (float) (age45_54));
            builder.endObject();

            builder.startObject();
            builder.field("@type", "AgeGroupsItem");
            builder.field("ageGroup", "age55-64");
            builder.field("viewerPercentage", (float) (age55_64));
            builder.endObject();

            builder.startObject();
            builder.field("@type", "AgeGroupsItem");
            builder.field("ageGroup", "age65-");
            builder.field("viewerPercentage", (float) (age65));
            builder.endObject();

            builder.endArray();
            builder.endObject();

            updateRequest.doc(builder);

            client.update(updateRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateChannelCountries(SearchHit hit) {
        UpdateRequest updateRequest = new UpdateRequest(hit.getIndex(), hit.getType(), hit.getId());
        try {
            int c = ThreadLocalRandom.current().nextInt(1, 4);

            XContentBuilder builder = XContentFactory.jsonBuilder();
            builder.startObject();
            builder.field("countries");
            builder.startArray();

            if (c == 1) RU.incrementAndGet();
            if (c == 2) US.incrementAndGet();
            if (c == 3) BOTH.incrementAndGet();

            if (c == 1 || c == 3) {
                builder.startObject();
                builder.field("@type", "CountryItem");
                builder.field("country", "RU");
                builder.field("views", 10000L);
                builder.field("likes", 1000L);
                builder.field("dislikes", 100L);
                builder.field("comments", 100L);
                builder.field("shares", 100L);
                builder.endObject();
            }
            if (c == 2 || c == 3) {
                builder.startObject();
                builder.field("@type", "CountryItem");
                builder.field("country", "US");
                builder.field("views", 10000L);
                builder.field("likes", 1000L);
                builder.field("dislikes", 100L);
                builder.field("comments", 100L);
                builder.field("shares", 100L);
                builder.endObject();
            }

            builder.endArray();
            builder.endObject();

            updateRequest.doc(builder);

            client.update(updateRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateChannelGenders(SearchHit hit) {
        UpdateRequest updateRequest = new UpdateRequest(hit.getIndex(), hit.getType(), hit.getId());
        try {
            double male = ThreadLocalRandom.current().nextDouble(1, 99);

            XContentBuilder builder = XContentFactory.jsonBuilder();
            builder.startObject();
            builder.field("gender");
            builder.startArray();

            builder.startObject();
            builder.field("@type", "GenderItem");
            builder.field("gender", "female");
            builder.field("viewerPercentage", (float) (100D - male));
            builder.endObject();

            builder.startObject();
            builder.field("@type", "GenderItem");
            builder.field("gender", "male");
            builder.field("viewerPercentage", (float) male);
            builder.endObject();

            builder.endArray();
            builder.endObject();

            updateRequest.doc(builder);

            client.update(updateRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateChannelInfo(SearchHit hit) {
        UpdateRequest updateRequest = new UpdateRequest(hit.getIndex(), hit.getType(), hit.getId());

        int i1 = ThreadLocalRandom.current().nextInt(0, 10);
        int i2 = ThreadLocalRandom.current().nextInt(0, 10);
        int i3 = ThreadLocalRandom.current().nextInt(0, 10);
        String[] randomTopics = {topics[i1], topics[i2], topics[i3]};

        long age = ThreadLocalRandom.current().nextLong(12, 62);
        LocalDate localDate = LocalDate.now().minusYears(age);
        Long randomBloggerBirthday = Timestamp.valueOf(localDate.atStartOfDay()).getTime();

        int j = ThreadLocalRandom.current().nextInt(0, 2);
        String randomLanguage = languages[j];

        long randomMonth = ThreadLocalRandom.current().nextLong(0, 100);
        LocalDate randomMonthLocalDate1 = LocalDate.now().minusMonths(randomMonth);
        Long randomPublishedAt = Timestamp.valueOf(randomMonthLocalDate1.atStartOfDay()).getTime();

        updateRequest.doc(XContentType.JSON,
                "topics", randomTopics,
                "bloggerBirthday", randomBloggerBirthday,
                "defaultLanguage", randomLanguage,
                "publishedAt", randomPublishedAt);

        try {
            client.update(updateRequest);
        } catch (IOException e) {
            log.error("UpdateResponse ", e);
        }
    }
}
