package tv.adhive.cloud.statistics.integration;

import feign.Feign;
import feign.Headers;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import tv.adhive.cloud.statistics.filter.media.MediaCriteria;
import tv.adhive.cloud.statistics.filter.media.MediaResponse;
import tv.adhive.cloud.statistics.filter.media.MediaTag;
import tv.adhive.model.media.response.AIStatistics;
import tv.adhive.model.media.response.Source;
import tv.adhive.model.media.response.Tag;
import tv.adhive.model.smart.MediaType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AIStatisticsIntegrationTest {

    interface Statistics {
        @Headers("Content-Type: application/json")
        @RequestLine("POST /aiStatistics/create")
        void getChannelMedia(AIStatistics criteria);
    }

    public static void main(String... args) {
        Statistics statistics = Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(Statistics.class, "http://localhost:8090/");

        List<Source> sources = new ArrayList<>();
        final Source source = new Source();
        sources.add(source);
        source.setType("video");

        final ArrayList<Tag> tags = new ArrayList<>();
        Tag tag = new Tag();
        tag.setTag("adhive");
        tags.add(tag);
        source.setTags(tags);

        AIStatistics criteria = new AIStatistics()
                .setChannelId("UCo6WM00OdCVNxRGC5Z4uSUg")
                .setId("NfzX-lHghpU")
                .setLastModifiedDate(new Date())
                .setPublishedAt(new Date())
                .setTypeSource("YOUTUBE")
                .setSources(sources);

        statistics.getChannelMedia(criteria);
    }
}
