package tv.adhive.cloud.statistics.integration;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Test;

import java.io.IOException;

public class GenerateTest {

    RestHighLevelClient client = new RestHighLevelClient(
            RestClient.builder(
                    new HttpHost("localhost", 9200, "http")));

    @Test
    public void testYoutubeIndex() throws IOException {
        DataGenerator dataGenerator = new DataGenerator(client);

        dataGenerator.scroll("youtube-channel-index", 50);
    }
}
