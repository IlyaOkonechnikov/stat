package tv.adhive.cloud.statistics.integration;

import feign.Feign;
import feign.Headers;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import tv.adhive.cloud.statistics.filter.media.MediaCriteria;
import tv.adhive.cloud.statistics.filter.media.MediaResponse;
import tv.adhive.cloud.statistics.filter.media.MediaTag;
import tv.adhive.model.smart.MediaType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AnalyticsControllerIntegrationTest {

    interface Statistics {
        @Headers("Content-Type: application/json")
        @RequestLine("POST /analytics/media")
        MediaResponse getChannelMedia(MediaCriteria criteria);
    }

    public static void main(String... args) {
        Statistics statistics = Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(Statistics.class, "http://dev.adhive.info/stat");

        MediaCriteria criteria = new MediaCriteria();
        criteria.setType("INSTAGRAM");
        criteria.setPublishedAt(new Date(0));
        criteria.setChannelId("kurmet_ismailov");

        List<MediaTag> mediaTags = new ArrayList<>();
        MediaTag tag = new MediaTag();
        tag.setTag("Mercedes-Benz");
        tag.setMediaType(MediaType.VIDEO);
        tag.setQuantity(1);
        mediaTags.add(tag);

        criteria.setTags(mediaTags);

        MediaResponse response = statistics.getChannelMedia(criteria);
        System.out.println(response.getMedia());
    }
}
