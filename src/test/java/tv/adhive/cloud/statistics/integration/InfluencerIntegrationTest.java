package tv.adhive.cloud.statistics.integration;

import feign.Feign;
import feign.Headers;
import feign.Logger;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.slf4j.Slf4jLogger;
import org.junit.Test;
import tv.adhive.cloud.statistics.search.Criteria;
import tv.adhive.cloud.statistics.search.LongRange;

import java.util.ArrayList;
import java.util.List;

public class InfluencerIntegrationTest {

    interface Influencers {
        @Headers("Content-Type: application/json")
        @RequestLine("POST /influencer/list")
        List<String> list(Criteria criteria);
    }

    @Test
    public void getYoutubeIds() {
        Influencers client = Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
//                .target(Influencers.class, "http://dev.adhive.info/stat");
                .logger(new Slf4jLogger(InfluencerIntegrationTest.class))
                .logLevel(Logger.Level.FULL)
                .target(Influencers.class, "http://localhost:8090");

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");


        final List<String> tags = new ArrayList<>();
        tags.add("person");

        LongRange views = new LongRange().setFrom(1000L).setTo(5000L);

        criteria.setViews(views);
        criteria.setTags(tags);

        List<String> response = client.list(criteria);
        response.forEach(System.out::println);
    }
}
