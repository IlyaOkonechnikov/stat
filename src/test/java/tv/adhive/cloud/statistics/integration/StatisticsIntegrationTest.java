package tv.adhive.cloud.statistics.integration;

import feign.*;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import tv.adhive.cloud.statistics.search.Criteria;
import tv.adhive.cloud.statistics.search.LongRange;
import tv.adhive.model.channel.analytics.CountryItem;
import tv.adhive.model.channel.statistics.StatisticsResponse;

import java.util.ArrayList;
import java.util.List;

public class StatisticsIntegrationTest {

    interface Statistics {
        @Headers("Content-Type: application/json")
        @RequestLine("POST /statistics/youtube")
        StatisticsResponse youtube(Criteria criteria);
    }

    public static void main(String... args) {
        Statistics github = Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(Statistics.class, "http://dev.adhive.info/stat");
//                .target(Statistics.class, "http://localhost:8090");

        // Fetch and print a list of the contributors to this library.
        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

//        final List<String> description = new ArrayList<>();
//        description.add(".");
//        description.add("you");
//        description.add("time");
//        criteria.setDescription(description);

        final List<String> tags = new ArrayList<>();
        tags.add("person");
//        tags.add("you");
//        tags.add("time");

        LongRange views = new LongRange().setFrom(1000L).setTo(5000L);

        criteria.setViews(views);
        criteria.setTags(tags);

        StatisticsResponse response = github.youtube(criteria);
        System.out.println(response.getChannelsAmount() + " (" + response.getAverage() + ")");
    }
}
