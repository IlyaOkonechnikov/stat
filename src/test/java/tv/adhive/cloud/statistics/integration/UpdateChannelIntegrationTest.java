package tv.adhive.cloud.statistics.integration;

import feign.Feign;
import feign.Headers;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.channel.Gender;
import tv.adhive.model.media.request.TypeMediaSource;
import tv.adhive.model.media.response.AIStatistics;
import tv.adhive.model.media.response.Source;
import tv.adhive.model.media.response.Tag;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UpdateChannelIntegrationTest {

    interface Statistics {
        @Headers("Content-Type: application/json")
        @RequestLine("PUT /statistics/channel/5")
        void updateChannel(Channel channel);
    }

    public static void main(String... args) {
        Statistics statistics = Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
//                .target(Statistics.class, "http://dev.adhive.info/stat/");
                .target(Statistics.class, "http://localhost:8090/");

        Channel channel = new Channel();
        channel.setId("philipp_adhive");
        channel.setBloggerGender(Gender.MALE);
        channel.setCountry("ID");
        channel.setPictureUrl("https://instagram.fhrk1-1.fna.fbcdn.net/vp/cc3d27f2ef81613e7854d5bf4e64d44e/5B6FF8F0/t51.2885-19/s150x150/29094601_412118159250441_5751339402689249280_n.jpg");
        channel.setName("Philipp");
        channel.setType(TypeMediaSource.INSTAGRAM);
        channel.setUrl("https://www.instagram.com/philipp_adhive/");

        statistics.updateChannel(channel);
    }
}
