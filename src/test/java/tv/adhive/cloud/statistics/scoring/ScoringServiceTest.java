package tv.adhive.cloud.statistics.scoring;

import org.junit.Test;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ScoringServiceTest {


    @Test
    public void getFeatureVector() throws IOException {
        ScoringService scoringService = new ScoringService();

        List<String> urls = new ArrayList<>();
        urls.add("https://www.instagram.com/dyageleva/");

        final List<ScoringVector> featureVector = scoringService.getFeatureVector(urls);

        String[] header = {"FREQ",
                "LIKE_COEFF",
                "COMMENT_COEFF",
                "MOSCOW_PART",
                "SPB_PART",
                "REGION_PART",
                "SUB"};


        String[] fields = {"freq",
                "likeCoeff",
                "commentCoeff",
                "moscowPart",
                "spbPart",
                "regionPart",
                "sub"};

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("logs/vector.csv"))) {
            ICsvBeanWriter csvWriter = new CsvBeanWriter(writer, CsvPreference.STANDARD_PREFERENCE);

            csvWriter.writeHeader(header);

            for (ScoringVector vector : featureVector) {
                csvWriter.write(vector, fields);
            }

            csvWriter.close();
        }

        featureVector.forEach(System.out::println);
    }
}