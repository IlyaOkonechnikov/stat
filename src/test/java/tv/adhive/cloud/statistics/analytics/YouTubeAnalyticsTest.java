package tv.adhive.cloud.statistics.analytics;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import tv.adhive.cloud.statistics.search.AgeGroup;
import tv.adhive.model.channel.analytics.AgeGroupsItem;
import tv.adhive.model.channel.analytics.ChannelAnalyticsItem;
import tv.adhive.model.channel.analytics.CountryItem;
import tv.adhive.model.channel.analytics.GenderItem;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YouTubeAnalyticsTest {

    RestHighLevelClient client = new RestHighLevelClient(
            RestClient.builder(
                    new HttpHost("localhost", 9200, "http")));

    @Test
    public void isExistsAge() throws IOException {
        final BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        BoolQueryBuilder innerBool = QueryBuilders.boolQuery();
        innerBool.must(QueryBuilders.existsQuery("age"));
        NestedQueryBuilder nestedQueryBuilder = QueryBuilders.nestedQuery("age", innerBool, ScoreMode.Avg);
        boolQuery.must(nestedQueryBuilder);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQuery);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("youtube-channel-index");
        searchRequest.types("youtube-channel");
        searchRequest.searchType(SearchType.QUERY_THEN_FETCH);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);

        System.out.println(search.getHits().totalHits);
    }

    @Test
    public void isExistsCountries() throws IOException {
        final BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        BoolQueryBuilder innerBool = QueryBuilders.boolQuery();
        innerBool.must(QueryBuilders.existsQuery("countries"));
        NestedQueryBuilder nestedQueryBuilder = QueryBuilders.nestedQuery("countries", innerBool, ScoreMode.Avg);
        boolQuery.must(nestedQueryBuilder);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQuery);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("youtube-channel-index");
        searchRequest.types("youtube-channel");
        searchRequest.searchType(SearchType.QUERY_THEN_FETCH);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);

        System.out.println(search.getHits().totalHits);
    }

    @Test
    public void isExistsGender() throws IOException {
        final BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        BoolQueryBuilder innerBool = QueryBuilders.boolQuery();
        innerBool.must(QueryBuilders.existsQuery("gender"));
        NestedQueryBuilder nestedQueryBuilder = QueryBuilders.nestedQuery("gender", innerBool, ScoreMode.Avg);
        boolQuery.must(nestedQueryBuilder);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQuery);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("youtube-channel-index");
        searchRequest.types("youtube-channel");
        searchRequest.searchType(SearchType.QUERY_THEN_FETCH);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);

        System.out.println(search.getHits().totalHits);
    }

    @Test
    public void isExistsStatistics() throws IOException {
        final BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        BoolQueryBuilder innerBool = QueryBuilders.boolQuery();
        innerBool.must(QueryBuilders.existsQuery("statistics"));
        NestedQueryBuilder nestedQueryBuilder = QueryBuilders.nestedQuery("statistics", innerBool, ScoreMode.Avg);
        boolQuery.must(nestedQueryBuilder);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQuery);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("youtube-channel-index");
        searchRequest.types("youtube-channel");
        searchRequest.searchType(SearchType.QUERY_THEN_FETCH);
        searchRequest.source(sourceBuilder);

        final SearchResponse search = client.search(searchRequest);

        System.out.println(search.getHits().totalHits);
    }

    @Test
    public void updateWithCountry() throws IOException {
        List<ChannelAnalyticsItem> countries = new ArrayList<>();
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("countries", countries);

        CountryItem countryItem = new CountryItem();
        countryItem.setCountry("XX");
        countryItem.setViews(1000L);
        countryItem.setLikes(500L);
        countryItem.setDislikes(500L);
        countryItem.setComments(250L);
        countryItem.setShares(100L);

        countries.add(countryItem);

        ObjectMapper mapper = new ObjectMapper();
        final String value = mapper.writeValueAsString(objectMap);

        System.out.println(value);

        XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        builder.field("countries");
        builder.startArray();
        builder.startObject();
        builder.field("country", "XX");
        builder.field("views", 1000L);
        builder.field("likes", 1000L);
        builder.field("dislikes", 1000L);
        builder.field("comments", 1000L);
        builder.field("shares", 1000L);
        builder.endObject();
        builder.endArray();
        builder.endObject();

        System.out.println(builder.string());

        UpdateRequest updateRequest = new UpdateRequest("youtube-channel-index", "youtube-channel", "UCe2txMAhusRl-9tm10PnJoA");
        updateRequest.doc(value, XContentType.JSON);
//        updateRequest.doc(builder);

        final UpdateResponse update = client.update(updateRequest);
        System.out.println(update.status());
    }

    @Test
    public void updateWithGender() throws IOException {
        List<ChannelAnalyticsItem> genderes = new ArrayList<>();
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("gender", genderes);


        GenderItem genderItemM = new GenderItem().setGender("MALE").setViewerPercentage(72.3F);
        GenderItem genderItemF = new GenderItem().setGender("FEMALE").setViewerPercentage(27.7F);

        genderes.add(genderItemM);
        genderes.add(genderItemF);

        ObjectMapper mapper = new ObjectMapper();
        final String value = mapper.writeValueAsString(objectMap);

        System.out.println(value);

        UpdateRequest updateRequest = new UpdateRequest("youtube-channel-index", "youtube-channel", "UCljtovlxaoBQXKJ88MzvWIA");
        updateRequest.doc(value, XContentType.JSON);
//        updateRequest.doc(builder);

        final UpdateResponse update = client.update(updateRequest);
        System.out.println(update.status());
    }

    @Test
    public void updateWithDefaultLanguage() throws IOException {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("defaultLanguage", "xx");

        ObjectMapper mapper = new ObjectMapper();
        final String value = mapper.writeValueAsString(objectMap);

        System.out.println(value);

        UpdateRequest updateRequest = new UpdateRequest("youtube-channel-index", "youtube-channel", "UCljtovlxaoBQXKJ88MzvWIA");
        updateRequest.doc(value, XContentType.JSON);

        UpdateResponse update = client.update(updateRequest);
        System.out.println(update.status());

        updateRequest = new UpdateRequest("youtube-channel-index", "youtube-channel", "UCe2txMAhusRl-9tm10PnJoA");
        updateRequest.doc(value, XContentType.JSON);

        update = client.update(updateRequest);
        System.out.println(update.status());
    }

    @Test
    public void updateWithBloggerBirthday() throws IOException {
        Map<String, Object> objectMap = new HashMap<>();

        LocalDate localDate = LocalDate.now();
        Timestamp timestamp = Timestamp.valueOf(localDate.atStartOfDay());
        objectMap.put("bloggerBirthday", Timestamp.valueOf(localDate.minusYears(22).atStartOfDay()));

        ObjectMapper mapper = new ObjectMapper();
        final String value = mapper.writeValueAsString(objectMap);

        System.out.println(value);

        UpdateRequest updateRequest = new UpdateRequest("youtube-channel-index", "youtube-channel", "UCljtovlxaoBQXKJ88MzvWIA");
        updateRequest.doc(value, XContentType.JSON);

        UpdateResponse update = client.update(updateRequest);
        System.out.println(update.status());

        updateRequest = new UpdateRequest("youtube-channel-index", "youtube-channel", "UCe2txMAhusRl-9tm10PnJoA");
        updateRequest.doc(value, XContentType.JSON);

        update = client.update(updateRequest);
        System.out.println(update.status());
    }

    @Test
    public void updateWithAge() throws IOException {
        List<ChannelAnalyticsItem> ages = new ArrayList<>();
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("age", ages);

        AgeGroupsItem item1 = new AgeGroupsItem().setAgeGroup(AgeGroup.AGE13_17.getName()).setViewerPercentage(10F);
        AgeGroupsItem item2 = new AgeGroupsItem().setAgeGroup(AgeGroup.AGE18_24.getName()).setViewerPercentage(10F);
        AgeGroupsItem item3 = new AgeGroupsItem().setAgeGroup(AgeGroup.AGE25_34.getName()).setViewerPercentage(10F);
        AgeGroupsItem item4 = new AgeGroupsItem().setAgeGroup(AgeGroup.AGE35_44.getName()).setViewerPercentage(10F);
        AgeGroupsItem item5 = new AgeGroupsItem().setAgeGroup(AgeGroup.AGE55_64.getName()).setViewerPercentage(10F);
        AgeGroupsItem item6 = new AgeGroupsItem().setAgeGroup(AgeGroup.AGE65.getName()).setViewerPercentage(10F);

        ages.add(item1);
        ages.add(item2);
        ages.add(item3);
        ages.add(item4);
        ages.add(item5);
        ages.add(item6);

        ObjectMapper mapper = new ObjectMapper();
        final String value = mapper.writeValueAsString(objectMap);

        System.out.println(value);

        UpdateRequest updateRequest = new UpdateRequest("youtube-channel-index", "youtube-channel", "UCljtovlxaoBQXKJ88MzvWIA");
        updateRequest.doc(value, XContentType.JSON);

        UpdateResponse update = client.update(updateRequest);
        System.out.println(update.status());

        updateRequest = new UpdateRequest("youtube-channel-index", "youtube-channel", "UCe2txMAhusRl-9tm10PnJoA");
        updateRequest.doc(value, XContentType.JSON);

        update = client.update(updateRequest);
        System.out.println(update.status());
    }
}
