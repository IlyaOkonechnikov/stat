package tv.adhive.cloud.statistics.minter.client;

import feign.Feign;
import feign.Logger;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.junit.Test;
import tv.adhive.cloud.statistics.minter.pojo.Report;
import tv.adhive.cloud.statistics.minter.pojo.country.CountriesResponse;
import tv.adhive.cloud.statistics.minter.pojo.DataUnit;
import tv.adhive.cloud.statistics.minter.request.ReportRequest;
import tv.adhive.cloud.statistics.minter.response.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class MinterIoTest {

    static class AccessTokenInterceptor implements RequestInterceptor {
        @Override
        public void apply(RequestTemplate template) {
            template.query("access_token", "2dniHIpdAUhM7wXQHT1cfnM8Uq7Sdjel");
        }
    }

    MinterIo minterIo = Feign.builder()
            .encoder(new JacksonEncoder())
            .decoder(new JacksonDecoder())
            .logger(new Logger.JavaLogger().appendToFile("logs/http.log"))
            .logLevel(Logger.Level.FULL)
            .requestInterceptor(new AccessTokenInterceptor())
            .target(MinterIo.class, "https://api.minter.io/v1.0/");

    @Test
    public void getAccessToken() {
    }

    @Test
    public void getUserData() {
    }

    @Test
    public void getAllReports() {
        final ReportsResponse allReports = minterIo.getAllReports();
        System.out.println(allReports);
    }

    @Test
    public void createReport() {
        final UserDataResponse userData = minterIo.getUserData("jadraim");
        System.out.println(userData);

        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setUserId(userData.getUser().getId());

        final ReportResponse report = minterIo.createReport(reportRequest);
        System.out.println(report);
    }

    @Test
    public void getReport() {
        final Report report = minterIo.getReport("5ab426201d41c80024eb6a68");
        System.out.println(report);
    }

    @Test
    public void deleteReport() {
        final SuccessResponse report = minterIo.deleteReport("5ab424771d41c8006d84120e");
        System.out.println(report);
    }

    @Test
    public void getNumberOfPosts() {
        final LocalDate to = LocalDate.now();
        final LocalDate from = to.minusDays(10);

        final String toDate = to.format(DateTimeFormatter.ISO_LOCAL_DATE);
        final String dateFrom = from.format(DateTimeFormatter.ISO_LOCAL_DATE);

        final DataResponse report = minterIo.getNumberOfPosts("5ab8050d1d41c8002caea335", dateFrom, toDate, DataUnit.day);
        System.out.println(report);
    }

    @Test
    public void getCitiesOfFollowers() {
        final LocalDate to = LocalDate.now();
        final LocalDate from = to.minusDays(365);

        final String toDate = to.format(DateTimeFormatter.ISO_LOCAL_DATE);
        final String dateFrom = from.format(DateTimeFormatter.ISO_LOCAL_DATE);

        final CitiesResponse report = minterIo.getCitiesOfFollowers("5ab8050d1d41c8002caea335", dateFrom, toDate, DataUnit.day);
        System.out.println(report);
    }

    @Test
    public void getCountriesOfFollowers() {
        final LocalDate to = LocalDate.now();
        final LocalDate from = to.minusDays(365);

        final String toDate = to.format(DateTimeFormatter.ISO_LOCAL_DATE);
        final String dateFrom = from.format(DateTimeFormatter.ISO_LOCAL_DATE);

        final CountriesResponse report = minterIo.getCountriesOfFollowers("5ab8050d1d41c8002caea335", dateFrom, toDate, DataUnit.day);
        System.out.println(report);
    }

    @Test
    public void getGenderOfFollowers() {
        final LocalDate to = LocalDate.now();
        final LocalDate from = to.minusDays(365);

        final String toDate = to.format(DateTimeFormatter.ISO_LOCAL_DATE);
        final String dateFrom = from.format(DateTimeFormatter.ISO_LOCAL_DATE);

        final GendersResponse report = minterIo.getGenderOfFollowers("5ab8050d1d41c8002caea335", dateFrom, toDate, DataUnit.day);
        System.out.println(report);
    }

    @Test
    public void getLanguageOfFollowers() {
        final LocalDate to = LocalDate.now();
        final LocalDate from = to.minusDays(365);

        final String toDate = to.format(DateTimeFormatter.ISO_LOCAL_DATE);
        final String dateFrom = from.format(DateTimeFormatter.ISO_LOCAL_DATE);

        final LanguagesResponse report = minterIo.getLanguageOfFollowers("5ab8050d1d41c8002caea335", dateFrom, toDate, DataUnit.day);
        System.out.println(report);
    }
}
