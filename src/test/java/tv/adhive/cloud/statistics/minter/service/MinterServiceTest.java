package tv.adhive.cloud.statistics.minter.service;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import tv.adhive.cloud.statistics.minter.pojo.Report;
import tv.adhive.cloud.statistics.minter.response.ReportResponse;
import tv.adhive.cloud.statistics.repository.ChannelRepository;
import tv.adhive.cloud.statistics.service.ElasticService;
import tv.adhive.model.channel.Channel;
import tv.adhive.model.channel.analytics.ReportState;
import tv.adhive.model.media.request.TypeMediaSource;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class MinterServiceTest {

    static RestHighLevelClient highLevelClient;

    static ChannelRepository channelRepository;

    static ElasticService elasticService;

    @BeforeClass
    public static void setUp() throws Exception {
        highLevelClient = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));

        channelRepository = new ChannelRepository(highLevelClient);

        elasticService = mock(ElasticService.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
    }

    @Test
    public void createReports() throws IOException {
        MinterService minterService = new MinterService(channelRepository, elasticService);

        minterService.createReports();
    }

    @Test
    public void findAllReports() throws IOException {
        MinterService minterService = new MinterService(channelRepository, elasticService);

        final List<Report> allReports = minterService.findAllReports();
        assertTrue(allReports.size() > 0);
        System.out.println(allReports.size());
    }

    @Test
    public void updateMetrics() throws IOException {
        MinterService minterService = new MinterService(channelRepository, elasticService);

        minterService.updateMetrics();
    }

    @Test
    public void updateReportInfo() {
        MinterService minterService = new MinterService(channelRepository, elasticService);
        Channel channel = new Channel();
        channel.setType(TypeMediaSource.INSTAGRAM);
        ReportResponse reportResponse = new ReportResponse();
        reportResponse.setReportId("reportId");
        ReportState reportState = new ReportState();
        reportState.setIsReportReady(false);
        reportState.setReportId("reportId");

        minterService.updateReportInfo(channel, reportResponse);

        verify(elasticService).update(channel, channel.getType().name());
        assertEquals(reportState, channel.getReportState());
    }

    @Test
    public void getCountriesOfFollowers() {
        MinterService minterService = new MinterService(channelRepository, elasticService);
        String reportId = "reportId";

        minterService.getCitiesOfFollowers(reportId);
    }

    @Test
    public void getGendersOfFollowers() {
        MinterService minterService = new MinterService(channelRepository, elasticService);
        String reportId = "reportId";

        minterService.getGendersOfFollowers(reportId);
    }

    @Test
    public void getLanguagesOfFollowers() {
        MinterService minterService = new MinterService(channelRepository, elasticService);
        String reportId = "reportId";

        minterService.getLanguagesOfFollowers(reportId);
    }

    @Test
    public void getCitiesOfFollowers() {
        MinterService minterService = new MinterService(channelRepository, elasticService);
        String reportId = "reportId";

        minterService.getCitiesOfFollowers(reportId);
    }
}
