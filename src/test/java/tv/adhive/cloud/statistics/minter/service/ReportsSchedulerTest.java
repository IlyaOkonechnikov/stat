package tv.adhive.cloud.statistics.minter.service;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author drius on 29.03.2018.
 */
public class ReportsSchedulerTest {

    private static MinterService minterService;

    @Before
    public void beforeSetup() {
        minterService = mock(MinterService.class);
    }

    @Test
    public void createReports() throws Exception {
        ReportsScheduler reportsScheduler = new ReportsScheduler();
        reportsScheduler.minterService(minterService);

        reportsScheduler.createReports();

        verify(minterService).createReports();
    }

    @Test
    public void updateMetrics() throws Exception {
        ReportsScheduler reportsScheduler = new ReportsScheduler();
        reportsScheduler.minterService(minterService);

        reportsScheduler.updateReports();

        verify(minterService).updateMetrics();
    }
}