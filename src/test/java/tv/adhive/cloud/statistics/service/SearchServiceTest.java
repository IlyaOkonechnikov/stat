package tv.adhive.cloud.statistics.service;

import org.apache.http.HttpHost;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.junit.Test;
import tv.adhive.cloud.statistics.search.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class SearchServiceTest {

    static RestHighLevelClient highLevelClient;

    {
        highLevelClient = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));
    }

    @Test
    public void justFindAllYouTubeChannels() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(473, channelsByCriteria.size());
    }

    @Test
    public void findAllYouTubeChannelsWithOneSpecificTag() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        final ArrayList<String> tags = new ArrayList<>();
        tags.add("mouse");
        criteria.setTags(tags);

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(61, channelsByCriteria.size());
    }

    @Test
    public void findAllYouTubeChannelsWithTwoSpecificTag() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        final ArrayList<String> tags = new ArrayList<>();
        tags.add("mouse");
        tags.add("tvmonitor");
        criteria.setTags(tags);

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(101, channelsByCriteria.size());
    }

    @Test
    public void findAllYouTubeChannelsWithViews() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        LongRange views = new LongRange();
        views.setFrom(1000L);
        views.setTo(5000L);

        criteria.setViews(views);

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(12, channelsByCriteria.size());
    }

    @Test
    public void findAllYouTubeChannelsWithSubscribers() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        LongRange subscribers = new LongRange();
        subscribers.setFrom(1000L);
        subscribers.setTo(5000L);

        criteria.setSubscribers(subscribers);

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(5, channelsByCriteria.size());
    }

    @Test
    public void findAllYouTubeChannelsWithPublishedAt() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        criteria.setPublishedAt(new PublishedAt().setValue(new Date()).setSign(Sign.LT));

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(6, channelsByCriteria.size());
        channelsByCriteria.forEach(c -> System.out.println(c));
    }

    @Test
    public void findAllYouTubeChannelsWithCountries() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        List<String> countries = new ArrayList<>();
        countries.add("ru");

        criteria.setCountries(countries);

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(2, channelsByCriteria.size());
        channelsByCriteria.forEach(c -> System.out.println(c));
    }

    @Test
    public void findAllYouTubeChannelsWithTwoCountries() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        List<String> countries = new ArrayList<>();
        countries.add("ru");
        countries.add("us");

        criteria.setCountries(countries);

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(2, channelsByCriteria.size());
        channelsByCriteria.forEach(c -> System.out.println(c));
    }

    @Test
    public void findAllYouTubeChannelsWithGenders() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        Genders gendersCriteria = new Genders().setFemale(25F).setMale(75F);
        criteria.setGenders(gendersCriteria);

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(2, channelsByCriteria.size());
        channelsByCriteria.forEach(c -> System.out.println(c));
    }

    @Test
    public void findAllYouTubeChannelsWithBloggerLanguage() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        criteria.setBloggerLanguage("RU");

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(2, channelsByCriteria.size());
        channelsByCriteria.forEach(c -> System.out.println(c));
    }

    @Test
    public void findAllYouTubeChannelsWithBloggerAge() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        criteria.setBloggerAge(Age.PLUS_21);

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(2, channelsByCriteria.size());
        channelsByCriteria.forEach(c -> System.out.println(c));
    }

    @Test
    public void findAllYouTubeChannelsWithAuditoryAge() throws IOException {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "adhive")
                .build();

        Client client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria();
        criteria.setType("YOUTUBE");

        criteria.setAges(new LongRange().setFrom(23L).setTo(50L));

        final List<String> channelsByCriteria = service.findChannelsByCriteria(criteria);
        assertNotNull(channelsByCriteria);
        assertTrue(channelsByCriteria.size() > 1);
        assertEquals(2, channelsByCriteria.size());
        channelsByCriteria.forEach(c -> System.out.println(c));
    }

    @Test
    public void findInstagramChannelsWithCountryRussia() throws Exception {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "elasticsearch")
                .build();

        TransportClient client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria()
                .setType("INSTAGRAM")
                .setCountries(Collections.singletonList("RUSSIAN FEDERATION"));

        List<String> channels = service.findChannelsByCriteria(criteria);
        assertNotNull(channels);
        assertTrue(channels.size() > 1);
        channels.forEach(System.out::println);
    }

    @Test
    public void findInstagramChannelWithLanguageRussian() throws Exception {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "elasticsearch")
                .build();

        TransportClient client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria()
                .setType("INSTAGRAM")
                .setLanguages(Collections.singletonList("RUSSIAN"));

        List<String> channels = service.findChannelsByCriteria(criteria);
        assertNotNull(channels);
        assertTrue(channels.size() > 1);
        channels.forEach(System.out::println);
    }

    @Test
    public void findInstagramChannelWithCityMoscow() throws Exception {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "elasticsearch")
                .build();

        TransportClient client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria()
                .setType("INSTAGRAM")
                .setCities(Collections.singletonList("MOSCOW"));

        List<String> channels = service.findChannelsByCriteria(criteria);
        assertNotNull(channels);
        assertTrue(channels.size() > 1);
        channels.forEach(System.out::println);
    }

    @Test
    public void findInstagramChannelWithGender() throws Exception {
        Settings esSettings = Settings.builder()
                .put("cluster.name", "elasticsearch")
                .build();

        TransportClient client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

        SearchService service = new SearchService(client, highLevelClient);

        Criteria criteria = new Criteria()
                .setType("INSTAGRAM")
                .setGenders(new Genders().setMale(25F).setFemale(75F));

        List<String> channels = service.findChannelsByCriteria(criteria);
        assertNotNull(channels);
        assertTrue(channels.size() > 1);
        channels.forEach(System.out::println);
    }

    @Test
    public void findAverageViews() {
    }

    @Test
    public void findAverageLikes() {
    }
}