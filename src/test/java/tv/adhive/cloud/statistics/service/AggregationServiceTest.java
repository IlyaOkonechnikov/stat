package tv.adhive.cloud.statistics.service;

import org.apache.http.HttpHost;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import tv.adhive.cloud.statistics.model.Metrics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AggregationServiceTest {

    static RestHighLevelClient highLevelClient;

    static final String youtube_id = UUID.randomUUID().toString();

    static final String instagram_id = UUID.randomUUID().toString();

    @BeforeClass
    public static void setUp() throws Exception {
        highLevelClient = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));

        IndexRequest indexRequestYT = new IndexRequest("youtube-channel-index", "youtube-channel", youtube_id);
        IndexRequest indexRequestIG = new IndexRequest("instagram-channel-index", "instagram-channel", instagram_id);

        XContentBuilder xContentBuilder = XContentFactory.jsonBuilder();
        xContentBuilder.startObject();
        xContentBuilder.field("statistics");

        xContentBuilder.startObject();
        xContentBuilder.field("subscriberCount", 910000000000000000L);
        xContentBuilder.field("viewCount", 920000000000000000L);
        xContentBuilder.field("likeCount", 930000000000000000L);
        xContentBuilder.endObject();

        xContentBuilder.endObject();

        indexRequestYT.source(xContentBuilder);
        indexRequestIG.source(xContentBuilder);
        final IndexResponse indexYT = highLevelClient.index(indexRequestYT);
        indexYT.forcedRefresh();
        final IndexResponse indexIG = highLevelClient.index(indexRequestIG);
        indexIG.forcedRefresh();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        DeleteRequest requestYT = new DeleteRequest("youtube-channel-index", "youtube-channel", youtube_id);
        DeleteRequest requestIG = new DeleteRequest("instagram-channel-index", "instagram-channel", instagram_id);
        highLevelClient.delete(requestYT);
        highLevelClient.delete(requestIG);
    }

    @Test
    public void maxMetriksYoutube() throws IOException, InterruptedException {
        Thread.sleep(500);
        AggregationService aggregationService = new AggregationService(highLevelClient);
        final Metrics channelMetrics = aggregationService.getChannelMetrics("youtube-channel-index", "youtube-channel");
        assertNotNull(channelMetrics);
        assertEquals(910000000000000000L, channelMetrics.getMaxSubscribers());
        assertEquals(920000000000000000L, channelMetrics.getMaxViews());
        assertEquals(930000000000000000L, channelMetrics.getMaxLikes());
    }

    @Test
    public void maxMetriksInstagram() throws IOException, InterruptedException {
        Thread.sleep(500);
        AggregationService aggregationService = new AggregationService(highLevelClient);
        final Metrics channelMetrics = aggregationService.getChannelMetrics("instagram-channel-index", "instagram-channel");
        assertNotNull(channelMetrics);
        assertEquals(910000000000000000L, channelMetrics.getMaxSubscribers());
        assertEquals(920000000000000000L, channelMetrics.getMaxViews());
        assertEquals(930000000000000000L, channelMetrics.getMaxLikes());
    }

    @Test
    public void getAvgLikesByChannelIds() throws InterruptedException, IOException {
        List<String> channelIds = new ArrayList<>();
        channelIds.add("aliona_sibirskaya");
        channelIds.add("ylka_ylejka");
        channelIds.add("vikatrue");
        channelIds.add("onalera");
        channelIds.add("roman_tranter");

        AggregationService aggregationService = new AggregationService(highLevelClient);
        final long coverage = aggregationService.getAvgLikesByChannelIds(channelIds);

        assertTrue(coverage > 0);
        assertTrue(coverage >= 5);
        assertEquals(60, coverage);
        System.out.println(coverage);
    }
}
