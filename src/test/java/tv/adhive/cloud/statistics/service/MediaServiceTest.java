package tv.adhive.cloud.statistics.service;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import tv.adhive.cloud.statistics.filter.media.Media;
import tv.adhive.cloud.statistics.filter.media.MediaCriteria;
import tv.adhive.cloud.statistics.filter.media.MediaTag;
import tv.adhive.cloud.statistics.repository.MediaRepository;
import tv.adhive.model.smart.MediaType;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class MediaServiceTest {

    static MediaRepository mediaRepository;

    static RestHighLevelClient highLevelClient;

    static final String youtube_id = UUID.randomUUID().toString();

    static final String instagram_id = UUID.randomUUID().toString();

    @BeforeClass
    public static void setUp() throws Exception {
        highLevelClient = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));
        mediaRepository = new MediaRepository(highLevelClient);
    }

    @AfterClass
    public static void tearDown() throws Exception {
    }

    @Test
    public void getInstagramMedia() throws IOException {
        MediaService mediaService = new MediaService(highLevelClient, mediaRepository);

        Date localDate = Date.from(LocalDate.now().minusDays(34).atStartOfDay().toInstant(ZoneOffset.UTC));

        MediaCriteria mediaCriteria = new MediaCriteria();
        mediaCriteria.setChannelId("cristiano.real.madridista");
        mediaCriteria.setPublishedAt(localDate);

        final List<String> instagramMedia = mediaService.getInstagramMedia(mediaCriteria);
        assertNotNull(instagramMedia);
        assertTrue(instagramMedia.size() > 0);
        instagramMedia.forEach(System.out::println);
    }

    @Test
    public void getInstagramAi() throws IOException {
        MediaService mediaService = new MediaService(highLevelClient, mediaRepository);

        List<String> ids = new ArrayList<>(5);
        ids.add("Be-jdRLj7jt");
        ids.add("Be_S9uwjN-n");
        ids.add("Be-tR2cjscz");
        ids.add("Be_GiDyj91C");
        ids.add("Be-6LQXD2Vu");

        List<MediaTag> mediaTags = new ArrayList<>();
        MediaTag tag = new MediaTag();
        tag.setTag("handbag");
        tag.setMediaType(MediaType.IMAGE);
        tag.setQuantity(1);
        mediaTags.add(tag);

        final List<String> instagramMedia = mediaService.getInstagramAi(ids, mediaTags);
        assertNotNull(instagramMedia);
        assertTrue(instagramMedia.size() > 0);
        instagramMedia.forEach(System.out::println);
    }

    @Test
    public void getInstagramAiMercedesBenz() throws IOException {
        MediaService mediaService = new MediaService(highLevelClient, mediaRepository);

        List<String> ids = new ArrayList<>(5);
        ids.add("Bgqv3P_j6h5");
        ids.add("BgrshI5jKqk");
        ids.add("Bgrp9CKDF4r");

        List<MediaTag> mediaTags = new ArrayList<>();
        MediaTag tag = new MediaTag();
        tag.setTag("Mercedes-Benz");
        tag.setMediaType(MediaType.IMAGE);
        tag.setQuantity(1);
        mediaTags.add(tag);

        final List<String> instagramMedia = mediaService.getInstagramAi(ids, mediaTags);
        assertNotNull(instagramMedia);
        assertTrue(instagramMedia.size() > 0);
        instagramMedia.forEach(System.out::println);
    }

    @Test
    public void getInstagramAiAsStopWord() throws IOException {
        MediaService mediaService = new MediaService(highLevelClient, mediaRepository);

        List<String> ids = new ArrayList<>(5);
        ids.add("Be-jdRLj7jt");
        ids.add("Be_S9uwjN-n");
        ids.add("Be-tR2cjscz");
        ids.add("Be_GiDyj91C");
        ids.add("Be-6LQXD2Vu");

        List<MediaTag> mediaTags = new ArrayList<>();
        MediaTag tag = new MediaTag();
        tag.setTag("handbag");
        tag.setMediaType(MediaType.IMAGE);
        tag.setQuantity(0);
        mediaTags.add(tag);

        final List<String> instagramMedia = mediaService.getInstagramAi(ids, mediaTags);
        assertNotNull(instagramMedia);
        assertEquals(0, instagramMedia.size());
    }

    @Test
    public void getInstagramAnalyticsMedia() throws IOException {
        MediaService mediaService = new MediaService(highLevelClient, mediaRepository);

        Date localDate = Date.from(LocalDate.now().minusDays(34).atStartOfDay().toInstant(ZoneOffset.UTC));

        List<MediaTag> mediaTags = new ArrayList<>();
        MediaTag tag = new MediaTag();
        tag.setTag("handbag");
        tag.setMediaType(MediaType.IMAGE);
        tag.setQuantity(1);
        mediaTags.add(tag);

        MediaCriteria mediaCriteria = new MediaCriteria();
        mediaCriteria.setChannelId("cristiano.real.madridista");
        mediaCriteria.setPublishedAt(localDate);
        mediaCriteria.setTags(mediaTags);

        final List<Media> instagramMedia = mediaService.getInstagramAnalyticsMedia(mediaCriteria);
        assertNotNull(instagramMedia);
        assertTrue(instagramMedia.size() > 0);
        instagramMedia.forEach(System.out::println);
    }
}