package tv.adhive.cloud.statistics.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import tv.adhive.cloud.statistics.search.Criteria;
import tv.adhive.model.channel.statistics.StatisticsResponse;

/**
 * Test for {@link tv.adhive.cloud.statistics.controller.StatisticsController}
 * TODO comment {@link Ignore} annotation to enable test
 */
@Ignore
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class StatisticsControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * Youtube Avg Views
     */
    @Test
    public void test1() {
        ResponseEntity<StatisticsResponse> response = restTemplate.postForEntity("/statistics/youtube",
                new Criteria()
                        .setType("YOUTUBE")
                , StatisticsResponse.class);

        log.info("response - {}", response.getBody());
    }

    /**
     * Instagram Avg Likes
     */
    @Test
    public void test2() {
        ResponseEntity<StatisticsResponse> response = restTemplate.postForEntity("/statistics/instagram",
                new Criteria()
                        .setType("INSTAGRAM")
                , StatisticsResponse.class);

        log.info("response - {}", response.getBody());
    }
}
