package tv.adhive.cloud.statistics.component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Before;
import org.junit.Test;
import org.webvane.elastic.storage.repository.IndexAndMappingLowLevelRepository;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ScrollRepositoryTest {

    RestHighLevelClient client;

    IndexAndMappingLowLevelRepository lowLevelRepository;

    ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setUp() throws Exception {
        client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));

        RestClient restClient = RestClient.builder(
                new HttpHost("localhost", 9200, "http")).build();

        lowLevelRepository = new IndexAndMappingLowLevelRepository(restClient);
    }

    @Test
    public void scroll() throws IOException {
        ScrollRepository scrollRepository = new ScrollRepository(client);
        scrollRepository.scroll("instagram-channel-index", 500);
//        scrollRepository.scroll("youtube-channel-index", 500);
//        scrollRepository.scroll("instagram-aistatistics-index", 1000);
    }

    @Test
    public void getMapping() throws IOException, URISyntaxException {
        String type = "instagram-channel";
        String index = type + "-index";

        Response response = lowLevelRepository.getMapping(index, type);
        assertEquals(200, response.getStatusLine().getStatusCode());
        final String x = EntityUtils.toString(response.getEntity());
        System.out.println(x);

        String mappingSource = new String(Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource("instagram-channel-index.json").toURI())));
        final JsonNode expectedTree = mapper.readTree(mappingSource);
        final JsonNode expectedProperties = expectedTree.get("instagram-channel-index").get("mappings").get("instagram-channel");

        final JsonNode xTree = mapper.readTree(x);
        final JsonNode xProperties = xTree.get("instagram-channel-index").get("mappings").get("instagram-channel");

        assertEquals(expectedProperties, xProperties);
    }

    @Test
    public void updateMMapping() throws IOException, URISyntaxException {
        String index = "instagram-mediastatistics-index";
        String type = "instagram-mediastatistics";
        String mappings_field = "mappings";

        Response response = lowLevelRepository.getMapping(index, type);
        final String before = EntityUtils.toString(response.getEntity());
        System.out.println(before);

        final JsonNode beforeTree = mapper.readTree(before);
        final JsonNode beforeProperties = beforeTree.get(index).get(mappings_field).get(type);

        String mappingSource = new String(Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource("instagram-mediastatistics-index-V.1.1.json").toURI())));
        response = lowLevelRepository.putMapping(index, type, mappingSource);
        assertEquals(200, response.getStatusLine().getStatusCode());

        response = lowLevelRepository.getMapping(index, type);
        final String after = EntityUtils.toString(response.getEntity());
        System.out.println(after);

        final JsonNode afterTree = mapper.readTree(after);
        final JsonNode afterProperties = afterTree.get(index).get(mappings_field).get(type);

        assertNotEquals(beforeProperties, afterProperties);
    }


    @Test
    public void updateAMapping() throws IOException, URISyntaxException {
        String index = "instagram-aistatistics-index";
        String type = "instagram-aistatistics";
        String mappings_field = "mappings";

        Response response = lowLevelRepository.getMapping(index, type);
        final String before = EntityUtils.toString(response.getEntity());
        System.out.println(before);

        final JsonNode beforeTree = mapper.readTree(before);
        final JsonNode beforeProperties = beforeTree.get(index).get(mappings_field).get(type);

        String mappingSource = new String(Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource("instagram-aistatistics-index-V.1.1.json").toURI())));
        response = lowLevelRepository.putMapping(index, type, mappingSource);
        assertEquals(200, response.getStatusLine().getStatusCode());

        response = lowLevelRepository.getMapping(index, type);
        final String after = EntityUtils.toString(response.getEntity());
        System.out.println(after);

        final JsonNode afterTree = mapper.readTree(after);
        final JsonNode afterProperties = afterTree.get(index).get(mappings_field).get(type);

        assertNotEquals(beforeProperties, afterProperties);
    }

    @Test
    public void scrollAndUpdate() throws IOException {
        ScrollRepository scrollRepository = new ScrollRepository(client);

        scrollRepository.scrollAndUpdate("instagram-channel-index", "instagram-mediastatistics-index", "instagram-mediastatistics", 10);
        scrollRepository.scrollAndUpdate("instagram-channel-index", "instagram-aistatistics-index", "instagram-aistatistics", 10);
    }

    @Test
    public void updateTagsInChannel() throws IOException {
        ScrollRepository scrollRepository = new ScrollRepository(client);

        scrollRepository.updateTagsInChannel("instagram-channel-index", "instagram-aistatistics-index", "instagram-aistatistics", 10);
        scrollRepository.updateTagsInChannel("youtube-channel-index", "youtube-aistatistics-index", "youtube-aistatistics", 10);
    }

    @Test
    public void updateLikesInChannel() throws IOException {
        ScrollRepository scrollRepository = new ScrollRepository(client);

        scrollRepository.updateLikesInChannel("instagram-channel-index", "instagram-mediastatistics-index", "instagram-mediastatistics", 10);
    }
}
