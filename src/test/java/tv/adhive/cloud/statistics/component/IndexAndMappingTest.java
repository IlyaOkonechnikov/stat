package tv.adhive.cloud.statistics.component;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.junit.Test;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.webvane.elastic.storage.repository.IndexAndMappingLowLevelRepository;

import java.io.IOException;

public class IndexAndMappingTest {

    @Test
    public void checkAndUpdate() throws IOException {
        RestClient restClient = RestClient.builder(
                new HttpHost("localhost", 9200, "http")).build();
        ResourceLoader loader = new DefaultResourceLoader();
        IndexAndMappingLowLevelRepository repository = new IndexAndMappingLowLevelRepository(restClient);

        IndexAndMapping indexAndMapping = new IndexAndMapping(loader, repository);
        indexAndMapping.setTypes("youtube-mediastatistics");
        indexAndMapping.setPath("classpath:mapping/");

        indexAndMapping.checkAndUpdate();
    }
}