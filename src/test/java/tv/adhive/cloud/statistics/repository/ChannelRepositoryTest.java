package tv.adhive.cloud.statistics.repository;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import tv.adhive.model.channel.Channel;

import java.io.IOException;
import java.util.List;

import static tv.adhive.cloud.statistics.Constants.INSTAGRAM_CHANNEL_INDEX;
import static tv.adhive.cloud.statistics.Constants.INSTAGRAM_CHANNEL_TYPE;

public class ChannelRepositoryTest {

    static RestHighLevelClient highLevelClient;

    @BeforeClass
    public static void setUp() throws Exception {
        highLevelClient = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));
    }

    @AfterClass
    public static void tearDown() throws Exception {
    }

    @Test
    public void count() {
    }

    @Test
    public void searchAndFilterByNotToday() {
    }

    @Test
    public void findAllByStatisticsSubscriberCount() throws IOException {
        ChannelRepository repository = new ChannelRepository(highLevelClient);

        final List<Channel> channels = repository.findAllByStatisticsSubscriberCount(INSTAGRAM_CHANNEL_INDEX, INSTAGRAM_CHANNEL_TYPE, 1000, 1500);

        channels.forEach(System.out::println);
    }
}